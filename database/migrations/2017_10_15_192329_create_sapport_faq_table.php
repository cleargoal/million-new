<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSapportFaqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('supp_faqs', function (Blueprint $table) {
		    $table->increments('id');
		    $table->text('question');
		    $table->text('screenshot_q')->nullable(); // ques: file or link
		    $table->text('answer')->nullable();
		    $table->text('screenshot_a')->nullable(); // answ: file or link
		    $table->string('faqdepkey', 3)->nullable(); // FAQ department key
		    $table->index('faqdepkey', 'faq_dep_key_index');
		    $table->string('faqdepname', 30)->nullable(); // FAQ department name
		    $table->integer('useful')->default(0); // count mark as useful
		    $table->integer('views')->default(0); // count answer views
            $table->integer('user_id')->nullable()->unsigned();
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('supp_faqs');
    }
}
