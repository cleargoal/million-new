<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserClosuresTable extends Migration
{
    public function up()
    {
        Schema::create('user_closure', function(Blueprint $table)
        {
            $table->increments('closure_id');

            $table->bigInteger('ancestor', false, true);
            $table->bigInteger('descendant', false, true);
            $table->integer('depth', false, true);

            $table->foreign('ancestor')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('descendant')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('user_closure', function(Blueprint $table)
        {
            Schema::dropIfExists('user_closure');
        });
    }
}
