<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitcardTable extends Migration
{
    /**   * Run the migrations.   *  * @return void  */
    public function up()
    {
	    Schema::create('vicards', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('visi_slug', 100);
		    $table->unique('visi_slug', 'visi_slug_unique');
		    $table->integer('user_id');
		    $table->index('user_id', 'cards_user_id_index');
		    $table->string('user_slug', 100);
		    $table->index('user_slug', 'cards_user_slug_index');
		    $table->string('avatar', 100)->nullable();
		    $table->string('header', 80);
		    $table->unsignedSmallInteger('price');
		    $table->text('description');
		    $table->text('pictures'); // 5 pics * 20 symbols + 4 commas
		    $table->text('videos'); // 3 videos * 41 symbols + 2 commas
		    $table->text('contacts'); //
		    $table->text('links'); // links separated by commas
		    $table->text('styles'); // object serialized
		    $table->tinyInteger('actual')->default(0);
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('vicards');
    }
}
