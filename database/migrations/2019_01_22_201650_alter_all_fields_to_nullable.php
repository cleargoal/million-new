<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAllFieldsToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->integer('user_id')->nullable()->change();
            $table->float('amount')->nullable()->change();
            $table->string('currency')->nullable()->change();
            $table->string('product')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->string('pay_processor')->nullable()->change();
            $table->string('orderReference')->nullable()->change();
            $table->string('authCode')->nullable()->change();
            $table->string('phone')->nullable()->change();
            $table->string('cardPan')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            //
        });
    }
}
