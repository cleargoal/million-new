<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAvatarColumnAndDeletePicturesColumnFromVicardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vicards', function (Blueprint $table) {
            $table->dropColumn('pictures');
            $table->string('avatar', 50)->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vicards', function (Blueprint $table) {
            //
        });
    }
}
