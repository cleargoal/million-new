<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unique();
            $table->integer('ads_count')->default(0);
            $table->integer('ads_count_day')->default(0);
            $table->timestamp('ads_count_day_update')->useCurrent();
            $table->integer('vis_count')->default(0);
            $table->integer('vis_count_day')->default(0);
            $table->timestamp('vis_count_day_update')->useCurrent();
            $table->integer('bookmarks_count')->default(0);
            $table->integer('links_count')->default(0);
            $table->integer('links_count_day')->default(0);
            $table->timestamp('links_count_day_update')->useCurrent();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_stats');
    }
}
