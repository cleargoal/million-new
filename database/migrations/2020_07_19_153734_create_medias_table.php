<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('vicard_id')->unsigned()->index();
            $table->string('slug', 4)->index(); // 'cont' - contact, 'link' - URL, 'vide' - video link, 'pict' - picture name/path
            $table->string('value');
            $table->timestamps();

            $table->foreign('vicard_id')->references('id')->on('vicards')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medias', function (Blueprint $table) {
            $table->dropForeign(['vicard_id']);
        });
        Schema::dropIfExists('medias');
    }
}
