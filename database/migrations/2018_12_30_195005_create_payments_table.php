<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->index('user_id', 'payments_user_id_index');
            $table->timestamps();
            $table->float('amount');
            $table->string('currency');
            $table->string('product');
            $table->string('email'); // user email
            $table->index('email', 'email_index');
            $table->string('pay_processor'); // WFP & other
            $table->string('orderReference');
            $table->string('authCode');
            $table->string('phone'); // addition data for payer identification
            $table->string('cardPan'); // addition data for payer identification
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
