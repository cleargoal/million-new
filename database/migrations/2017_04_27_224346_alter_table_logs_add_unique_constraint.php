<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableLogsAddUniqueConstraint extends Migration
{
	
		private $unique_index_name = 'logs_constraint_user_id_src_id_log_slug';
	
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('logs', function (Blueprint $table) {
		    $table->index(['user_id', 'src_id', 'log_slug'], $this->unique_index_name);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('logs', function (Blueprint $table) {
		    $table->dropUnique($this->unique_index_name);
	    });
    }
}
