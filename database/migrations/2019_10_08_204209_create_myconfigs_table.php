<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyconfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('myconfigs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('parameter');
            $table->index('parameter');
            $table->string('param_descriptor')->nullable();
            $table->string('str_value')->nullable();
            $table->index('str_value');
            $table->integer('int_value')->nullable();
            $table->index('int_value');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('myconfigs');
    }
}
