<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayservicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payservices', function (Blueprint $table) {
            $table->increments('id');
            $table->char('payslug', 2)->unique();
            $table->string('name');
            $table->string('descript', 255);
            $table->decimal('cost', 7, 2);
            $table->string('purse', 255);
		        $table->char('active', 1);
		        $table->string('extdata', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payservices');
    }
}
