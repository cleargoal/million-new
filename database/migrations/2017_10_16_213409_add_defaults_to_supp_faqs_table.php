<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultsToSuppFaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('supp_faqs', function (Blueprint $table) {
		    $table->string('screenshot_q', 40)->default("")->change(); // ques: file or link
		    $table->string('screenshot_a', 40)->default("")->change(); // ques: file or link
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    // Schema::drop('supp_faqs');
	    // Schema::table('logs', function (Blueprint $table) {
		   //  $table->dropUnique($this->unique_index_name);
	    // });
    }
}
