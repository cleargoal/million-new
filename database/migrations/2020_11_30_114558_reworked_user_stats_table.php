<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReworkedUserStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_stats', function (Blueprint $table) {
            $table->dropUnique('user_stats_user_id_unique');
            $table->string('record_id')->default(0)->after('user_id');
            $table->string('record_type')->default(0)->after('record_id');
            $table->renameColumn('ads_count', 'views')->default(0)->change();
            $table->renameColumn('ads_count_day', 'views_today')->default(0)->change();
            $table->renameColumn('ads_count_day_update', 'daily_update')->change();
            $table->dropColumn(
                ['links_count_day_update',
                    'links_count_day',
                    'links_count',
                    'bookmarks_count',
                    'vis_count_day_update',
                    'vis_count_day',
                    'vis_count'
                ]
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_stats', function (Blueprint $table) {
            //
        });
    }
}
