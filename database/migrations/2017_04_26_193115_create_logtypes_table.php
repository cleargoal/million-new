<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogtypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('logtypes', function (Blueprint $table) {
		    $table->increments('id', 2); // record ID
		    $table->string('type_slug', 3); // log record type's SLUG
		    $table->string('type_descr', 60); // log record type's description
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('logtypes');
    }
}
