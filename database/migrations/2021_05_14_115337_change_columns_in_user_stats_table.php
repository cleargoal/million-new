<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsInUserStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_stats', function (Blueprint $table) {
            DB::statement('
                ALTER TABLE user_stats
                ALTER COLUMN record_id DROP DEFAULT');

//            $table->unsignedBigInteger('record_id')->nullable()->change();

            DB::statement('
                ALTER TABLE user_stats
                ALTER COLUMN record_id TYPE INT
                USING record_id::integer');

            DB::statement('
            ALTER TABLE user_stats
            ALTER COLUMN daily_update SET DEFAULT CURRENT_TIMESTAMP');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_stats', function (Blueprint $table) {
            //
        });
    }
}
