<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabBibleQuote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('biblequotes', function (Blueprint $table) {
		    $table->increments('id'); // record ID
		    $table->text('biblequote'); // biblequote text
		
		    $table->string('biblebook', 30); // book of Bible
		    $table->string('biblebookln', 30); // link to book of Bible
		
		    $table->unsignedSmallInteger('bblbooktop'); // topic of book of Bible
		    $table->string('bblbooktopln', 30); // link to topic of book of Bible
		
		    $table->unsignedSmallInteger('bibleverse'); // verse of Bible
		    $table->string('bibleverseln', 30); // link to verse of Bible
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('biblequotes');
    }
}
