<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /** * Run the migrations. * * @return void */
    public function up()
    {
	    Schema::create('logs', function (Blueprint $table) {
		    $table->increments('id', 16); // record ID
		    $table->integer('user_id')->nullable(); // user's ID - current who made action
		    $table->index('user_id', 'logs_user_id_index');
		    $table->integer('src_id'); // sources's (ad, link, page, etc.) ID that bookmarked, viewed, etc.
		    $table->index('src_id', 'logs_src_id_index');
		    $table->string('log_slug', 3); // log record type's SLUG
		    $table->index('log_slug', 'logs_log_slug_index');
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('logs');
    }
}
