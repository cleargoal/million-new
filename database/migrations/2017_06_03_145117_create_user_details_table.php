<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('user_details', function (Blueprint $table) {
		    $table->increments('id');
		    
		    $table->bigInteger('user_id', false, true);
		    $table->string('user_data_type');
		    $table->string('user_data_val');
		
		    $table->foreign('user_id')->references('id')->on('users');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('user_details', function (Blueprint $table) {
		    $table->dropForeign('fk_user_details_user_id');
		    $table->dropIfExists('user_details');
	    });
    }
}
