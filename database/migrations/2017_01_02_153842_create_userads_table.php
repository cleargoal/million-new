<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUseradsTable extends Migration
{
    /** * Run the migrations. * * @return void */
    public function up()
    {
        Schema::create('userads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->index('user_id', 'userads_user_id_index');
            $table->string('user_slug', 100);
            $table->index('user_slug', 'userads_user_slug_index');
            $table->string('adtext', 160);
            $table->string('adlink', 2048)->nullable();
            $table->string('adstyle', 15);
            $table->string('adfont', 5);
            $table->boolean('actual')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userads');
    }
}
