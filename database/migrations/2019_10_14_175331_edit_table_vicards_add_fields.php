<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTableVicardsAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vicards', function (Blueprint $table) {
            $table->string('avatar', 44)->nullable()->change();
            $table->string('currency', 5)->default('руб');
            $table->string('email', 50);
            $table->string('phone', 14)->nullable();
            $table->string('contacts', 50)->nullable()->change();
            $table->text('videos')->nullable()->change();
            $table->text('links')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
