<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Userad;
use Faker\Generator as Faker;

$factory->define(Userad::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'user_slug' => $faker->word . '-' . $faker->word . '-' . $faker->word . '-' . $faker->word,
        'adtext' => $faker->sentence,
        'adlink' => $faker->url,
        'adstyle' => $faker->hexColor . ',' . $faker->hexColor,
        'adfont' => $faker->randomFloat(1, 2, 3),
        'actual' => true,
    ];
});
