<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Userad;
use App\UserStat;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

$factory->define(UserStat::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
        'record_id' => function () {
        return factory(Userad::class)->create()->id;
    },
        'record_type' => 'ad',
        'views' => 1,
        'views_today' => 1,
        'daily_update' => Carbon::now()->addDay(),

    ];
});
