<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BiblequotesSeeder::class);
        $this->call(MyconfigSeeder::class);
        $this->call(LogtypesSeeder::class);
        $this->call(InitialUsersSeeder::class);
        $this->call(UseradsAlterSeeder::class);
        $this->call(FakePaymentsSeeder::class);
    }
}
