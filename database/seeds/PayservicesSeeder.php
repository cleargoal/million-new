<?php

use Illuminate\Database\Seeder;

class PayservicesSeeder extends Seeder
{
    /**  Run the database seeds.  @return void  */
    public function run()
    {
        DB::table('payservices')->insert([
            'payslug' => 'bc',
            'name' => 'Bit Coins - BTC',
            'descript' => 'Биткоины - цифровая валюта. Современный, быстрый, перспективный, удобный, защищенный способ оплаты в интернет. Стоимость - 3000 сатоши (~ $0.015)',
            'cost' => '3000',
            'purse' => '14uinzkH1FjXc7swcFh6vz3vzfDw8hKG7Z',
        ]);

        DB::table('payservices')->insert([
            'payslug' => 'op',
            'name' => 'OKpay мультивалютный',
            'descript' => 'OKpay - мультивалютный кошелек, принимает как рубли, так и твердую валюту, согласно правилам сервиса. Низкая комиссия при оплате, удобно и легко вводить средства и выполнять оплаты.',
            'cost' => '1.00',
            'purse' => 'OK214434895',
        ]);
	
	    DB::table('payservices')->insert([
			    'payslug' => 'bn',
			    'name' => 'Карта банка - VISA, MC, Maestro',
			    'descript' => 'Оплата картами любого банка - принимает как рубли, так и твердую валюту, согласно правилам сервиса. Низкая комиссия при оплате, удобно и легко вводить средства и выполнять оплаты.',
			    'cost' => '1.00',
			    'purse' => '',
	    ]);

    }
}
