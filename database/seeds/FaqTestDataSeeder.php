<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class FaqTestDataSeeder extends Seeder
{
    /*** Run the database seeds. ** @return void */
    public function run()
    {
	    $faker = Faker::create('ru_RU');
	    foreach (range(1,33) as $index) {
		    DB::table('supp_faqs')->insert([
			    'user_id' => $faker->numberBetween($min = 27, $max = 30),
			    'qu_header' => str_limit($faker->sentence($nbWords = 6, $variableNbWords = false), 32, ''),
			    'question' => $faker->paragraph($nbSentences = 2, $variableNbSentences = true),
			    'answer' => $faker->paragraph($nbSentences = 4, $variableNbSentences = true),
			    'useful' => $faker->numberBetween($min = 1, $max = 30),
			    'views' => $faker->numberBetween($min = 10, $max = 300),
			    'faqdepkey' => $faker->randomElement($array = array ('reg', 'pay', 'ads', 'pag', 'btc')),
		    ]);
	    }
    }
}
