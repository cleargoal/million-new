<?php

use App\User;
use Illuminate\Database\Seeder;

class InitialUsersSeeder extends Seeder
{
    protected $users = [
        0 => [
            'name' => 'million_owner',
            'email' => 'cleargoal01@gmail.com',
            'password' => '321654987',
            'parent_id' => null,
            'invitor' => '0',

            'user_data_type' => 'slug',
            'user_data_val' => 'centimeter-aparently-stranglehold-nighttime-transmutable'
        ],

        1 => [
            'name' => 'million_admin',
            'email' => 'bestresultdone@gmail.com',
            'password' => '321654987',
            'parent_id' => 1,
            'invitor' => '1',

            'user_data_type' => 'slug',
            'user_data_val' => 'disquieting-schizoid-ultrasonics-shiveringly'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createUserAndClosure();
        $this->createUserDetails();
    }

    protected function createUserAndClosure()
    {
        foreach ($this->users as $user) {
            $userExists = User::where('email', $user['email'])->exists();

            if (!$userExists) {
                User::create(
                    [
                        'name' => $user['name'],
                        'email' => $user['email'],
                        'password' => Hash::make($user['password']),
                        'parent_id' => $user['parent_id'],
                        'invitor' => $user['invitor'],
                    ]
                );
            }
        }
    }

    protected function createUserDetails()
    {
        foreach ($this->users as $user) {
            $userExists = User::where('email', $user['email'])->exists();

            if ($userExists) {
                $userId = User::where('email', $user['email'])->value('id');
                $userDetailsExists = DB::table('user_details')->where('user_id', $userId)->exists();

                if (!$userDetailsExists) {
                    DB::table('user_details')
                        ->insert(
                            [
                                'user_id' => $userId,
                                'user_data_type' => $user['user_data_type'],
                                'user_data_val' => $user['user_data_val'],
                            ]
                        );
                }
            }
        }
    }
}
