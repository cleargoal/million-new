<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class VicardTestDataSeeder extends Seeder
{
    /*** Run the database seeds. ** @return void */
    public function run()
    {
        $faker = Faker::create('ru_RU');
        foreach (range(1, 100) as $index) {
            $insert = [
                'user_id' => $faker->numberBetween($min = 1, $max = 30),
                'visi_slug' => $faker->word . '-' . $faker->word . '-' . $faker->word,
                'user_slug' => $faker->word . '-' . $faker->word . '-' . $faker->word . '-' . $faker->word,
                'avatar' => ''
                /*$faker->randomElement($array = [
//                    '1c92dc67dbd45d77b72d5b0b079f9e86.png',
//                    '22c3602825f2d5fdab9a7343332a6aa5.jpeg',
//                    '28afd94cd735576e1bf30409ac861585.jpeg',
//                    '4e8d8c0dff1e1a84286a6c4d9058e038.jpeg',
//                    '60284381028b13e55182afcc24ab5e9d.jpeg',
//                    '66494038f9489f68d408bd081ce6beb4.jpeg',
//                    '6bfd3ba6a1f1a74eac26fe777a56c519.jpeg',
//                    '908cb9ba6faf7fc98f719c34e0721cdc.gif',
//                    'a56d3cfeb153e5740b317629e8636bec.png',
//                    'ae27670ca4c73c3d8a3be237905e8fc2.jpeg',
//                    'be91eb6e2744b91bb058e09dccbe29b4.jpeg', 'ce7db17d378319b223b1740ac40d6e95.jpeg', 'd3f3601b2fc8c0d1386f1c796b8a3943.jpeg',
//                    'd7b982595f026a86d990f5070d469e1f.jpeg'])*/,

                'header' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                'price' => strval($faker->randomNumber($nbDigits = 5, $strict = false)),
                'description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
                'videos' => 'https://www.youtube.com/embed/SXj9pKSpdog'
                /*implode(',', $faker->randomElements([
                'https://www.youtube.com/embed/SXj9pKSpdog',
                'https://www.youtube.com/embed/m_bCKXKEGCg',
                'https://www.youtube.com/embed/E2f1146b4yc',
                'https://www.youtube.com/embed/hA1z1Ov0mZE',
                'https://www.youtube.com/embed/hx8XgFdmk7M',
                'https://www.youtube.com/embed/SXj9pKSpdog'
            ],
                $count = 3))*/,
                'contacts' => 'skype: ' . $faker->word . ', telegram: ' . $faker->phoneNumber,
                'links' => $faker->url,
                'styles' => '#' . $faker->regexify('[a-f0-9]{3}') . ',' . '#' . $faker->regexify('[a-f0-9]{3}'),
                'actual' => '1',
                'currency' => $faker->randomElement(['USD', 'EURO', 'RUB', 'UAH']),
                'email' => $faker->email,
                'phone' => '06745684354',
            ];
            DB::table('vicards')->insert($insert);
        }
    }
}
