<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Myconfig;

class MyconfigSeeder extends Seeder
{
    /**  Run the database seeds.  @return void */
    public function run()
    {
        Myconfig::create([
            'parameter' => Myconfig::PICTURES_FOLDER_NUMBER,
            'int_value' => 0
        ]);

        Myconfig::create([
            'parameter' => Myconfig::AVATARS_FOLDER_NUMBER,
            'int_value' => 0
        ]);
    }
}
