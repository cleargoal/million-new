<?php

use App\Payment;
use App\User;
use Illuminate\Database\Seeder;

class FakePaymentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'bestresultdone@gmail.com')->first();

        Payment::create([
            'user_id' => $user->id,
            'amount' => 10,
            'currency' => 'RUB',
            'product' => 'adv',
            'email' => $user->email,
            'pay_processor' => 'WFP'
        ]);

        Payment::create([
            'user_id' => $user->id,
            'amount' => 50,
            'currency' => 'RUB',
            'product' => 'vis',
            'email' => $user->email,
            'pay_processor' => 'WFP'
        ]);

    }
}
