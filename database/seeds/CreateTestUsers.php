<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use League\Csv\Writer;

class CreateTestUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('Ru_RU');
        
        $writer = Writer::createFromPath(dirname(__FILE__) . '\file_users.csv', 'w+');
        $writer->setDelimiter(";");
        $header = ["name", "email", "password", "clos_pos", "depth"];
        $writer->insertOne($header);
        foreach (range(1, 10000) as $newUser) {
            $fakerRow = [$faker->name, $faker->email, $faker->password, '1', '1'];
            $writer->insertOne($fakerRow);
        }

    }
}
