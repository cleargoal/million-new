<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LogtypesSeeder extends Seeder
{
    /*** Run the database seeds. ** @return void */
    public function run()
    {
        if (DB::table('logtypes')->get()->count() < 1) {

            DB::table('logtypes')->insert([
                'type_slug' => 'vad',
                'type_descr' => 'Отметка о просмотре объявления',
            ]);

            DB::table('logtypes')->insert([
                'type_slug' => 'bmk',
                'type_descr' => 'Отметка о сделанной закладке',
            ]);

            DB::table('logtypes')->insert([
                'type_slug' => 'lnk',
                'type_descr' => 'Отметка о клике по ссылке в объявлении',
            ]);
        }
    }
}
