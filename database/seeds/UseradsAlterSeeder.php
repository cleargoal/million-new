<?php

use App\User;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use App\Userad;

class UseradsAlterSeeder extends Seeder
{
    /**  Run the database seeds.  @return void */
    public function run()
    {
        $this->command->info('User ads Seeder - start');
//        if (Userad::count() < 1) {
            $this->fillDataTable();
//        }
    }

    protected function fillDataTable()
    {
        $userId = User::where('email', 'bestresultdone@gmail.com')->value('id');
        $slug = DB::table('user_details')->where('user_id', $userId)->value('user_data_val');

        // сделать новый массив - ассоциативный
        $reader_ass = [
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Международный проект, командная работа, построение структуры для продажи товаров, регистрация бесплатная, продуманные инструменты (на английском), смотри --->>>', 'adlink' => 'http://www.sfi4.com/16704399.9011/FREE', 'adstyle' => '#28d343,#1a3e52', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Отличный хостинг для пользователей из стран бывшего СНГ. Поддержка 24 часа онлайн, отвечает быстро и грамотно. Умеренная цена и высокое качество. Пробная неделя', 'adlink' => 'http://ukrline.com.ua/?ref=18550', 'adstyle' => '#4d5859,#a6fb1f', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Бесплатная реклама в зарубежном Инете - система активной раскрутки. Привлекайте трафик на свой забугорный ресурс за кредиты, которые получаете в этой же системе', 'adlink' => 'http://tinyurl.com/yaylbx48', 'adstyle' => '#4fd5ca,#040357', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Приму в подарок саксофон-тенор, желательно в рабочем состоянии, но можно и в нерабочем. Благодарю!!!!! Пишите cleargoal1@gmail.com, +38063-203-6717', 'adlink' => '', 'adstyle' => '#f769a6,#3d1902', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Наполнение ваших сайтов информацией, уникальный контент для вебмастера, уникальные авторские статьи, рерайт новостей на любые тематики, вирусный маркетинг.', 'adlink' => 'https://advego.com/2hAr7YjyTD', 'adstyle' => '#ee5e07,#220241', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Заработок для вебмастеров - рекламная международная сеть с акцентом на биткоин. Биткоин может расти или падать, а ты будешь зарабатывать все равно.', 'adlink' => 'https://coinzilla.io/ref/?u=348555ab20d5557e9c', 'adstyle' => '#f2654b,#102e0d', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Отдохнуть НЕДОРОГО по всему миру Вы можете в отелях типа «постель и завтрак». Сервис там нормальный. Подберите хорошее предложение для себя! Приятного отдыха!', 'adlink' => 'https://www.booking.com/bed-and-breakfast/index.html?aid=1227674', 'adstyle' => '#96178e,#eaf152', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Конан Дойль - известный английский писатель детективов. Его книги прочитаны во всем мире сотни миллионов раз. Прочтите и Вы. Бесплатно, без регистрации.', 'adlink' => '', 'adstyle' => '#ee5e07,#220241', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Косметика Avon с большими скидками - распродажа! Чем больше Ваш заказ, тем больше будет скидка. Все детали по тел. 7926911334', 'adlink' => 'https://twitter.com/AvonInsider', 'adstyle' => '#ee5e07,#220241', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Оказываю услуги по поддержке и доработке веб-сайтов на фреймворке Laravel. Опыт 4+ года. По ссылке смотреть немного подробнее. Портфолио - там же.', 'adlink' => '', 'adstyle' => '#ee5e07,#220241', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Предлагаю услуги по накрутке подписчиков в социальных сетях. Пишите мне Твиттер - будем сотрудничать. ', 'adlink' => 'https://twitter.com/fKSeqaILRc9g6Fm', 'adstyle' => '#ee5e07,#220241', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Продаю на запчасти сумку-тележку Gimi (Италия). Сломалась пластиковая деталь опоры колеса. Смотрите подробнее по ссылке.', 'adlink' => '', 'adstyle' => '#ee5e07,#220241', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Мобильные телефоны и аксессуары с доставкой прямо из Китая. Цены практически от производителя, так что Вы сэкономите около 20% и то и более.', 'adlink' => 'https://vk.com/id532565400', 'adstyle' => '#ee5e07,#220241', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Вступай в группы и ставь лайки за деньги. Автоматическая проверка, мгновенное начисление.', 'adlink' => 'https://vktarget.ru/?ref=7248339', 'adstyle' => '#ee5e07,#220241', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Тексты для сайта как лицо для компании. Обращайтесь, и у вашего ресурса будет безупречный вид. Почта для связи: shevchenko.ok88@gmail.com', 'adlink' => '', 'adstyle' => '#ee5e07,#220241', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Большое всемирное сообщество домашнего бизнеса (сайт на англ. языке). Возможность продавать свои товары и услуги по всему миру. Присоединяйся.', 'adlink' => 'http://www.sfi4.com/16704399/FREE', 'adstyle' => '#ee5e07,#220241', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Просто войди и прочти... ', 'adlink' => 'http://www.joinmysfiteam.com/16704399', 'adstyle' => '#ee5e07,#220241', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Интересный аукцион, ставка ставится 1 раз - нужно предугадать ход и результат торгов. Англоязычный интерфейс, но все понятно.', 'adlink' => 'http://www.sfi4.com/16704399/Astro', 'adstyle' => '#ee5e07,#220241', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Меняю астероид на комету. Для жизни не пригоден. Содержит много титаната бария. Без атмосф. Планетарная масса 9*10^16 мегатонн. Звоните +999 (11) 8495675135487', 'adlink' => '', 'adstyle' => '#ee5e07,#220241', 'adfont' => '2.4em',],
            ['user_id' => $userId, 'user_slug' => $slug, 'adtext' => 'Посмотрите сколько в мире прекрасных мест где можно отдохнуть, начинайте больше зарабатывать, пользуйтесь этим', 'adlink' => 'https://www.booking.com/index.html?aid=1794209', 'adstyle' => '#ee5e07,#220241', 'adfont' => '2.4em',],
        ];

        foreach ($reader_ass as $val) {
            $uad = new Userad();
            foreach ($val as $key => $sub) {
                $uad->$key = $sub;
            }
            $uad->save();
        }
    }
}
