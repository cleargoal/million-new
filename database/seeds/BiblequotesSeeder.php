<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;
use App\Biblequote;

class BiblequotesSeeder extends Seeder
{
	/**  Run the database seeds.  @return void */
	public function run()
	{
		if (Biblequote::count() < 1) {
		    $this->fillDataTable();
        }
	}

    protected function fillDataTable()
    {
        $reader = Reader::createFromPath(base_path('database/seeds/homestead_biblequotes.csv'), 'r');
        $reader_all = $reader->getRecords(); //returns all the CSV records as an Iterator object
        // info ($reader_all);

        foreach ($reader_all as $key => $val) {
            // создать новый массив - ассоциативный
            $reader_ass = [];
            $reader_ass['biblequote'] = $val['1'];
            $reader_ass['biblebook'] = $val['2'];
            $reader_ass['biblebookln'] = $val['3'];
            $reader_ass['bblbooktop']= $val['4'];
            $reader_ass['bblbooktopln'] = $val['5'];
            $reader_ass['bibleverse'] = $val['6'];
            $reader_ass['bibleverseln'] = $val['7'];

            // foreach ($val as $skey => $sval) {
            // 		if ($skey == 1) $reader_ass['biblequote'] = $sval;
            // 		if ($skey == 2) $reader_ass['biblebook'] = $sval;
            // 		if ($skey == 3) $reader_ass['biblebookln'] = $sval;
            // 		if ($skey == 4) $reader_ass['bblbooktop']= $sval;
            // 		if ($skey == 5) $reader_ass['bblbooktopln'] = $sval;
            // 		if ($skey == 6) $reader_ass['bibleverse'] = $sval;
            // 		if ($skey == 7) $reader_ass['bibleverseln'] = $sval;
            // 	}
            $bbq = Biblequote::create($reader_ass);
        }
        // info ($reader_ass);
	}
}
