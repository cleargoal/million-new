<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use League\Csv\Writer;

class CreateTestPayments extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('Ru_RU');
    
        $writer = Writer::createFromPath(dirname(__FILE__) . '\file_pays.csv', 'w+');
        $writer->setDelimiter(";");
        $header = ["user_id", "amount", "product"];
        $writer->insertOne($header);
        foreach (range(1, 9990) as $newUser) {
            $fakerRow = [$faker->numberBetween($min = 1, $max = 9900), $faker->randomElement(['4', '20', '10', '50']), $faker->randomElement(['adv', 'vis'])];
            $writer->insertOne($fakerRow);
        }
    }
}
