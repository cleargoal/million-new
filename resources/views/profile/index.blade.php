@extends('layouts.app')
@section('title', 'Кабинет пользователя')

@section('content')
<div class="artily-profile-wrapper pos-static">
    <div class="million-panel">
        <div class="million-panel-heading mt-4">
            <h3>Профиль пользователя: {{ $user -> name }}</h3>
        </div>
        <div class="million-panel-body">
            <div class="profile-main">
                <div class="profile-left">
                    <ul class="nav nav-tabs profile-tabs" role="tablist">
                        <li data-id="#tab-1" class="active">Обзор</li>
                        <li data-id="#tab-2">Ваши объявления</li>
                        <li data-id="#tab-3">Ваши визитки</li>
                        <li data-id="#tab-4">Ваши платежи</li>
                        <li data-id="#tab-7">Ваша аудитория</li>
                        <li data-id="#tab-5">Вы просматривали закладки</li>
                        <li data-id="#tab-6">Вы просматривали визитки</li>
                    </ul>
                </div>
                <div class="profile-right">
                    <div class="prof-tab-content tab-pane show" id="tab-1">
                        @include('profile.view')
                    </div>
                    <div class="prof-tab-content tab-pane" id="tab-2">
                        @include('profile.ads')
                    </div>
                    <div class="prof-tab-content tab-pane" id="tab-3">
                        @include('profile.vis')
                    </div>
                    <div class="prof-tab-content tab-pane" id="tab-4">
                        @include('profile.pay')
                    </div>
                    <div class="prof-tab-content tab-pane" id="tab-5">
                        @include('profile.bookmarks')
                    </div>
                    <div class="prof-tab-content tab-pane" id="tab-6">
                        @include('profile.viewvis')
                    </div>
                    <div class="prof-tab-content tab-pane" id="tab-7">
                        @include('profile.invites')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script src="\js\profile.js"></script>
@endpush

