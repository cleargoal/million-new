<h5>Вы просматривали визитки</h5>
<table class="visit-table">
    <thead>
    <tr>
        <td>Заголовок визитки</td>
        <td>Цена в визитке</td>
        <td><img src = "/img/eye7w.png" alt = "Видел" title="Кол-во просмотров" class="profeye"></td>
    </tr>
    </thead>
    <tbody>
    @foreach ($lbvs as $lbv)
        <tr>
            <td><a href = "{{route('showvslug', $lbv->visi_slug)}}" target="_blank">{{$lbv->header}}</a ></td>
            <td>{{$lbv->price}}</td>
            <td>{{$lbv->grp_vs}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
