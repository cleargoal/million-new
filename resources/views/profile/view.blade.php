<h4>Обзор. Статистика обновляется регулярно</h4>
<table class="mtb">
    <thead>
    <tr>
        <th>Ваши данные</th>
        <th>кол-во</th>
    </tr>
    </thead>
    <tr>
        <td>Объявления</td>
        <td>{{ $ovewview['ads_count'] }}</td>
    </tr>
    <tr>
        <td>Просмотры объявлений</td>
        <td>{{ $user->stat['ads_count'] }}</td>
    </tr>
    <tr>
        <td>Просмотры объявлений за 24 часа</td>
        <td>{{ $user->stat['ads_count_day'] }}</td>
    </tr>
    <tr>
        <td>Добавлений в закладки</td>
        <td>{{ $user->stat['bookmarks_count'] }}</td>
    </tr>
    <tr>
        <td>Добавлений в закладки за 24 часа</td>
        <td>{{ $user->stat['bookmarks_count_day'] }}</td>
    </tr>
    <tr>
        <td>Переход по ссылкам</td>
        <td>{{ $user->stat['links_count']}}</td>
    </tr>
    <tr>
        <td>Переход по ссылкам за 24 часа</td>
        <td>{{ $user->stat['links_count_day'] }}</td>
    </tr>
    <tr>
        <td>Визитки</td>
        <td>{{ $ovewview['vcs_count'] }}</td>
    </tr>
    <tr>
        <td>Просмотры визиток</td>
        <td>{{ $user->stat['vis_count'] }}</td>
    </tr>
    <tr>
        <td>Просмотры визиток за 24 часа</td>
        <td>{{ $user->stat['vis_count_day'] }}</td>
    </tr>
    <tr>
        <td>Оплачено всего, {{ $ovewview['currency'] }}</td>
        <td>{{ $ovewview['amount'] }}</td>
    </tr>
</table>
<div class="mt10">
    <h6>Ссылка для приглашения</h6>
    <div class="profile-links">{{route('slug-referral-link', $user->slug)}}</div>
</div>
<div class="mt10">
    <h6>Ссылка на список Ваших визиток</h6>
    <div class="profile-links">{{route('listuslug', [$user->slug])}}</div>
    <div class="small"><i>выделите и скопируйте, если хотите кому-то показать</i></div>
</div>
<div class="mt10 dark">
    @if($ovewview['vcs_count'] > 0)
      <a href="{{route('listuslug', $user->slug)}}" target="_blank" class="prof-bor-bot">Посмотреть свой список</a>
    @else
      <a href="{{route('createvisit')}}" target="_blank" class="prof-bor-bot">Вы еще не создавали визитки</a>
    @endif
    - <span class="small">откроется в новой вкладке</span>
</div>
