<h5>Ваши платежи</h5>
<table>
    <thead>
    <tr>
        <td>Дата платежа</td>
        <td>Валюта</td>
        <td>Сумма</td>
        <td>Продукт</td>
    </tr>
    </thead>
    <tbody>
    @foreach($paym as $pam)
        @php $pam->product !== null ? $prod = config('settings.'. $pam->product) : $prod[0] = '' ; @endphp
        <tr>
            <td>{{$pam->created_at}}</td>
            <td>{{$pam->currency}}</td>
            <td>{{$pam->amount}}</td>
            <td>{{$prod[0]}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
