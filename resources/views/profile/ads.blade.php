<h5>Ваши объявления</h5>
<div class="table-striped-wrapper">
  <table class="table-striped">
      <thead>
          <tr class="artily-proftable-head">
              <td style="width: 50%;">Текст</td>
              <td style="width: 10%;">Просмотры</td>
              <td style="width: 9%;">Закладки</td>
              <td style="width: 9%;">Переходы</td>
              <td style="width: 9% ">Перейти</td>
              <td style="width: 13% ">Редактировать</td>
              {{-- <td style="width: 16%; word-break: break-all;">Редактировать</td> --}}
          </tr>
      </thead>
      <tbody>
          @foreach($ads as $ad)
          <tr class="artily-proftable-row">
              <td >{{$ad->adtext}}</td>
              <td >{{$ad->adViews->views ?? 0}}</td>
              <td >{{$ad->links->views ?? 0}}</td>
              <td >{{$ad->bookmarks->views ?? 0}}</td>
              <td >
                  @if (!empty($ad['adlink']))
                  <a href="{{$ad['adlink']}}" target="_blank" data-adid="{{$ad->id}}" class="adid" target="_blank">
                      <div title="Переход по ссылке">&#128065;</div>
                  </a>
                  @endif
              </td>
              <td>
                  <a href="{{route('edit-ad', ['adid' => $ad->id])}}">
                      <div class="btn-info btn btn-edit" title="Редактировать">&#128221;</div>
                  </a>
              </td>
          </tr>
          @endforeach
      </tbody>
  </table>
</div>
