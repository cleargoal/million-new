<h5>Ваши визитки</h5>
<div class="your-visit-table-wrapper">
  <table class="your-visit-table">
      <thead>
      <tr>
          <td>Заголовок</td>
          <td>Просмотры</td>
          <td>Ссылка</td>
          <td></td>
      </tr>
      </thead>
      <tbody>
      @foreach ($visits['vcs'] as $vc)
          <tr>
              <td class="dark"><a href = "{{ route('showvslug', ['vicard' => $vc->visi_slug]) }}" target="_blank" >{{$vc->header}}</a ></td>
              <td style="text-align: center; padding-right: 0.8rem;">{{$visits['lgv']->where('src_id', $vc->id)->count('src_id')}}</td>
              <td style="padding: 0 1.5rem;">{{ route('showvslug', ['vicard' => $vc->visi_slug]) }}</td>
              <td style="padding: 0 1.5rem;"><a href="{{ route('edit.visit', ['vicard' => $vc->visi_slug]) }}">
                      <div class="btn btn-info-color" data-href="">Редактировать</div></a>
              </td>
          </tr>
      @endforeach
      </tbody>
  </table>
</div>
