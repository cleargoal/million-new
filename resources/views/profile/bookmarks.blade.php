<h5>Вы просматривали закладки</h5>
<table class="bml" data-csrf-token="{{csrf_token()}}" data-href="{{route('loglinkbmk')}}">
    <thead>
    <tr>
        <td>Текст объявления</td>
        <td><img src = "img/eye7w.png" alt = "Видел" title="Вы просматривали ... раз" class="profeye"></td>
        <td><span class="gotolink" title="Перейти по ссылке">>></span></td>
    </tr>
    </thead>
    <tbody>
    @foreach ($lbls as $lba)
        <tr>
            <td>{{$lba->adtext}}</td>
            <td>{{$lba->grp_ads}}</td>
            <td><a href = "{{$lba->adlink}}" target="_blank" data-adid="{{$lba->src_id}}" class="adid">
                    <span class="gotolink" title="Открыть ссылку в новой вкладке">>></span>
                </a></td>
        </tr>
    @endforeach
    </tbody>
</table>
