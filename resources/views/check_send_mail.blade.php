@extends('layouts.app')

@section('content')
    <div class="artily-million-panel">
        <div class="artily-million-panel-body">
            <h1>Проверка отсылки письма</h1>
        </div>
        <div class="artily-million-panel-body">
            <form action="{{route('check-email-send')}}" method="post" class="artily-faq-form">
                {{ csrf_field() }}
                <label for="name">Кому - имя</label>
                <input type="text" name="name" id="name"><br>
                <label for="email">Кому - почта</label>
                <input type="text" name="email" id="email"><br>
                <label for="subject">Заголовок</label>
                <input type="text" name="subject" id="subject"><br>
                <label for="message">Сообщение</label>
                <input type="text" name="message" id="message"><br>
                <input type="submit">

            </form>
        </div>
    </div>
@endsection
