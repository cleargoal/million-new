@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item page-item-control disabled"><span class="page-link">&#8656;</span></li>
            <li class="page-item page-item-control disabled"><span class="page-link">&#8592;</span></li>
        @else
            <li class="page-item page-item-control"><a class="page-link" href="{{ $paginator->url(1) }}" rel="prev">&#8656;</a></li>
            <li class="page-item page-item-control"><a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">&#8592;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item disabled"><span class="page-link">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active"><span class="page-link">{{ $page }}</span></li>
                    @else
                        <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item page-item-control"><a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">&#8594;</a></li>
            <li class="page-item page-item-control"><a class="page-link" href="{{ $paginator->url($paginator->lastPage()) }}" rel="next">&#8658;</a></li>
        @else
            <li class="page-item page-item-control disabled"><span class="page-link">&#8594;</span></li>
            <li class="page-item page-item-control disabled"><span class="page-link">&#8658;</span></li>
        @endif
    </ul>
@endif
