<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('user') }}'><i class='nav-icon la la-question'></i> Users</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('viewstatistics') }}'><i class='nav-icon la la-question'></i> View Statistics</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('supp_faq') }}'><i class='nav-icon la la-question'></i> Support Q&A</a></li>


<li class='nav-item'><a class='nav-link' href='{{ backpack_url('biblequote') }}'><i class='nav-icon la la-question'></i> Biblequotes</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('logtype') }}'><i class='nav-icon la la-question'></i> LogTypes</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('payment') }}'><i class='nav-icon la la-question'></i> Payments</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('userad') }}'><i class='nav-icon la la-question'></i> Advertisements</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('vicard') }}'><i class='nav-icon la la-question'></i> Visit cards</a></li>
