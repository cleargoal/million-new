@extends(backpack_view('blank'))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div>
                    @if(!empty($faq -> screenshot_q))
                        <img src="{{ asset($faq -> screenshot_q ) }}">
                    @endif
                </div>
                <div class="panel-heading" style="font-size: 130%; font-weight: 700;" >
                    <p>Вопрос</p>
                    <h5 class="butt-faq-ask">{{ $faq -> question }}</h5>
                </div>
                <div class="panel-body">
                    <form action="{{ backpack_url('supp_faq/'.$faq->id.'/send-answer') }}" enctype="multipart/form-data" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="answer">Текст ответа</label>
                            <textarea name="answer" class="form-control">{{ $faq -> answer ?? '' }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="screenshot_a">Прикрепить изображение</label>
                            <input type="file" name="screenshot_a" class="form-control" accept="image/*">
                        </div>
                        <button type="submit" class="btn btn-primary">{{ empty($faq -> answer) ? 'ответить' : 'изменить' }}</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
