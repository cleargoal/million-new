@extends(backpack_view('blank'))

@section('content')
    <div class="row">
            <table class="table table-light">
                <thead class="thead-light">
                <th>ID:</th>
                <th>Name</th>
                <th>Email</th>
                <th>Регистрация</th>
                </thead>
                <tbody>
                @forelse ($children as $user)
                    <tr>
                        <th>{{ $user -> id }}</th>
                        <td>{{ $user -> name }}</td>
                        <td>{{ $user -> email }}</td>
                        <td>{{ $user -> created_at }}</td>
                    </tr>
                @empty
                    <tr><td></td><th>Нет приглашенных пользователей</th></tr>
                @endforelse
                </tbody>
            </table>

    </div>
@endsection
