@extends(backpack_view('blank'))

@section('content')
    <div class="row">
        <table class="table table-light">
            <thead class="thead-light">
            <th>ID:</th>
            <th>Name</th>
            <th>Email</th>
            <th>Регистрация</th>
            </thead>
            <tbody>
            @if ($parent)
                <tr>
                    <th>{{ $parent -> id }}</th>
                    <td>{{ $parent -> name }}</td>
                    <td>{{ $parent -> email }}</td>
                    <td>{{ $parent -> created_at }}</td>
                </tr>
            @else
                <tr>
                    <td></td>
                    <th>{{ '-' }}</th>
                    <td>{{ '-' }}</td>
                    <td>{{ '-' }}</td>
                    <td>{{ '-' }}</td>
                </tr>
            @endif
            </tbody>
        </table>

    </div>
@endsection
