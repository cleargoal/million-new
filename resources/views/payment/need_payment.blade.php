@extends('layouts.app')
@section('title', 'Оплатите продукт')

@section('content')

    <div class="artily-need-payment-wrap">
        <div class="artily-million-panel">
            {{-- <div class="artily-million-panel-heading"> --}}
            <h3>Продукт нужно оплатить</h3>
            {{-- </div> --}}
            <div class="artily-million-panel-body">
                <h4 class="artily-million-want">Вы хотите создать</h4>
                <h4 class="artily-million-product">{{$product}}</h4>
                <h4 class="artily-million-thanks">Благодарим за Ваш выбор!</h4>
                <a href="{{route('pay-for-ad')}}">
                    <button class="btn btn-main-color">Перейти к оплате</button>
                </a>
            </div>
        </div>
    </div>


@endsection
