@extends('layouts.app')
@section('title', 'Страница оплаты')

@section('content')
    <div class="artily-pay-bigwrap">
        <div class="artily-pay-wrap">
            <div class="artily-pay-shorttitle"><h3>Оплата наших услуг</h3></div>
            <div class="artily-pay-longtitle"><strong>После нажатия на кнопку Вы перейдете на страницу платежной системы, чтобы провести оплату в защищенном
                    режиме. После оплаты система вернет Вас обратно на наш сайт. </strong></div>
            <div class="artily-pay-main">
                <div class="artily-pay-choice">
                    <div class="artily-pay-rub">
                        <h4>Оплата - Рубли</h4>
                        <div class="artily-pay-buttons">
                            <a href="https://secure.wayforpay.com/button/b0bdfb5f4554b">
                                <button class="btn btn-info-color" type="button">Объявление - 10 руб.</button>
                            </a>
                            <a href="https://secure.wayforpay.com/button/bdb2f745ca422">
                                <button class="btn btn-info-inverse" type="button">Визитка - 50 руб.</button>
                            </a>
                        </div>
                    </div>

                    <div class="artily-pay-uah">
                        <h4>Оплата - Гривна</h4>
                        <div class="artily-pay-buttons">
                            <a href="https://secure.wayforpay.com/button/b05ec0a797e76">
                                <button class="btn btn-info-color" type="button">Объявление - 4 грн.</button>
                            </a>
                            <a href="https://secure.wayforpay.com/button/b35f01ca42d0c">
                                <button class="btn btn-info-inverse" type="button">Визитка - 20 грн.</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="artily-pay-mail">
                    <div class="">Внимание! В платежной форме WayForPay вводите
                        <h5 class="" style="padding: 4px 10px;">зарегистрированный у нас емэйл</h5> - только так мы сможем определить, что оплатили именно Вы.
                        <p>Ваш зарегистрированный емэйл:</p>
                        <input type="email" id="select-eml" value="{{Auth::user()->email}}" style="color: white" onclick="this.select();">
                        <p class="small">Клик, чтобы выделить емэйл, затем скопируйте его в буфер обмена и затем вставьте из буфера при оплате.</p>
                    </div>
                </div>
            </div>
            <div class="artily-pay-info">
                <h5>Емэйл о результате платежа Вы получите от платежной системы WayForPay</h5>
                <div class="">После получения подтверждения оплаты от платежной системы Вы можете сразу перейти к созданию своего оплаченного продукта -
                    объявления или визитки.
                </div>
            </div>
        </div>
    </div>

@endsection
