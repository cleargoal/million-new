@extends('layouts.app')
@section('title', 'По какой-то причине оплата ' . $product . ' не удалась')

@section('content')
    <div class="artily-need-payment-wrap">
        <div class="artily-million-panel">
            <h3>Оплата не удалась</h3>
            <div class="artily-million-panel-body">
                <h4 class="artily-million-want">Вы пытались оплатить</h4>
                <h4 class="artily-million-product">{{$product}}</h4>
                <h4 class="artily-million-want"> но произошла какая-то ошибка.</h4>
                <a href="{{route('pay-for-ad')}}">
                    <button class="btn btn-main-color">Попробуйте снова</button>
                </a>
            </div>
        </div>
    </div>

@endsection
