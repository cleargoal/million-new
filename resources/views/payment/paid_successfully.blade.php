@extends('layouts.app')
@section('title', 'Вы успешно оплатили ' . $product)

@section('content')

    <div class="artily-need-payment-wrap">
        <div class="artily-million-panel">
            <h3>Оплачено успешно!</h3>
            <div class="artily-million-panel-body">
                <h4 class="artily-million-want">Вы оплатили </h4>
                <h4 class="artily-million-product">{{$product}}.</h4>
                <h4 class="artily-million-thanks">Спасибо!</h4>
                <a href="{{route($route)}}">
                    <button class="btn btn-main-color">Теперь Вы можете перейти к созданию</button>
                </a>
            </div>
        </div>
    </div>


@endsection
