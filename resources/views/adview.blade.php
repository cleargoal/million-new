@extends('layouts.app')
@section('title', 'Просмотр образцов объявлений')

@section('content')
    @php
        $noDisplay = $mode == 'b' ? ' hide' : '';
    @endphp
    <div id="adview" class="artily-adview {{$noDisplay}}">
        <div class="artily-adview-wrapper">
            <!-- header -->
            <div class="artily-adview-header">
                <div class="artily-adview-h3">
                    <h3>Просмотр объявления</h3>
                    <div class="artily-gotobmk hide">

{{--                        <p>--}}
{{--                            <span>Вы отметили <span id="adsCount">Х</span> <span id="bmkWords">закладок</span></span>--}}
{{--                        </p>--}}
{{--                        <button class="btn-small btn-info-inverse" id="saveBook">Перейти&nbsp;&nbsp;>>></button>--}}

                    </div>

                    <div class="artily-blink-area" id="blink-area">
                        <div class="artily-bookmrk">
                            <label for="bookmrk" id="bmrklabel">
                                <input id="bookmrk" type="checkbox" class="artily-input-bookmrk"/>
                                <span id="glcheck" class="glyph-check">&#8730;</span>
                                <h4 class="artily-bookmark-glyph-check">Закладка</h4>
                            </label>

                        </div>
                    </div>
                </div>
            </div>

            <div class="artily-admain-wrapper">
                <div class="artily-adview-main">
                    {{-- left button --}}
                    <div class="artily-button-pan artily-pan-prev" id="buttpan" data-href="{{ route('adviewed')}}"
                         data-csrf-token="{{ csrf_token() }}">
                        <button class="artily-butprev" id="butprev"> <</button>
                    </div>

                    {{-- content --}}
                    <div class="artily-adview-content">
                        <div id="viewsavedad" class="artily-ad-text" data-ads="{{$ads}}"></div>
                    </div>

                    {{-- right button --}}
                    <div class="artily-button-pan artily-pan-next" id="pan-next"
                         data-href="{{ route('save-bookmark')}}">
                        <button class="artily-butnext" id="butnext" data-href="{{ route('list-bookmarks')}}">>
                        </button>
                    </div>
                </div>

                <div class="next-ad-timer-field-parent">
                    <div class="artily-adview-info">
                        <p><span>Сделайте закладку</span> - кнопка вверху справа, чтобы иметь возможность посмотреть сайт,
                            где данное предложение описано подробнее</p>
                    </div>
                    <div class="next-ad-timer-field">
                        <div class="ad-timer-text">
                            Переход к следующему объявлению через:
                        </div>
                        <div class="next-ad-timer">
                            <div class="counter_field">
                                <div class="div_counter_text">30</div>
                                <svg width="100" height="100" xmlns="http://www.w3.org/2000/svg">
                                    <circle id="circle" class="segmented_circle" r="30" cy="50" cx="40" stroke-width="6"
                                            stroke="#6fdb6f" fill="none"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if($mode == 's')
                <div class="million-panel">
                    <div class="million-panel-body">
                        <div>
                            <p>В таком виде Ваше объявление будут видеть посетители сайта. Если Вы хотите что-то изменить -
                                отредактируйте его.</p>
                            <a href="{{route('edit-ad', ['adid' => $usersad])}}">
                                <button class="btn btn-main-color">Редактировать</button>
                            </a>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    @if($mode == 'b')
        <div class="messages" id="mess">
            <div id="title" class="title info">Внимание!</div>
            <div id="message" class="message">
                <div>
                    <h5>Прежде создания и публикации своего объявления, Вам необходимо посмотреть примеры/образцы
                        - {{$viewAmount}} штук.</h5>
                    <br>
                    <h4>Обратите внимание:</h4>
                    <h6>Вы можете делать на объявления закладки - для последующего просмотра ресурсов, которые
                        анонсируются в объявлениях.</h6>
                    <p>Кнопка для закладок находится справа вверху в виде - голубая "галочка" и кнопка
                        <span class="glyph-check">&#8730; </span>
                        <span class="artily-bookmark-glyph-check">Закладка</span>
                    </p>
                </div>
                <br>
                <div>
                    <div class="btn btn-main-color cursor-pointer" id="message-ok">
                        ОК
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
@push('scripts')
    <script src="{{asset('/js/advert.js')}}" defer></script>
@endpush
