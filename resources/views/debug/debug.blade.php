@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel">
                    <div class="panel-header">Check variables and other</div>
                    <div class="panel-body">
                        <table>
                            @foreach($got_array as $key => $val)
                                <tr>
                                    <td>{{$key}}</td>
                                    <td>{{$val}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
