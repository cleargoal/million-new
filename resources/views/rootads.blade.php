@extends('layouts.app')
@section('title', 'Просмотр объявлений')

@section('content')
    <div id="adview" class="artily-adview">
        <div class="artily-adview-wrapper">
            <div class="artily-adview-header">
                <div class="artily-adview-h3">
                    <h3>Просмотр объявления</h3>
                    <div class="artily-gotobmk hide">
                        <p>
                            <span>Вы отметили <span id="adsCount">Х</span> <span id="bmkWords">закладок</span></span>
                        </p>
                        <button class="btn-small btn-info-inverse" id="saveBook">Перейти&nbsp;&nbsp;>>></button>
                    </div>

                    <div class="artily-blink-area" id="blink-area">
                        <div class="artily-bookmrk">
                            <label for="bookmrk" id="bmrklabel">
                                <input id="bookmrk" type="checkbox" class="artily-input-bookmrk"/>
                                <span id="glcheck" class="glyph-check">&#8730;</span>
                                <h4 class="artily-bookmark-glyph-check">Закладка</h4>
                            </label>

                        </div>
                    </div>
                </div>
            </div>

            <div class="artily-admain-wrapper">
                <div class="artily-adview-main">
                    {{-- left button --}}
                    <div class="artily-button-pan artily-pan-prev" id="buttpan" data-href="{{ route('adviewed')}}"
                         data-csrf-token="{{ csrf_token() }}">
                        <button class="artily-butprev" id="butprev"> <</button>
                    </div>

                    {{-- content --}}
                    <div class="artily-adview-content">
                        <div id="viewsavedad" class="artily-ad-text" data-ads="{{$ads}}"></div>
                    </div>

                    {{-- right button --}}
                    <div class="artily-button-pan artily-pan-next" id="pan-next"
                         data-href="{{ route('save-bookmark')}}">
                        <button class="artily-butnext" id="butnext" data-href="{{ route('list-bookmarks')}}">>
                        </button>
                    </div>
                </div>

                <div class="artily-adview-info">
                    <p><span>Сделайте закладку</span> - кнопка вверху справа, чтобы иметь возможность посмотреть сайт,
                        где данное предложение описано подробнее</p>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="/js/advert.js" defer></script>
@endpush
