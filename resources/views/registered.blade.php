@extends('layouts.app')
@section('title', 'Вы зарегистрированы!')

@section('content')
<div class="registered-wrap">
    <div class="million-panel">
        <div class="million-panel-heading" style="font-weight: 700;">
            <h3>Регистрация успешна</h3>
        </div>
        <div class="million-panel-body">
            <h4>Теперь можно продолжить работу с сайтом</h4>
            <p>Если Вы еще не до конца ознакомились с процессом подачи объявления - для продолжения чтения перейдите на <a href="{{route('home')}}">

                    <button class="btn btn-info-color">Главную страницу</button>
                </a></p>
            <p>Если Вы уже готовы создавать свое объявление - <a href="{{route('create-ad')}}">
                    <button class="btn btn-info-color">Создать объявление</button>
                </a></p>
            <p>Если Вам нужна визитка - <a href="{{route('createvisit')}}">
                    <button class="btn btn-info-color">Создать визитку</button>
                </a></p>
                <p>
                    Перейти в личный кабинет
                <a href="{{ route('profile') }}">
                <button class="btn btn-info-color">
                    Перейти в  кабинет
                </button>
               </a>
                </p>
            <p></p>
            <p>Все эти ссылки также присутствуют в меню сайта</p>
        </div>
    </div>
</div>
@endsection
