@extends('layouts.app')
@section('title', 'Вопросы и ответы')

@section('content')
    <div class="artily-support-wrapper">

        <div class="artily-support-head">
            <div>
                <h3>Вопросы и Ответы</h3>
                <p>Кликните на вопрос, чтобы прочесть ответ</p>
            </div>
            <button class="btn btn-main-color show-form">Задать вопрос</button>
        </div>

        <div class="">

            <div class="artily-support-list">
                <ol class="bookms"
                    id="faqviewhref"
                    data-href="{{ route('save.faq.view')}}"
                    data-csrf-token="{{csrf_token()}}">
                    @php
                        $clsnum = 0;
                    @endphp

                    @foreach($pageslist as $page)
                        <li class="art-list-item ques-line" id="{{ $page->id }}">
                            <div class="artily-list-row">
                                <div class="artily-listrow-content">{{ $page->question }}</div>
                                <div class="artily-bord-left">
                                    <span class="supp-eye">&#128065;</span>
                                    <span class="views" id="view_{{$page->id}}">{{ $page->views }}</span>
                                </div>
                            </div>
                            <div class="faq-answer hide" id="answer_{{ $page->id}}">
                                <div>
                                    @if($page->screenshot_q)
                                        <img src="{{asset($page->screenshot_q)}}" alt="">
                                    @endif
                                    <div class=""><em>Ответ:</em><br>
                                        <p>{{ $page->answer }}</p>
                                        @if($page->screenshot_a)
                                            <img src="{{asset($page->screenshot_a)}}">
                                        @endif
                                    </div>
                                    <div class="like-vert bord-left" title="Полезный">
                                        <span {{--class="arrow"--}}>&#128077;</span>
                                        <span class="usefulness" id="like_{{$page->id}}">{{ $page->useful }}</span>
                                    </div>
                                </div>
                        </li>
                    @endforeach

                </ol>
            </div>

        </div>


        <div id="popup" class="artily-popup hide">
            <div class="askques" id="sqhref" data-href="{{route('save-support-question')}}">

                <h4>Задать вопрос</h4>
                <form class="artily-faq-form" action="" enctype="multipart/form-data" method="post" id="quesform">
                    <textarea name="question" id="question" class="" placeholder="Введите Ваш вопрос" required>
                    </textarea>
                    <div class="artily-file-upload">
                        <label class="artily-upload-label" for="screenshot_q" id="screenshot_l" "
                        data-csrf-token="{{csrf_token()}}">
                        Если необходимо, загрузите картинку (скриншот и т.п.)<br>
                        Выберите файл
                        </label>
                        <input type="file" name="screenshot_q" id="screenshot_q" accept="image/*" value="">
                        <span class="artily-upload-img" id="imgWrapper"><img src="" alt="" id="uploaded"></span>
                    </div>

                    <div class="faq_buttons">

                        <div><input type="submit" class="btn btn-main-color faq-submit" id="faq_subm" value="ОТПРАВИТЬ">
                        </div>
                        <div><input type="reset" class="btn btn-reset-color faq-escape" id="faq_esc" value="ОТМЕНИТЬ">
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <h5 class="color-accept-bright hide" id="resolve">Ваш вопрос успешно отослан менеджеру. Ответ будет дан
            вскоре.</h5>
        <h5 class="color-orangered hide" id="reject">Не удалось отослать Ваш вопрос. Пожалуйста повторите еще раз
            позже.</h5>
    </div>
    <div class="pages-links">{{ $pageslist->links() }}</div>
@endsection
@push('scripts')
    <script src="/js/support-faq.js" defer></script>
@endpush
