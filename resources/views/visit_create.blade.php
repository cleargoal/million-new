@extends('layouts.app')
@section('title', 'Создание визитки')

@section('content')
    <div class="visit-creation" id="visit-post">
        <div id="view-zit" class="view-zit__inner">
            <form method="post" action="{{route('savevisit')}}" id="createvisit" enctype="multipart/form-data">
                <!-- left column start-->

                <div class="visit-creation__wrapp">
                    <div class="borders">
                        <div class="ava-wrap"><img id="avatimg" src="/img/avatar.png"/></div>
                        <div id="visit-avat-div">
                            <div style="font-size: small;">Загрузите Ваш аватар. Лучше всего подойдет квадратная
                                картинка.
                            </div>
                            <input type="file" id="visitavatar" name="avatar" accept="image/*"
                                   onchange="document.getElementById('avatimg').src = window.URL.createObjectURL(this.files[0])">
                            {{ csrf_field() }}
                        </div>
                    </div>

                </div>
                <div class="visit-creation-inner">
                    <div class="creation-download">
                        <div class="borders visit-creation__video">
                            <div>Прикрепить видео с Ютуба:</div>
                            <input type="text" name="videos[]" class="creation__video--input"
                                   placeholder="Введите ссылку на Ютуб видео или оставьте поле пустым"/>
                            <input type="text" name="videos[]" class="creation__video--input"
                                   placeholder="Введите ссылку на Ютуб видео или оставьте поле пустым"/>
                            <input type="text" name="videos[]" class="creation__video--input"
                                   placeholder="Введите ссылку на Ютуб видео или оставьте поле пустым"/>
                        </div>

                        <div class="visitpictures">
                            @error('pictures.*')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{  $errors->first('pictures.*') }}</strong>
                            </span>
                            @enderror
                            <div class="visitpictures-inner">
                                <div class="borders visitpictures-card visitpictures-card_flex">
                                    <div style="width: 200px; font-size: small;">Загрузите картинку
                                    </div>
                                    <div class="visitpictures-row">
                                      <img src="{{asset('img/empty-e.png')}}" id="pic1" alt="" class="img-edit-view">
                                      <input type="file" class="visitpictures-edit--input" name="pictures[]"
                                             accept="image/*"
                                             onchange="document.getElementById('pic1').src = window.URL.createObjectURL(this.files[0])">
                                    </div>
                                </div>
                                <div class="borders visitpictures-card visitpictures-card_flex">
                                    <div style="width: 200px; font-size: small;">Загрузите картинку
                                    </div>
                                    <div class="visitpictures-row">
                                      <img src="{{asset('img/empty-e.png')}}" id="pic2" alt="" class="img-edit-view">
                                      <input type="file" class="visitpictures-edit--input" name="pictures[]"
                                             accept="image/*"
                                             onchange="document.getElementById('pic2').src = window.URL.createObjectURL(this.files[0])">
                                    </div>
                                </div>
                                <div class="borders visitpictures-card visitpictures-card_flex">
                                    <div style="width: 200px; font-size: small;">Загрузите картинку
                                    </div>
                                    <div class="visitpictures-row">
                                      <img src="{{asset('img/empty-e.png')}}" id="pic3" alt="" class="img-edit-view">
                                      <input type="file" class="visitpictures-edit--input" name="pictures[]"
                                             accept="image/*"
                                             onchange="document.getElementById('pic3').src = window.URL.createObjectURL(this.files[0])">
                                    </div>
                                </div>
                                <div class="borders visitpictures-card visitpictures-card_flex">
                                    <div style="width: 200px; font-size: small;">Загрузите картинку
                                    </div>
                                    <div class="visitpictures-row">
                                      <img src="{{asset('img/empty-e.png')}}" id="pic4" alt="" class="img-edit-view">
                                      <input type="file" class="visitpictures-edit--input" name="pictures[]"
                                             accept="image/*"
                                             onchange="document.getElementById('pic4').src = window.URL.createObjectURL(this.files[0])">
                                    </div>
                                </div>
                                <div class="borders visitpictures-card visitpictures-card_flex">
                                    <div style="width: 200px; font-size: small;">Загрузите картинку
                                    </div>
                                    <div class="visitpictures-row">
                                      <img src="{{asset('img/empty-e.png')}}" id="pic5" alt="" class="img-edit-view">
                                      <input type="file" class="visitpictures-edit--input" name="pictures[]"
                                             accept="image/*"
                                             onchange="document.getElementById('pic5').src = window.URL.createObjectURL(this.files[0])">
                                    </div>
                                </div>

                                <div id="visitstyles" class="borders visit-styles visitpictures-card">
                                    <div>Выбор цвета:</div>
                                    <div style="display: inline-block;">
                                        <label for="style-back" style="display: block;">Фон</label>
                                        <input type="color" class="visitcolor" value="#f5f3e0" name="style-back"
                                               id="style-back" title="Цвет фона">
                                    </div>
                                    <div style="display: inline-block;">
                                        <label for="style-font" style="display: block;">Текст</label>
                                        <input type="color" class="visitcolor" name="style-font" id="style-font"
                                               title="Цвет текста">
                                    </div>
                                </div>
                            </div>
                            <!-- left column end -->
                        </div>
                    </div>
                    <!-- left column end -->

                    {{-- rihgt column start--}}
                    <div class="visbody visbody-creation">
                        <div class="visbody-inner">
                            <div class="visbody-header">
                                <div class="visbody-header__title">
                                    <div class=" viz-header">
                                        <textarea name="header" id="visitheader" rows="3" maxlength="80" autofocus
                                                  required
                                                  placeholder="Заголовок до 80 символов (обязательное поле)"></textarea>
                                        <div id="left-symbols">Осталось <span id="symbols">80</span> символов</div>
                                    </div>
                                </div>
                                <div class="visbody-header__prise">
                                    <input type="number" id="visitprice" name="price" placeholder="Цена" min="1"
                                           max="99999"
                                           required>
                                    <select name="currency" id="currency" required>
                                        <option value="Руб">Руб</option>
                                        <option value="Грн">Грн</option>
                                        <option value="USD">USD</option>
                                        <option value="EURO">EURO</option>
                                        <option value="Тенге">Тенге</option>
                                    </select>
                                </div>
                            </div>
                            <div class="visidescr">
                        <textarea id="visitdescr" name="description" required placeholder="Описание Вашего продукта, услуги, которые Вы предлагаете или того, что Вы хотите купить, найти, заказать, приобрести или даже получить в подарок.
						<!!!>До 1500 знаков с пробелами. ВНИМАНИЕ! В этой форме НЕ ВСЕ ПОЛЯ ОБЯЗАТЕЛЬНЫ, но Вы сами заинтересованы заполнить все как можно лучше.">
                            Описание Вашего продукта, услуги, которые Вы предлагаете или того, что Вы хотите купить, найти, заказать, приобрести или даже получить в подарок.
                            <!!!>До 1500 знаков с пробелами.
                            ВНИМАНИЕ! В этой форме НЕ ВСЕ ПОЛЯ ОБЯЗАТЕЛЬНЫ, но Вы сами заинтересованы заполнить все как можно лучше.</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="contacts-group" class="contacts-group visit-creation__contact borders">
                    <div class="cont-contact">
                        <div class="cont-contact__title">
                            <span class="und">Контакты</span>:
                        </div>
                        <div class="contacts-group__inner">
                            <div>
                                <input type="email" id="email" name="email" class="cont-contact__email"
                                       placeholder="е-мэйл, обязательно"
                                       required>
                                <input type="tel" id="phone" name="phone" class="cont-contact__tel" maxlength="15"
                                       placeholder="Номер телефона в международном формате" required>
                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div>
                                <textarea type="text" id="visitcontacts" name="contacts" maxlength="50"
                                          placeholder="Введите любые Ваши контакты если нужны, кроме е-мэйл и телефон, введенных ранее"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="cont-reference">
                        <div class="cont-reference__title">
                            <span class="und">Ссылки (через запятую)</span>:
                        </div>
                        <div>
                                <textarea type="text" id="visitlinks" name="links"
                                          placeholder="Введите ссылки, которые нужны, через запятую, или оставьте поле пустым"></textarea>
                        </div>
                        <div style="padding: 6px;" class="savecard-wrapp">
                            <input type="submit" id="savecard" value="Отправить" style="float: none;">
                        </div>
                    </div>
                </div> <!-- row -->
            </form>
        </div>
    </div>
    <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
    <script>
        const editor = CKEDITOR.replace('visitdescr');

        editor.on( 'required', function(evt) {
            editor.showNotification( 'This field is required.', 'warning' );
            evt.cancel()
        } )
    </script>
@endsection
@push('scripts')
    <script src="\js\visit-create.js"></script>
@endpush
