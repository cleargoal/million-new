<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | Миллион просмотров объявления гарантировано</title>
    <!-- Open graph section -->
    <meta property="og:title" content="{{$ogData['og_title']}}"/>
    <meta property="og:url" content="{{request()->fullUrl()}}"/>
    <meta property="og:image" content="{{$ogData['og_image']}}"/>
    <meta property="og:description" content="{{$ogData['og_description']}}"/>
    <meta property="og:locale" content="ru_RU"/>
    <meta property="og:site_name" content="Миллион просмотров Вашего объявления"/>
    <!-- Open graph end -->

    <link rel="icon" href="/img/favicon.png">

    <link href="/css/style.css" rel="stylesheet" as="style" prefetch>

    <script>window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token(),]); ?></script>
</head>

<body id="app-layout">
<div id="app" class="app-page">

    <div>
        @include('includes.navigation')
    </div>

    <div class="artily-main-wrapper">
        @yield('content')
        <div class="footerblock"></div>
    </div>

    <div class="">
        @include('includes.footer')
    </div>
</div>

<!-- JavaScripts -->
<script src="/js/general.js" defer></script>
<script src="/js/ie10-viewport-bug-workaround.js" async></script>
@stack('scripts')
<script src="/js/analyticstracking.js" async></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</body>

</html>
