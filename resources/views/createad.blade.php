@extends('layouts.app')
@section('title', 'Создать свое объявление')

@section('content')
    <div class="create-complement">
        <div class="million-panel">
            <div class="million-panel-heading">
                <h4>Создание объявления</h4>
            </div>
            <div class="million-panel-body">
                @if ($mode == 'e' && (!isset($editad) || is_null($editad)))
                    <div>
                        <h5>Неправильная ссылка!</h5>
                        <p>Перейдите в свой Профиль и попробуйте снова.</p>
                    </div>
                @else
                    <div class="row" id="construct-area">
                        @php
                            if ($mode == 'e') {
                            $action = 'update-ad';
                            }
                            else {
                            $action = 'do-create-ad';
                            }
                        @endphp
                        <form method="post" action="{{ route($action) }}">
                            {{ csrf_field() }}
                            <div class="col-md-10">
                                <textarea name="adtext" id="complement-text" required autofocus
                                          @if($mode !=='e')placeholder="Ваш текст - до 160 символов" @endif
                                          maxlength="160">@if($mode == 'e'){{$editad->adtext}}@endif</textarea>
                                {{-- ATTENTION! Do not insert any breaks into this textarea --}}
                                <div style="font-size: 75%; margin: -4px 0 -10px;">Символов:
                                    @php
                                        if($mode == 'e') $mstr = mb_strlen ($editad->adtext);
                                        else $mstr = 0;
                                        $lstr = 160-$mstr;
                                    @endphp
                                    введено - <span id="complement-type">{{$mstr}}</span>,
                                    осталось - <span id="complement-left">{{$lstr}}</span>
                                </div>
                                <br>
                                <input type="text" name="adlink" id="complement-link" placeholder="ссылка на Ваш ресурс"
                                       value="@if($mode == 'e'){{$editad->adlink}}@endif">
                            </div>
                            <div class="col-md-2 created-options ptb" @if($mode=='e' ) id="edit-styles" data-stls="{{$editad->adstyle}}" data-fnts="{{$editad->adfont}}@endif">
                                <select name="adstyle" id="select-styles"></select>
                                <select name="adfont" id="fsize">
                                    <option value="2em">Шрифт малый</option>
                                    <option value="2.4em">Шрифт средний</option>
                                    <option value="3em">Шрифт большой</option>
                                </select>
                                <div>
                                    <label for="no-link">
                                        <input type="checkbox" id="no-link" @if ($mode=='e' && empty($editad->adlink)) checked @endif>
                                        Без ссылки
                                    </label>
                                </div>
                                <input type="submit" id="complement-submit" class="btn btn-main-color" value="Сохранить" @if(empty($editad->adlink))
                                title="Введите данные в поля <Текст> и <Ссылка>" disabled @endif >
                            </div>
                            @if($mode == 'e')
                                <input type="hidden" name="id" value="{{$editad->id}}">
                            @endif
                        </form>
                    </div>
                    <div class="row">
                        <div class="view-area" id="view-area">
                            <div id="complement-view-area" @if($mode == 'e')@endif>
                                @if($mode == 'e'){{$editad->adtext}}@elseОбласть просмотра объявления@endif
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="/js/create-ad.js" defer></script>
@endpush
