@extends('layouts.app')
@section('title', 'Способ оплаты выбран')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Оплата</h4>
					</div>
					<div class="panel-body">
						@foreach($paymentserv as $servline)
							@endforeach
						<div>Вы выбрали метод оплаты: <span class="bold-ital">{{$servline['name']}}</span></div>
					</div>
					<div class="panel-body">
						<div></div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
