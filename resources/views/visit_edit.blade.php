@extends('layouts.app')
@section('title', 'Редактирование визитки')

@section('content')
    <div class="visit-creation" id="visit-post" style="background-color: {{$styleBack}}; color: {{$styleFont}};">
        <div id="view-zit" class="view-zit__inner">
            <form method="post" action="{{route('update-visit')}}" id="edit-visit" enctype="multipart/form-data">
                <!-- left column start-->

                <div class="visit-creation__wrapp">
                    <div class="borders">
                        @if (empty($viewCard->avatar))
                            <div class="ava-wrap">
                                <img id="avatimg" src="/img/avatar-min.png" alt="avatar"/>
                            </div>
                        @else
                            <div class="vis-avatar">
                                <img id="avatimg" src="{{asset('storage/avatars/' . $viewCard->avatar)}}" alt="avatar"/>
                            </div>
                        @endif
                        <div id="visit-avat-div">
                            <div style="font-size: small;">Загрузите Ваш аватар. Лучше всего подойдет квадратная
                                картинка.
                            </div>
                            @php $avats = $mode == 'e' ? $viewCard->avatar : ''; @endphp
                            <input type="file" id="visitavatar" name="avatar" value="{{$avats}}" accept="image/*"
                                   onchange="document.getElementById('avatimg').src = window.URL.createObjectURL(this.files[0])">
                            {{ csrf_field() }}
                            <input type="hidden" name="visi_slug" value="{{$viewCard->visi_slug}}">
                        </div>
                    </div>
                </div>

                <div class="visit-creation-inner">
                    <div class="creation-download">
                        <div class="borders visit-creation__video">
                            <div>Прикрепить видео:</div>
                            @for($key = 0; $key < 3; $key++)
                                <input type="text" name="videos[]" class="creation__video--input"
                                       @if(!empty($videoArray[$key])) value="{{$videoArray[$key]}}" @endif
                                       placeholder="Введите ссылку на Ютуб видео или оставьте поле пустым"/>
                            @endfor
                        </div>

                        <div class="visitpictures">
                            <div class="visitpictures-inner">

                                @for($key = 0; $key < 5; $key++)
                                    <input type="hidden" name="returned_pics[{{$key}}]"
                                           value="{{$pictures[$key]?? ''}}">
                                    <div class="borders visitpictures-card">
                                        <div style="width: 200px; font-size: small;">Загрузите картинку</div>

                                        <input type="file" class="visitpictures-edit--input" name="pictures[]"
                                               accept="image/*"
                                               onchange="document.getElementById('pic{{$key+1}}').src = window.URL.createObjectURL(this.files[0])"/>

                                        @php
                                            if(empty($pictures[$key])) {
                                           $pictures[$key] = asset('img/empty-e.png');
                                        }
                                        @endphp
                                        <img src="{{$pictures[$key]}}" id="pic{{$key+1}}" alt="" class="img-edit-view">

                                    </div>

                                @endfor

                                <div id="visitstyles" class="borders visit-styles visitpictures-card">
                                    <div>Выбор цвета:</div>
                                    <div style="display: inline-block;">
                                        <label for="style-back" style="display: block;">Фон</label>
                                        <input type="color" class="visitcolor" value="{{$styleBack}}" name="style-back"
                                               id="style-back" title="Цвет фона">
                                    </div>
                                    <div style="display: inline-block;">
                                        <label for="style-font" style="display: block;">Текст</label>
                                        <input type="color" class="visitcolor" value="{{$styleFont}}" name="style-font"
                                               id="style-font" title="Цвет текста">
                                    </div>
                                </div>
                            </div>
                            <!-- left column end -->

                        </div>
                    </div>
                    <!-- left column end -->

                    {{-- rihgt column start--}}
                    <div class="visbody visbody-creation">
                        <div class="visbody-inner">
                            <div class="visbody-header">
                                <div class="visbody-header__title">
                                    <div class=" viz-header">
                                        <textarea name="header" id="visitheader" rows="3" maxlength="80" autofocus
                                                  required
                                                  placeholder="Заголовок до 80 символов (обязательное поле)">{{$viewCard->header}}</textarea>
                                        <div id="left-symbols">Осталось <span
                                                id="symbols">{{80 - Str::length($viewCard->header)}}</span> символов
                                        </div>
                                    </div>
                                </div>
                                <div class="visbody-header__prise">
                                    <input type="number" id="visitprice" name="price" placeholder="Цена" min="1"
                                           max="99999" value="{{$viewCard->price}}"
                                           required>
                                    <select name="currency" id="currency" required>
                                        <option value="Руб" @if($viewCard->currency === "Руб") selected @endif>Руб
                                        </option>
                                        <option value="Грн" @if($viewCard->currency === "Грн") selected @endif>Грн
                                        </option>
                                        <option value="USD" @if($viewCard->currency === "USD") selected @endif>USD
                                        </option>
                                        <option value="EURO" @if($viewCard->currency === "EURO") selected @endif>EURO
                                        </option>
                                        <option value="Тенге" @if($viewCard->currency === "Тенге") selected @endif>
                                            Тенге
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="visidescr">
                                <textarea id="visitdescr" name="description" required>
                                    {!!$viewCard->description!!}
                                </textarea>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="contacts-group" class="contacts-group visit-creation__contact borders">
                    <div class="cont-contact">
                        <div class="cont-contact__title">
                            <span class="und">Контакты</span>:
                        </div>
                        <div class="contacts-group__inner">
                            <div>
                                <input type="email" id="email" name="email" class="cont-contact__email"
                                       placeholder="е-мэйл, обязательно" required value="{{$viewCard->email}}">
                                <input type="tel" id="phone" name="phone" class="cont-contact__tel" maxlength="15"
                                       placeholder="телеф/вайбер/телеграм/вотсап - 1 номер" required
                                       value="{{$viewCard->phone}}">
                            </div>
                            <div>
                                <textarea type="text" id="visitcontacts" name="contacts" maxlength="50"
                                          placeholder="Введите любые Ваши контакты если нужны, кроме е-мэйл и телефон, введенных ранее (до 50 символов)">
{{$viewCard->contacts}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="cont-reference">
                        <div class="cont-reference__title">
                            <span class="und">Ссылки (через запятую)</span>:
                        </div>
                        <div>
                                <textarea type="text" id="visitlinks" name="links"
                                          placeholder="Введите ссылки, которые нужны, через запятую, или оставьте поле пустым">
{{$viewCard->links}}
                                </textarea>
                        </div>
                        <div style="padding: 6px;" class="savecard-wrapp">
                            <input type="submit" id="savecard" style="float: none;">
                        </div>
                    </div>
                </div> <!-- row -->
            </form>
        </div>
    </div>
    <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('visitdescr');
    </script>
@endsection
@push('scripts')
    <script src="\js\visit-create.js"></script>
@endpush
