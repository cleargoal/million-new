@extends('layouts.app')
@section('title', 'Просим подтвердить емэйл')

@section('content')
	<div class = "verify-email-wrap">
		<div class = "million-panel">
			<div class = "million-panel-heading">
				<h3>Подтвердите свой емэйл</h3>
			</div >
			<div class = "million-panel-body" >
                <h4>Вам отослано письмо с кодом подтверждения</h4>
                <p>Это письмо должно прийти на Ваш зарегистрированный емэйл.</p>
                <p>В письме есть кнопка и ссылка, выполняющие одинаковое действие - переход на нашу страницу подтверждения емэйл адреса.</p>
                <p>После этого Вы сможете войти в свой личный кабинет на нашем сайте и продолжить работу.</p>
			</div >
		</div >
	</div >
@endsection
