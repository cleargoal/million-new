@extends('layouts.app')
@section('title', 'Выбор способа оплаты')

@section('content')
	<div class = "col-md-10 col-md-offset-1" >
		<div class = "panel panel-default" >
			<div class = "panel-heading">
				<h4>Выбор способа оплаты</h4>
			</div >
			<div class = "panel-body" >
				<table>
					<thead >
					<td >Сервис</td >
					<td >Описание</td >
					<td >Стоимость</td >
					<td >Выбрать</td >
					</thead >
					<tbody >
					@foreach($allpays as $payser)
						<tr >
							<td class="payservices">{{ $payser->name }}</td >
							<td class="payservices">{{ $payser->descript }}</td >
							<td >{{ $payser->cost }}</td >
							<td >
								<button class = "choose-pay-method" value = "{{ $payser->payslug }}"
					        onclick = "window.location.href='{{ route('route-payment', $payser->payslug) }}'" >
									Оплатить
								</button >
							</td >
						</tr >
					@endforeach
					</tbody >
				</table >
			</div >
		</div >
	</div >
@endsection
