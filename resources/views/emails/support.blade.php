<html>
<head>
    <style>
        body {
            font-family: Comfortaa, 'Open Sans', Arial, sans-serif;
            padding: 0 2rem;
            width: 900px;
            margin: 0 auto;
            text-align: center;
        }

        .main {
            padding: 2rem;
        }

        .above {
            color: #888;
            font-weight: bold;
        }

        .below {
            color: #444;
        }
    </style>
</head>
<body>
<div style="display: table; width: 100%; height: 60px; color: #6b37c2; margin-top: 20px; font-size: 3.6rem;">
    <h3 style="display: table-cell; vertical-align: middle; font-weight: 100;">Получен ответ на Ваш вопрос</h3>
</div>

<div class="main">
    <h2>Вопрос ({{ $supp_faq->created_at }}):</h2>
    <p class="above">{{ $supp_faq->question }}</p>
    <h2>Ответ ({{ $supp_faq->updated_at }}):</h2>
    <p class="above">{{ $supp_faq->answer }}</p>
</div>

<hr style="border-top: 1px #47aae3 solid; margin: 30px auto 5px; width: 16%;">
</body>
</html>
