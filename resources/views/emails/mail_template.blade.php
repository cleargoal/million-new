<html>
<head>
    <style>
        body {font-family: Gilroy, 'Open Sans', Arial, sans-serif; padding: 0 2rem; width: 900px; margin: 0 auto; text-align: center;}
        .main {padding: 2rem;}
        .above {color: #888; font-weight: bold;}
        .below {color: #444;}
    </style>
</head>
<body>
<div style="display: table; width: 100%; height: 60px; color: #6b37c2; margin-top: 20px; font-size: 3.6rem;">
    <h3 style="display: table-cell; vertical-align: middle; font-weight: 100;">{{$maildata['header']}}</h3>
</div>
<hr style="border-top: 1px #47aae3 solid; margin: 30px auto 5px; width: 16%;">

<div class="main">
    <h2>{{$maildata['title']}}</h2>
    <p class="above">{{$maildata['text1']}}</p>
    <p class="above">{{$maildata['text2']}}</p>
    
    <div>
        <a href="{{route($maildata['linkparams'])}}">
            <div style="display: inline-block; font-size: 1.5rem; padding: 20px 3rem; text-align: center; background: linear-gradient(to bottom, #47aae3, #2588c1); color: #fff; border-radius: 10px; margin: 1rem auto; cursor: pointer;">
                {{$maildata['button']}}
            </div>
        </a>
    </div>
    <p class="below">{{$maildata['text3']}}</p>
    <p class="below">{{$maildata['text4']}}</p>
</div>

<div style="display: table; width: 100%; text-align: center; height: 40px; color: #444; font-size: 11px;">
    <div style="display: table-cell; vertical-align: middle;">
        <a href="{{route('home')}}" style="color: inherit;" target="_blank">{{$maildata['footer']}}</a>
    </div>
</div>
</body>
</html>
