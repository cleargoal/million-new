<html>
<head>
    <style>
        body { font-family: 'Open Sans', Arial, sans-serif; padding: 0 2rem;}
    </style>
</head>
<body>
<div style="display: table; width: 100%; text-align: center; height: 60px; background: #002a80; color: #fff; font-weight: bold; margin-top: 20px;
        border-top-left-radius:7px; border-top-right-radius: 7px;">
    <h4 style="display: table-cell; vertical-align: middle;">Вы зарегистрированы!</h4>
</div>
<h5>Вы зарегистрированы в сервисе "Миллион показов"</h5>
<p></p>

<div>
    <div style="width: 50%; padding: 30px 0; text-align: center; background-color: #008a48; color: #fff; border-radius: 7px; margin: auto auto; cursor: pointer;">
        {{--<a href="http://million-ru.topearning.space" style="color: inherit;">--}}
        <a href="{{route('profile')}}" style="color: inherit;">
            Пожалуйста, подтвердите Ваш е-мэйл
        </a>
    </div>
</div>
<p>После этого Вы сможете войти в кабинет и получить свою ссылку.</p>
<p></p>

<div style="display: table; width: 100%; text-align: center; height: 40px; background: #40aae7; color: #fff; font-size: 14px; font-weight: bold;
        border-bottom-left-radius: 7px; border-bottom-right-radius: 7px;">
    <div style="display: table-cell; vertical-align: middle; ">
        {{--<a href="http://million-ru.topearning.space" style="color: inherit;" target="_blank">Миллион показов (С)</a>--}}
        <a href="{{route('home')}}" style="color: inherit;" target="_blank">Миллион показов (С)</a>
    </div>
</div>
</body>
</html>
