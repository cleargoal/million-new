@extends('layouts.app')
@section('title', 'Домашняя страница')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">Успешно</div>

					<div class="panel-body">
						Вход выполнен!
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
