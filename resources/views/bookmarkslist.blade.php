@extends('layouts.app')
@section('title', 'Список закладок')

@section('content')

    <div class="artily-bookmarksout-wrapper">
        <div class="artily-bookmarks-wrapper">

            <div class="artily-bookmarks-head">
                @php
                    $pageTitle = count($listbookmarks) < 1 ? 'Вы не сделали закладки' : 'Просмотрите свои закладки' ;
                @endphp
                <h3>{{$pageTitle}}</h3>
            </div>

            <div class="artily-bookmarks-list">
                <div class="bmrkline bml" id="listBooks" data-csrf-token="{{csrf_token()}}"
                     data-href="{{route('loglinkbmk')}}">
                    <ol class="bookms">
                        @foreach($listbookmarks as $bookmline)
                            <li class="art-list-item ques-line">
                                <!-- <li class="bookmark-list-li"> -->
                                @if (!empty($bookmline['adlink']))
                                    <a href="{{$bookmline['adlink']}}" target="_blank" data-adid="{{$bookmline['id']}}"
                                       class="adid">
                                        <div>{{$bookmline['adtext']}}</div>
                                    </a>
                                @else
                                    <span class="adid">{{$bookmline['adtext']}}</span>
                                @endif
                            </li>
                        @endforeach
                    </ol>
                </div>
            </div>

        </div>
        @if($showBtnCreate === true)
            <div class="artily-create-ad">
                <a href="{{route('create-ad')}}">
                    <button class="btn btn-info-color">Создать свое объявление</button>
                </a>
            </div>
        @endif
    </div>

    <script>
        const listBooks = document.querySelector("#listBooks");
        const saveLinkRoute = listBooks.getAttribute("data-href");
        const token = listBooks.getAttribute("data-csrf-token");
        const allAdid = document.querySelectorAll(".adid");
        allAdid.forEach(function (item) {
            const adId = item.getAttribute("data-adid");
            item.addEventListener("click", function () {
                axios.post(saveLinkRoute, {
                    adv_id: adId
                }, {
                    headers: {
                        "X-CSRF-TOKEN": token
                    }
                });
            });
        });
    </script>
@endsection
