@extends('layouts.app')
@section('title', 'Кабинет пользователя')

@section('content')
    {{--<div class="container">--}}
    <div class = "row" >
        <div class = "col-md-10 col-md-offset-1" >
            <div class = "panel panel-default" >
                <div class = "panel-heading" ><h3>Профиль пользователя: {{$user->name}}</h3></div >
                <div class = "panel-body" >
                    <div class="row">
                        <div class="col-md-2">
                            <ul class="nav nav-tabs profile-tabs" role="tablist">
                                <li data-id="#tab-1" class="active"><a href="#tab-1">Обзор</a></li>
                                <li data-id="#tab-2"><a href="#tab-2">Ваши объявления</a></li>
                                <li data-id="#tab-3"><a href="#tab-3">Ваши визитки</a></li>
                                <li data-id="#tab-4"><a href="#tab-4">Ваши платежи</a></li>
                                <li data-id="#tab-5"><a href="#tab-5">Ваши просмотры закладок</a></li>
                                <li data-id="#tab-6"><a href="#tab-6">Вы просматривали визитки</a></li>
                                <li data-id="#tab-7"><a href="#tab-7">Ваша аудитория</a></li>
                            </ul>
                        </div>
                        <div class="col-md-10">
                            <div class="prof-tab-content tab-pane show" id="tab-1">
                                <h4>Обзор. Статистика обновляется 1 раз в сутки</h4>
                                <table class="mb20">
                                    <thead>
                                    <tr>
                                        <th>Ваши данные</th>
                                        <th>кол-во</th>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td>Объявления</td>
                                        <td>{{$ads->count()}}</td>
                                    </tr>
                                    <tr>
                                        <td>Просмотры объявлений</td>
                                        <td>{{$lga->where('log_slug', 'vad')->count()}}</td>
                                    </tr>
                                    <tr>
                                        <td>Визитки</td>
                                        <td>{{$vcs->count()}}</td>
                                    </tr>
                                    <tr>
                                        <td>Просмотры визиток</td>
                                        <td>{{$lgv->where('log_slug', 'vvp')->count()}}</td>
                                    </tr>
                                    <tr>
                                        <td>Оплачено всего, {{$paym->first()->currency}}</td>
                                        <td>{{$paym->sum('amount')}}</td>
                                    </tr>
                                </table>
                                <div class="mt10">
                                    <div>Ссылка для приглашения</div>
                                    <div class="prof-links">http://{{$_SERVER['HTTP_HOST']}}/{{$user->slug}}</div>
                                </div>
                                <div class="mt10">
                                    <div>Ссылка на список Ваших визиток</div>
                                    <div class="prof-links">http://{{$_SERVER['HTTP_HOST']}}/pageslist/{{$user->slug}}</div>
                                </div>
                                <div class="mt10">
                                    {{--<a href = "http://{{$_SERVER['HTTP_HOST']}}/pageslist/{{$user->slug}}" target="_blank">--}}
                                    <a href = "{{route('listuslug', [$user->slug])}}" target="_blank">
                                        Посмотреть свой список</a >
                                    - <span class="small">откроется в новой вкладке</span>
                                </div>
                            </div>
                            {{-- end of TAB 1 --}}
                            <div class="prof-tab-content tab-pane" id="tab-2">
                                <h5>Ваши объявления</h5>
                                <div class="table-striped-wrapper">
                                  <table class="table-striped">
                                      <thead>
                                      <tr class="prof-table-row">
                                          <td style="width: 72%;">Текст</td>
                                          <td style="width: 7%; word-break: break-all;">Просмотры</td>
                                          <td style="width: 7%; word-break: break-all;">Закладки</td>
                                          <td style="width: 7%; word-break: break-all;">Переходы</td>
                                          <td style="width: 7%; word-break: break-all;">Действие</td>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      @foreach($ads as $ad)
                                          <tr class="prof-table-row">
                                              @php
                                                  $adid = $ad->id;
                                              @endphp
                                              <td class="prof-table-td">{{$ad->adtext}}</td>
                                              <td>{{$lga->where('log_slug', 'vad')->where('src_id', $adid)->count()}}</td>
                                              <td>{{$lga->where('log_slug', 'bmk')->where('src_id', $adid)->count()}}</td>
                                              <td>{{$lga->where('log_slug', 'lnk')->where('src_id', $adid)->count()}}</td>
                                              <td>
                                                  <a href="{{route('edit-ad', ['adid' => $adid])}}"><div class="btn-info btn btn-edit" id="">Редактир.</div></a>
                                              </td>
                                          </tr>
                                      @endforeach
                                      </tbody>
                                  </table>
                                </div>
                            </div>
                            {{-- end of TAB 2 --}}
                            <div class="prof-tab-content tab-pane" id="tab-3">
                                <h5>Ваши визитки</h5>
                                <table>
                                    <thead>
                                    <tr>
                                        <td>Заголовок</td>
                                        <td>Просмотры</td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($vcs as $vc)
                                        <tr>
                                            <td><a href = "{{ route('showvslug', ['vicard' => $vc->visi_slug]) }}" target="_blank" >{{$vc->header}}</a ></td>
                                            <td>{{$lgv->where('src_id', $vc->id)->count('src_id')}}</td>
                                            <td><a href="{{ route('showvslug', ['vicard' => $vc->visi_slug, 'mode' => 'e']) }}">
                                                    <div class="btn-info btn btn-edit" data-href="">Редактировать</div></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{-- end of TAB 3 --}}
                            <div class="prof-tab-content tab-pane" id="tab-4">
                                <h5>Ваши платежи</h5>
                                <table>
                                    <thead>
                                    <tr>
                                        <td>Дата платежа</td>
                                        <td>Валюта</td>
                                        <td>Сумма</td>
                                        <td>Продукт</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($paym as $pam)
                                        <tr>
                                            <td>{{$pam->created_at}}</td>
                                            <td>{{$pam->currency}}</td>
                                            <td>{{$pam->amount}}</td>
                                            @php$prod = config('settings.'. $pam->product);@endphp
                                            <td>{{$prod[0]}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{-- end of TAB 4 --}}
                            <div class="prof-tab-content tab-pane" id="tab-5">
                                <h5>Вы просматривали закладки</h5>
                                <table class="bml" data-csrf-token="{{csrf_token()}}" data-href="{{route('loglinkbmk')}}">
                                    <thead>
                                    <tr>
                                        <td>Текст объявления</td>
                                        <td><img src = "img/eye7w.png" alt = "Видел" title="Вы просматривали ... раз" class="profeye"></td>
                                        <td><span class="gotolink" title="Перейти по ссылке">>></span></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($lbls as $lba)
                                        <tr>
                                            <td>{{$lba->adtext}}</td>
                                            <td>{{$lba->grp_ads}}</td>
                                            <td><a href = "{{$lba->adlink}}" target="_blank" data-adid="{{$lba->src_id}}" class="adid">
                                                    <span class="gotolink" title="Открыть ссылку в новой вкладке">>></span>
                                                </a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{-- end of TAB 5 --}}
                            <div class="prof-tab-content tab-pane " id="tab-6">
                                <h5>Вы просматривали визитки</h5>
                                <table class="visit-table">
                                    <thead>
                                    <tr>
                                        <td>Заголовок визитки</td>
                                        <td>Цена в визитке</td>
                                        <td><img src = "img/eye7w.png" alt = "Видел" title="Вы просматривали ... раз" class="profeye"></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($lbvs as $lbv)
                                        <tr>
                                            <td><a href = "{{route('showvslug', $lbv->visi_slug)}}" target="_blank">{{$lbv->header}}</a ></td>
                                            <td>{{$lbv->price}}</td>
                                            <td>{{$lbv->grp_vs}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{-- end of TAB 6 --}}
                            <div class="prof-tab-content tab-pane" id="tab-7">
                                <h5>Кто будет просматривать ваши объявления</h5>
                                <table>
                                    <thead>
                                    <tr>
                                        <td>Участники</td>
                                        <td>Реальные</td>
                                        <td>Активные</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{--@foreach ($lbvs as $lbv)--}}
                                        <tr>
                                            <td>{{$members[0]}}</td>
                                            <td>{{$members[1]}}</td>
                                            <td>{{$members[2]}}</td>
                                        </tr>
                                    {{--@endforeach--}}
                                    </tbody>
                                </table>
                                <div>
                                    <dl>
                                        <dt>Участники</dt>
                                            <dd>Люди, приглашенные Вами или Вашими последователями, зарегистрированные, но еще не продвинулись дальше</dd>
                                        <dt>Реальные</dt>
                                            <dd>Зарегистрированные и оплатившие свое объявление, как опубликовали, так и еще нет</dd>
                                        <dt>Активные</dt>
                                            <dd>Это Участники - такие, кто уже пригласил следующих (у них уже есть Участники) - это те, кто увеличивают кол-во Ваших просмотров.</dd>
                                    </dl>
                                    {{--<p>Таким образом: "Реальные" и "Активные" - это часть от "Участников".</p>--}}
                                </div>
                            </div>
                            {{-- end of TAB 7 --}}
                        </div>

                    </div>
                </div >
            </div >
        </div >
    </div >
    {{--</div>--}}
@endsection
