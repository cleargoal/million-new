@extends('layouts.app')
@section('title', 'Кабинет пользователя')

@section('content')
	{{--<div class="container">--}}
	<div class = "row" >
		<div class = "col-md-12" >
			<div class = "panel panel-default" >
				<div class = "panel-heading" style = "font-size: 130%; font-weight: 700;" >Профиль пользователя</div >
				<div class = "panel-body" >
					<form class = "form-horizontal" role = "form" method = "POST" action = "{{ route('profile') }}" >
						{{ csrf_field() }}

						<div class = "form-group{{ $errors->has('name') ? ' has-error' : '' }}" >
							<label for = "name" class = "col-md-4 control-label" >Ваше имя</label >

							<div class = "col-md-6" >
								<input id = "name" type = "text" class = "form-control" name = "name" value = "{{ old('name') }}" autofocus >

								@if ($errors->has('name'))
									<span class = "help-block" >
                                        <strong >{{ $errors->first('name') }}</strong >
                                    </span >
								@endif
							</div >
						</div >

						<div class = "form-group{{ $errors->has('email') ? ' has-error' : '' }}" >
							<label for = "email" class = "col-md-4 control-label" >E-Mail адрес</label >

							<div class = "col-md-6" >
								<input id = "email" type = "email" class = "form-control" name = "email" value = "{{ old('email') }}" disabled >

								@if ($errors->has('email'))
									<span class = "help-block" >
                                        <strong >{{ $errors->first('email') }}</strong >
                                    </span >
								@endif
							</div >
						</div >

						<div class = "form-group{{ $errors->has('password') ? ' has-error' : '' }}" >
							<label for = "password" class = "col-md-4 control-label" >Пароль</label >

							<div class = "col-md-6" >
								<input id = "password" type = "password" class = "form-control" name = "password" >

								@if ($errors->has('password'))
									<span class = "help-block" >
                                        <strong >{{ $errors->first('password') }}</strong >
                                    </span >
								@endif
							</div >
						</div >

						<div class = "form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}" >
							<label for = "password-confirm" class = "col-md-4 control-label" >Повтор пароля</label >

							<div class = "col-md-6" >
								<input id = "password-confirm" type = "password" class = "form-control" name = "password_confirmation" >

								@if ($errors->has('password_confirmation'))
									<span class = "help-block" >
                                        <strong >{{ $errors->first('password_confirmation') }}</strong >
                                    </span >
								@endif
							</div >
						</div >

						<div class = "form-group" >
							<div class = "col-md-6 col-md-offset-4" >
								<button type = "submit" class = "btn btn-primary" >
									<i class = "fa fa-btn fa-user" ></i > Регистрация
								</button >
							</div >
						</div >
					</form >
				</div >
			</div >
		</div >
	</div >
	{{--</div>--}}
@endsection
