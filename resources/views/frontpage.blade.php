@extends('layouts.app')
@section('title', 'Главная страница')
@section('content')
    <!-- ********** HEADER ********** -->

    <div class="artily-main-entry">
        <div class="artily-main-text">
            <div class="artily-text-wrapper">
                <h4 style="letter-spacing: 0.5em;">Суть нашего предложения:</h4>
                <h2>ПОЛУЧИТЕ ЗА 10 РУБЛЕЙ</h2>
                <h1 class="mb20">более 100 000</h1>
                <h2 class="text-uppercase">уникальных просмотров</h2>
                <h2>Вашего объявления</h2>
                <h3>на нашем сервисе</h3>
                <a href="#w" class="more-on-first ">Подробнее</a>
            </div>
        </div>
    </div>

    <!-- ********** FIRST ********** -->
    <div id="w" class="w">
        <div class="row nopadding">
            <div class=" ptb">
                <h3 class="notmargintop">Условия нашего предложения</h3>
                <div class="opts row">
                    <div class="opts41 item centered">
                        <div>&#8381</div>
                        <h4>стоимость 10 рублей</h4>
                    </div>
                    <div class="opts42  centered loading"><img class="first__img4" data-src="/img/firsts__img_4.png"
                                                               alt=""><h4>более
                            100 000 просмотров</h4> <span><p>Если же быть точным - <b>131&nbsp;070</b>
                    <und>уникальных просмотров</und>
                    (Сто тридцать одна тысяча семьдесят).
                </p>
                <p>Почему такая точность? Это довольно просто - секрет кроется в способе распространения информации о Вашем объявлении.</p>
                <p>Ключевой момент - это бинарная матрица. После публикации объявления Вы сообщаете о нашем сервисе двоим людям.</p>
                <h5> Да, всего лишь 2-м!</h5>
                </span>
                    </div>
                    <div class="opts43 item centered">
                        <div>
                            <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                            <svg class="first__img2" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
<g>
    <g>
        <path d="M489.469,465.394c5.948-9.98,9.255-21.304,9.498-33.06c0.367-17.756-6.241-34.342-18.606-46.702l-53.508-53.508
			c-12.041-12.042-28.087-18.624-45.318-18.624c-0.464,0-0.929,0.005-1.395,0.015c-11.759,0.242-23.086,3.547-33.067,9.495
			l-7.857-7.856c5.949-9.981,9.258-21.305,9.501-33.061c0.367-17.758-6.243-34.349-18.615-46.714l-53.509-53.508
			c-12.366-12.365-28.963-18.966-46.709-18.608c-11.756,0.243-23.08,3.551-33.059,9.499l-7.852-7.853
			c5.949-9.981,9.256-21.308,9.498-33.066c0.365-17.756-6.247-34.345-18.615-46.71L126.358,31.63
			c-21.301-21.303-54.282-24.233-79.764-9.105L24.066,0.007L0,24.082l22.522,22.514c-15.13,25.48-12.205,58.457,9.09,79.766
			l53.496,53.501c12.041,12.043,28.087,18.628,45.322,18.626c0.464,0,0.933-0.005,1.399-0.015
			c11.763-0.243,23.09-3.551,33.073-9.499l7.852,7.853c-5.951,9.981-9.258,21.308-9.502,33.067
			c-0.368,17.756,6.241,34.347,18.606,46.711l53.509,53.51c12.042,12.042,28.087,18.624,45.317,18.623
			c0.463,0,0.929-0.005,1.395-0.015c11.763-0.242,23.09-3.551,33.068-9.5l7.853,7.851c-5.949,9.982-9.255,21.31-9.495,33.071
			c-0.363,17.757,6.248,34.346,18.611,46.703l53.501,53.51c12.416,12.408,28.795,18.582,45.346,18.582
			c11.854-0.001,23.796-3.169,34.431-9.484l22.531,22.533L512,487.927L489.469,465.394z M163.267,139.194l-20.3-20.304
			l-24.073,24.069l20.304,20.306c-2.618,0.719-5.322,1.12-8.069,1.177c-8.375,0.218-16.198-2.897-21.947-8.647l-53.494-53.5
			c-7.928-7.933-10.363-19.411-7.449-29.993l7.023,7.021l24.067-24.075l-7.003-7.001c2.862-0.785,5.787-1.189,8.692-1.189
			c7.827,0,15.492,2.863,21.27,8.642l53.501,53.506c5.751,5.747,8.822,13.538,8.65,21.936
			C164.381,133.884,163.983,136.583,163.267,139.194z M313.508,289.449l-20.31-20.307l-24.069,24.073l20.308,20.305
			c-2.612,0.717-5.313,1.117-8.057,1.173c-8.415,0.162-16.194-2.899-21.941-8.645l-53.509-53.511
			c-5.746-5.745-8.817-13.537-8.642-21.935c0.057-2.745,0.456-5.447,1.174-8.06l20.3,20.302l24.073-24.069l-20.304-20.306
			c2.612-0.717,5.313-1.117,8.055-1.173c0.225-0.005,0.449-0.008,0.673-0.008c8.139,0,15.668,3.058,21.262,8.652l53.509,53.508
			l0.003,0.003c5.75,5.747,8.821,13.538,8.648,21.936C314.627,284.132,314.226,286.835,313.508,289.449z M463.76,439.681
			l-7.009-7.009l-24.071,24.071l6.998,6.999c-10.581,2.911-22.058,0.473-29.99-7.452l-53.5-53.508
			c-5.747-5.745-8.818-13.536-8.647-21.936c0.057-2.745,0.455-5.447,1.172-8.06l20.31,20.307l24.069-24.073l-20.306-20.301
			c2.611-0.716,5.309-1.114,8.052-1.171c0.224-0.005,0.447-0.007,0.671-0.007c8.145,0,15.676,3.058,21.271,8.653l53.508,53.508
			l0.003,0.005c5.745,5.742,8.815,13.527,8.64,21.923C464.875,434.371,464.476,437.07,463.76,439.681z"/>
    </g>
</g>

                                <g></g></svg>
                        </div>
                        <h4>разрешены ссылки любого вида</h4>
                    </div>
                    <div class="opts44 item centered">
                        <div>
                            <svg class="first__img3" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                 viewBox="0 0 24 24">
                                <path
                                    d="M12 3c5.514 0 10 3.592 10 8.007 0 4.917-5.145 7.961-9.91 7.961-1.937 0-3.383-.397-4.394-.644-1 .613-1.595 1.037-4.272 1.82.535-1.373.723-2.748.602-4.265-.838-1-2.025-2.4-2.025-4.872-.001-4.415 4.485-8.007 9.999-8.007zm0-2c-6.338 0-12 4.226-12 10.007 0 2.05.738 4.063 2.047 5.625.055 1.83-1.023 4.456-1.993 6.368 2.602-.47 6.301-1.508 7.978-2.536 1.418.345 2.775.503 4.059.503 7.084 0 11.91-4.837 11.91-9.961-.001-5.811-5.702-10.006-12.001-10.006zm-3.5 10c0 .829-.671 1.5-1.5 1.5-.828 0-1.5-.671-1.5-1.5s.672-1.5 1.5-1.5c.829 0 1.5.671 1.5 1.5zm3.5-1.5c-.828 0-1.5.671-1.5 1.5s.672 1.5 1.5 1.5c.829 0 1.5-.671 1.5-1.5s-.671-1.5-1.5-1.5zm5 0c-.828 0-1.5.671-1.5 1.5s.672 1.5 1.5 1.5c.829 0 1.5-.671 1.5-1.5s-.671-1.5-1.5-1.5z"/>
                            </svg>
                        </div>
                        <h4>любая тематика</h4>
                        <span>за исключением призывов к насилию <br>(и маты - запрещены)</span>
                    </div>
                </div>
            </div>
        </div><!--/row -->
    </div>

    <!-- ********** BLUE SECTION - PICTON ********** -->
    <div id="picton" class="picton">
        <div class="row nopadding">
            <div class=" pictontext">
                <h3>Но, что очень важно,</h3>
                <p>сообщаете не тем, кому Вы хотите показать свое объявление - не целевой аудитории!</p>
                <h4>А таким,</h4>
                <h3>кого может заинтересовать</h3>
                <h4>показ объявления на огромную аудиторию <br>за символическую сумму - 10 рублей.</h4>
            </div>
        </div><!--row -->
        <div class="container itemtable">
            <div class="row nopadding">
                <h3 class="masonaryhead ptb">А дальше происходит так: </h3>
                <ol class="no-masonry">
                    <li class="item opts41 firstblock"><h4> Ваши двое</h4>
                        <p>тоже <span>публикуют</span> свои объявления &emsp;
                    </li>
                    <li class="item opts42 secondblock"><h4>Перед публикацией</h4>
                        <p>
                            <und> просматривают Ваше объявление</und> &emsp;
                    </li>
                    <li class="item opts43"><h4>Сообщают каждый</h4>
                        <p>еще 2-м <span>заинтересованным</span> людям - это уже
                            <span class ="large-font">4</span> &emsp;
                    </li>
                    <li class="item opts42 secondblock"><h4>Четверо
                            повторяют</h4>
                        <p>
                            все те же действия:
                            <span>просматривают Ваше объявление</span>
                            и объявления тех, кто их пригласил,&emsp;
                    </li>
                    <li class="item opts44"><h4>Публикуют свое объявление</h4>
                        <p> затем приглашают<span> каждый еще по 2 человека</span> - это уже
                            <span class="large-font">8</span> &emsp;
                    </li>
                    <li class="item opts41 firstblock"><h4>Восьмеро
                            повторяют</h4>
                        <p>
                            все те же действия: просматривают, публикуют, приглашают &emsp;
                    </li>
                    <li class="item opts42 secondblock"><h4>Так повторяется 16 раз</h4>
                        <p>
                            с удвоением количества просмотров
                            <span>вашего объявления</span>
                            на каждом витке повторения &emsp;
                    </li>
                    <li class="item opts43"><h4>На 16-й раз получится</h4>
                        <p>
                            <und> уже 65 536 просмотров</und>
                            + предыдущих 65 534 - итого 131 070 уникальных просмотров <br>
                            <button class="btn-main-page" id="lookTable">Посмотреть таблицу</button>
                        </p>
                        <div id="showMatrix" class="show-matrix hidden-matrix hide">
                            <table class="cards-table">
                                <thead>
                                <tr>
                                    <td>№ витка</td>
                                    <td>Просмотров <br>на витке</td>
                                    <td>Просмотров <br>всего от начала</td>
                                </tr>
                                </thead>
                                @php
                                    $levels = 17;
                                    $rowRes = 1;
                                    $rowTot = 0;
                                @endphp
                                @for($i=1; $i<$levels; $i++)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$rowRes *= 2}}</td>
                                        <td>{{$rowTot += $rowRes}}</td>
                                    </tr>
                                @endfor
                            </table>
                            <div class="close-matrix" id="closeMatrix">&times;</div>
                        </div>
                    </li>
                    <li class="item opts44"><h4>После этого</h4>
                        <p> показы Вашего <span> объявления</span> на сервисе прекращаются.</p></li>
                </ol>
                <div class="mt">
                    <div class="btn-theme centered align-center">Регистрация в меню &#8657;</div>
                </div>
            </div><!--row-->
        </div><!-- container -->
    </div><!--/Picton -->

    <!-- ********** purple section *************** -->
    <div id="purple" class="purple bg-royal-purple ptb">
        <div class="row nopadding">
            <div class=" robin-egg-blue purblock">
                <h2>Почему это будет происходить, как написано?</h2>
                <h2>Потому что все приглашают </h2>
                <h1><span>заинтере&shyсованных</span> активных людей,</h1>
                <h2>а не простую публику</h2>
                <div class="mt">
                    <div class="btn-theme centered align-center">Регистрация в меню &#8657;</div>
                </div>
            </div> <!-- /robin-egg-blue -->
        </div><!--row -->
    </div>

    <!-- ********* inner White section *********** -->
    <div id="w2" class="w ptb">
        <div class="row nopadding">
            <div class="w2-block">
                <h3 class="notmargintop">Обратите внимание</h3>
                <h4>что при публикации объявления, Вы дополнительно публикуете ссылку
                    <span class="large-font">на основной ресурс</span>, который Вы хотите показать:
                </h4>
                <div class="opts-cont">
                    <div class="opts4 opts41 optblock loading">
                        <img class="img4" data-src="img/site.webp"/><span> Свой сайт</span>
                    </div>
                    <div class="opts4 opts42 optblock loading">
                        <img class="img4" data-src="img/lpr.webp"/><span>Лендинг пэйдж</span>
                    </div>
                    <div class="opts4 opts43 optblock loading">
                        <img class="img4" data-src="img/instagram.webp"/><span> Страницу соцсети</span>
                    </div>
                    <div class="opts4 opts44 optblock loading">
                        <img class="img4" data-src="img/doski.webp"/><span> Или иное - то, что Вам надо</span>
                    </div>
                </div>
                <p>Поэтому просим подготовить ссылку на соответствующий ресурс и, если возможно, поставьте на него
                    счетчик посещений, а еще лучше -
                    Google аналитику - это даст Вам дополнительную информацию о Ваших посетителях.</p>
                <p>Разумеется мы ведем статистику всех просмотров, закладок и переходов по Вашей ссылке. Отчет по
                    статистике Вы в любое время
                    сможете увидеть на странице своего профиля, но Вам будет полезно иметь уверенность, что эти переходы
                    достигают цели -
                    посетители заходят на указанный Вами ресурс. </p>
            </div>
        </div> <!-- row -->
    </div><!-- /w2 -->

    <!-- ********* green section *********** -->
    <div id="visit-card" class="opts45 ptb .visit-card">
        <div class="row nopadding">
            <div class="visit-card__inner">
                <h3>Если же у Вас такого ресурса нет</h3>
                <div class="visit-card__subtitle">
                    <p> или, по некоторым причинам, Вы не хотите его использовать в данном случае, мы можем предоставить
                        Вам за отдельную плату - 50 руб., <span>страницу-визитку</span>, на которой Вы можете поместить определенный набор данных:
                    </p>
                </div>
                <ul class="resource-list">
                    <li>
                        <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                        <svg display="none">
                            <symbol id="visit-card__svg--white" viewBox="0 0 512 512"
                                    style="enable-background:new 0 0 512 512;">

                                <path d="M383.841,171.838c-7.881-8.31-21.02-8.676-29.343-0.775L221.987,296.732l-63.204-64.893
			c-8.005-8.213-21.13-8.393-29.35-0.387c-8.213,7.998-8.386,21.137-0.388,29.35l77.492,79.561
			c4.061,4.172,9.458,6.275,14.869,6.275c5.134,0,10.268-1.896,14.288-5.694l147.373-139.762
			C391.383,193.294,391.735,180.155,383.841,171.838z"/>
                                <path d="M256,0C114.84,0,0,114.84,0,256s114.84,256,256,256s256-114.84,256-256S397.16,0,256,0z M256,470.487
			c-118.265,0-214.487-96.214-214.487-214.487c0-118.265,96.221-214.487,214.487-214.487c118.272,0,214.487,96.221,214.487,214.487
			C470.487,374.272,374.272,470.487,256,470.487z"/>

                            </symbol>
                        </svg>
                        <svg class="visit-card__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        Заголовок Вашей визитки <br> <span> до 80 символов</span>
                    </li>
                    <li>
                        <svg class="visit-card__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        цена Вашего продукта/услуги
                        <br><span>с выбором из списка валют</span>
                    </li>
                    <li>
                        <svg class="visit-card__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        Ваш аватар или фото
                        <br><span>не обязательно, но желательно</span>
                    </li>
                    <li>
                        <svg class="visit-card__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        Описание Вашего продукта, услуги <br> <span> 1500 символов, включая пробелы</span>
                    </li>
                    <li>
                        <svg class="visit-card__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        Фото продукта/услуги <br><span> хорошего качества, 5 шт.</span></li>
                    <li>
                        <svg class="visit-card__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        Видео с Ютуба <br> <span> 3 шт.</span></li>
                    <li>
                        <svg class="visit-card__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        Ваши всевозможные контакты <br> <span> телефоны, почта, мессенджеры, ссылки на Ваши другие ресурсы</span>
                    </li>
                </ul>

            </div>
        </div>
        <div id="vizit" class="container vizit">
            <div class="vizit-up_title">Здесь Вы видите отдаленный пример визитки:</div>
            <div class="vizit-logo">
                <div class="vizit-logo__inner">
                    <div class="visimgblock">
                        <div class="loading"><img data-src="img/avatar.png"/></div>
                    </div>
                </div>
                <div class="vizit-contact">
                    <div class="vizit-contact__inner">Контакты:
                        <ul>
                            <li>www.site.com</li>
                            <li>email@gmail.com</li>
                            <li>skype_me</li>
                            <li>+7-495-999-9999</li>
                            Ссылки:
                            <li>www.site1.com</li>
                            <li>www.site2.com</li>
                            <li>www.site3.com</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="vizit-block">

                <div class="vizit-title">
                    <div class="vizit-title__inner">
                        <div class="vishead"><h3>Заголовок до 80 символов</h3></div>
                        <div class="blockprice"><h4>ЦЕНА</h4></div>
                    </div>
                    <div class="row borders" style="background-color: #FCFCFC; height: 200px;">
                        <div class="description">Описание Вашего продукта, услуги, которые Вы предлагаете или того, что
                            Вы хотите купить,
                            найти, заказать, приобрести или даже получить в подарок. <br>До 1500 знаков с пробелами.
                        </div>
                    </div>

                </div>
                <div class="nopadding"><p>
                    <div class="opts-cont">
                        <div class="fivepic loading"><img class="img4" data-src="img/ski1_tn.webp"/></div>
                        <div class="fivepic loading"><img class="img4" data-src="img/ski2_tn.webp"/></div>
                        <div class="fivepic loading"><img class="img4" data-src="img/ski3_tn.webp"/></div>
                        <div class="fivepic loading"><img class="img4" data-src="img/ski4_tn.webp"/></div>
                        <div class="fivepic loading"><img class="img4" data-src="img/ski5_tn.webp"/></div>
                    </div>
                    </p>

                </div> <!-- row -->
                <div class="vizit-video">
                    <div class="vizit-video__inner">
                        <div class="vizit-video__img loading">
                            <img data-src="img/frame-wrapper--img.webp" alt="" class="frame-wrapper--img">
                            <div class="youtube-button">
                                <div class="triangle"></div>
                            </div>
                        </div>
                        <div class="frame-wrapper"></div>
                    </div>
                </div>
            </div>

        </div> <!-- row -->
    </div> <!-- container -->
    <div class="row nopadding see-visit ">
        <div class="see-visit__inner">
                <span><b>При этом вокруг нет никаких отвлекающих баннеров, тизеров или других видов рекламы! Только Ваша инфа... <br> Страница хранится на
                        сервере до тех пор, пока она для Вас актуальна. <br></b> Цветовая гамма настраивается.</span>
            <br>
            <a href="{{route('pageslist')}}" style="display: inline-block;">
                <button class="btn-white btn-theme btnlist  btn-main-page" id="btnviewvisit">Посмотреть визитки</button>
            </a>
            <div class="mt">
                <div class="btn-info btn-theme centered align-center" style="max-width: 75% !important;">Заказать визитку
                    Вы сможете после регистрации &emsp; &#9650;
                </div>
            </div>
            <p>У вашей визитки будет доступная ссылка вида <br>
                <und style="cursor: pointer;">http://million-ru.segment.work/уникальный-слаг-вашей-визитки</und>
            </p>
        </div>
    </div>

    <div class="no-link">
        <div class="row nopadding">
            <div class="no-width">
                <p>Если для Вашего объявления внешний ресурс не нужен, можно публиковать без
                    него - при создании своего объявления Вы просто отметите "птичку",
                    указывающую системе, что внешний ресурс не нужен.</p>
                <p>Все остальное остается без изменений, только не будет переходов по ссылке.</p>
                <p>Если Вы планируете приобрести у нас страницу-визитку, то начать публикацию удобнее с нее - чтобы у Вас сразу появилась ссылка на нее,
                    которую Вы вставите в объявление. Ссылка на визитку будет доступна в кабинете.</p>
            </div>
        </div>
    </div>

    <!-- ********** BLUE SECTION - CURIOUS ********** -->
    <div id="curious" class="ptb curious">
        <div class="row nopadding curious-top">
            <div class="curious-top__inner">
                <h4 class="curheadtext notmargintop">Чтобы просмотры начались, <br>Вам нужно выполнить</h4>
                <h3 class="steps">3 простых шага:</h3>
            </div>
        </div><!--/row -->

        <div class="row nopadding curious-block">
            <div class="curious-block__inner">
                <ol class="opts-cont">
                    <li class="opts31 inner">
                        <div class="curious-block__img loading">
                            <img src="" class="img3" data-src="img/order-1.webp"/></div>
                        <div class="curious-block__text">
                            <h4>Заказ:</h4>
                            <ul>
                                <li class="">
                                    <span>регистрация</span>
                                    - имя, е-мэйл, пароль; указывайте реальный е-мэйл - Вы будете получать наш отчет о
                                    количестве просмотров Вашего
                                    объявления и визитки, если ее тоже создадите.
                                </li>
                                <li>
                                    <span>оплата</span>
                                    - всего 10 рублей
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="opts32 inner">
                        <div class="curious-block__img loading"><img data-src="img/tvorch-1-min.webp"/></div>
                        <div class="curious-block__text">
                            <h4>Творчество:</h4>
                            <ul>
                                <li>
                                    <span>просмотреть образцы объявлений</span>
                                    20 шт., это неотъемлемый элемент нашего сервиса - увидеть реальные образцы
                                    объявлений
                                </li>
                                <li>
                                    <span>составить свое объявление</span>
                                    - до 160 знаков и отдельно - ссылка на страницу, которую Вы рекламируете
                                </li>
                                <li>
                                    <span>опубликовать в 1 клик</span>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="opts33 inner">
                        <div class="curious-block__img loading"><img data-src="img/marketing-1-min.webp" class="img3"/>
                        </div>
                        <div class="curious-block__text"><h4>Маркетинг:</h4>
                            Приглашение на сервис всего лишь 2-х человек, которые
                            <div class="centered">
                                <span> <h6>заинтересованы получить</h6></span>
                                более 100 000 просмотров
                                своего объявления
                            </div>
                        </div>
                    </li>
                </ol>

                <div class="mt">
                    <div class="btn-theme centered align-center btn-main-page">Регистрация в меню &#8657;</div>
                </div>
            </div>
        </div><!--/row -->
    </div><!--/curious -->

    <!-- ********** BLUE SECTION - MALIBU ********** -->
    <div class="ptb" id="malibu">
        <div class="row nopadding malibu">
            <h3 class="notmargintop malibu__title">Как только Вы зарегистрируете свой е-мэйл</h3>
            <div class="malibu__inner blimg loading">
                <img data-src="img/shot04.webp" class="img-responsive malibu-inner__img align-left" alt=""
                     data-effect="slide-left">
            </div>

            <div class="malibu-text">

                <p>и подтвердите его, мы сформируем Вашу персональную ссылку на наш сервис, которую Вы будете давать
                    приглашенным Вами людям, чтобы система
                    <span>показала им и всем последователям
                       <span class="large-font">Ваше объявление</span>,
                    </span> когда они придут на сервис.
                </p>
                <p>
                    <span>Очень Вас просим</span>
                    - <b>
                        <span class="large-font">не надо</span>
                    </b>
                    <span>рассылать сотни приглашений незнакомым людям</span>, спамить и тому подобное - эти действия не
                    принесут желаемого результата. Достаточно дать ссылку
                    <span class="large-font">двоим людям</span>,
                    <span>о которых Вы точно знаете</span>, что им наша услуга понадобится.
                </p>
                <p>Если же Вы таких не знаете - просто опросите несколько знакомых Вам людей об их заинтересованности в
                    просмотрах их объявления на
                    таких условиях:</p>
                <ul class="list-conditions">
                    <li>более 100 000 просмотров</li>
                    <li>стоимость 10 рублей</li>
                    <li>разрешены ссылки любого вида</li>
                    <li>любая тематика,
                        <small>за исключением призывов к насилию (и маты запрещены)</small>
                    </li>
                </ul>
                <h4>Двоих вы точно найдете.</h4>
            </div>
        </div><!--row -->
    </div><!--/Malibu -->

    <!-- ********* rose section *********** -->
    <div id="wide" class="rose  wide ptb">
        <div class="container">
            <p class="notmargintop"><h4>Это не обязательно должны быть бизнесмены или предприниматели.</h4><span>Ведь у очень многих людей время от времени возникает необходимость
                оповестить большую аудиторию
                о своих потребностях или возможностях:</span> </p>
            <div class="no-masonry5 .wide__content">
                <ol class="wide__content--left">
                    <li class="item">
                        <svg class="wide__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        <p>кто-то ищет клиентов</p></li>
                    <li class="item">
                        <svg class="wide__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        <p>а кто-то - друзей или единомышленников по интересам</p></li>
                    <li class="item">
                        <svg class="wide__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        <p>некоторые ищут попутчика, для ежедневных поездок</p></li>
                    <li class="item">
                        <svg class="wide__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        <p>кто-то для разовой поездки - в дальние дали</p></li>
                    <li class="item">
                        <svg class="wide__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        <p>ищут покупателя</p></li>
                    <li class="item">
                        <svg class="wide__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        <p>

                            ищут продавца - какой-либо редкой штуковины,
                            <und>не обязательно</und>
                            дорогой
                        </p>
                    </li>
                </ol>
                <ol class="wide__content--right">
                    <li class="item">
                        <svg class="wide__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        <p>ищут что-то купить просто подешевле</p></li>
                    <li class="item">
                        <svg class="wide__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        <p>продают и покупают б/у вещи</p></li>
                    <li class="item">
                        <svg class="wide__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        <p>меняются…</p></li>
                    <li class="item">
                        <svg class="wide__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        <p>дарят подарки :)</p></li>
                    <li class="item">
                        <svg class="wide__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        <p>хотят получить подарок!</p></li>
                    <li class="item">
                        <svg class="wide__svg">
                            <use xlink:href="#visit-card__svg--white"></use>
                        </svg>
                        <p>да мало ли что...</p></li>
                </ol>
            </div>
            <p class="notmarginbott">Из аудитории <b><nobr>больше 100 000 читателей</nobr></b>, очень вероятно найдется тот, кто
                нужен.</p>
        </div> <!-- /container -->
    </div> <!-- /rose -->

    <!-- ****** white ****** -->
    <div id="traffic" class="ptb traffic">
        <div class="container centered orangeblock traffic-block">
            <h6 class="notmargintop">Читатели <span>Вашего объявления</span> могут стать хорошим трафиком на Ваш
                веб-сайт: </h6>
            <h6>даже если <span>объявлением </span> заинтересуется только 1% от общего числа прочитавших </h6>
            <h6>и перейдут по ссылке - <span>больше 1000 человекobr</span> -
                <span class="large-font">высокий результат за 10 рублей.</span>
            </h6>
            <h5>Знаете ли вы другой сервис с таким <span>соотношением</span> цена/трафик? </h5> <br>
            <h6 class="notmarginbott">Ваша задача написать такой текст из 160 знаков, который привлечет внимание своим
                содержанием. <br> Что-то типа хорошего твита, с тем
                отличием, что</h6>
        </div> <!-- /container -->
    </div> <!-- /white -->

    <!-- ****** white ****** -->
    <div class="ptb opts41 declaration ">
        <div class="container centered orangeblock declaration__inner">
            <h2 class="notmargintop">
                <span>Ваше объявление</span>
            </h2>
            <h1>обязательно прочтут</h1>
            <h1 id="mill" class="notmarginbott">131&nbsp;070 человек</h1>
        </div> <!-- /container -->
    </div> <!-- /white -->

    <!-- ****** gray ****** -->
    <div id="dark-side" class="ptb gray">
        <div class="container dark-side__inner ">

            <p>У этого сервиса есть и темная, казалось бы, сторона - никакого таргетинга, ни по месту проживания, ни по
                возрасту, ни по интересам, ни по покупательной способности, в общем - ни-ка-ко-го. Чистая лотерея :)</p>
            <p>Тем не менее, при таком подходе результат может оказаться
                <span class="large-font">гораздо выше,</span>
                чем Вы можете предположить - потому что
                <span style="color: #fff;"><b>скрытый таргетинг</b></span> все же есть - это
                    <span class="large-font color-orangered" style=" white-space: nowrap;"> Активные Люди</span>
                    и Ваше объявление может неожиданно оказаться для них <b>
                    <span style="color: #fff; ">
                        <und>важным</und>
                    </span>
                </b> и они его прочтут :) и сделают закладку для перехода.
            </p>
            <p class="notmarginbott">Вам нужен
                <b>
                    <span class="red-ext">короткий</span>
                    <span class="orange">яркий</span>
                    <span style="color:yellow;">текст</span>,
                    <span class="green-ext">который</span>
                    <span class="blues">сможет</span>
                    <span class="navy">зацепить</span>,
                    <span class="viole-ext">увлечь</span>
                </b> хотя бы на 20-30 секунд - этого достаточно, чтобы человек при просмотре нажал кнопку
                “Закладка”, а потом он уж <b>обязательно пройдет по ссылке этой закладки</b> потому что он ведь уже
                отметил ее, как достойную
                просмотра.
            </p>
        </div> <!-- /container -->
    </div> <!-- /gray -->

    <!-- ********** BLUE SECTION - JELLY ********** -->
    <div id="jelly" class="ptb jelly">
        <div class="jelly-inner" style="margin: 0 !important;">
            <div class="jelly-inner__block blueblock"> <!--  col-md-offset-1 -->

                <div class="jelly-text">
                    <h3 class="notmargintop textblueclock jelly__title">Итак, Вам нужно немного подготовиться -</h3>
                    <div>
                        <h4 class="secbluetext"><span>определиться с ресурсом</span>, который вы хотите продвигать с
                            помощью нашего сервиса.</h4> Или же,
                        <und>после регистрации,</und>
                        заказать себе у нас страницу-визитку.
                        <p>Если у Вас пока еще не приготовлен ресурс, это не препятствует Вам зарегистрироваться и
                            оплатить, позже Вы сможете зайти через меню
                            "Работа с объявлением"/"Опубликовать объявление" и продолжить: создать объявление и дать
                            ссылку.
                        </p>
                        <p>А пока Вы готовите ресурс, Вы можете параллельно давать свою ссылку своим избранным - это <b>сократит
                                время до начала</b>
                            просмотров Вашего объявления.
                            Хотя затягивать с ресурсом не стоит, ведь сразу после просмотра люди захотят перейти по
                            Вашей ссылке.</p>
                        <p>Ссылку для приглашения мы сформируем сразу же после Вашей регистрации.</p>
                        <h6>Доступна для регистрации Ваших приглашенных она станет сразу после оплаты.</h6>
                    </div>
                </div>
            </div>

            <div class="jelly-img centered">
                <div>
                    <p class="notmargintop">Мы позаботились, чтобы Вам было легко пользоваться нашим сервисом, в т.ч. и
                        при оплате. Оплата банковской картой через сервис WayForPay займет у Вас минимальное время.</p>
                </div>
                <div class="jelly-img__img loading" style="margin-bottom: 60px;"><img class="" data-src="img/w4p.webp"
                                                                                      style="width: 90%;"/>
                </div>
            </div><!--/row -->
        </div><!--/Jelly -->

        <!-- ******** yellow ********** -->
        <div id="finish" class="opts42 ptb finish">
            <div class="container yellowblock finish__inner">
                <div class="finish__title">
                    <h3 class="notmargintop">Как проходит весь процесс</h3>
                    <h4>Вам понадобится всего 20-30 минут:</h4>
                </div>
                <div class="opts-cont">
                    <div class="item">
                        <div class="procctext">Регистрация</div>
                        <div class="imgo loading"><img data-src="img/register-min.webp"></div>
                        <h6 class="centered imgsubtext imgsubtext--small">1-2 мин.</h6>
                    </div>
                    <div class="opts49 item">
                        <div class="procctext">Оплатить</div>
                        <div class="imgo loading"><img data-src="img/one_rt-min.webp"></div>
                        <h6 class="centered imgsubtext imgsubtext--small">2-3 мин.</h6>
                    </div>
                    <div class="opts49 item">
                        <div class="procctext">Образцы объявлений</div>
                        <div class="imgo loading"><img data-src="img/eyes3.gif"/></div>
                        <h6 class="centered imgsubtext imgsubtext--small">просмотреть и сделать закладки <br/> 11-12
                            мин.</h6>
                    </div>
                    <div class="opts49 item">
                        <div class="procctext">Составить свое объявление</div>
                        <div class="imgop loading"><img data-src="img/edit_ad3_m-min.webp"/></div>
                        <h6 class="centered imgsubtext imgsubtext--small">и опубликовать <br/> 5-8 мин.</h6>
                    </div>
                </div>
                <div class="finish__footer">
                    <h4 class="centered">после этого остается только сообщить избранным людям о нашем сервисе</h4> <br>
                    <h5 class="centered">Просмотры Вашего объявления начнутся сразу же, <br> как только Ваши
                        приглашенные начнут <br>
                        <span>делать то же самое, что сделали
                    Вы</span>

                        <br> просматривать образцы объявлений.
                    </h5>
                </div>
            </div> <!-- container -->
        </div> <!-- /yellow -->

        <!-- ********** FOOTER ********** -->
    </div>
@endsection
@push('scripts')
    <script src="\js\load-videoframe.js"></script>
    <script src="\js\optimizer.js"></script>
@endpush
