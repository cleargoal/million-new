@extends('layouts.app')
@section('title', 'Список визиток, страница ' . $current)

@section('content')
    <div id="b">
        <div class="artily-pagelist-wrapper">

            <div class="artily-pagelist-head">
                <h3>Просмотрите страницы визитки</h3>
            </div>

            <div class="artily-pagelist-list">
                <table class="pages-list-table">
                    @foreach($pageslist as $listPage)
                        <tr class="page-list-header-element">
                            <td class="pages-list-header-cell">
                                <a href="{{ route('showvslug', $listPage->visi_slug ) }}">
                                    <div class="">{{ $listPage->header }}</div>
                                </a>
                            </td>

                            <td class="pages-list-price-cell">{{ $listPage->price }}</td>
                            <td class="pages-list-currency-cell">{{ $listPage->currency}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="pages-links">{{ $pageslist->links() }}</div>
    </div>
@endsection
