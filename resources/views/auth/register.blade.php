@extends('layouts.app')

@section('content')
    <div class="artily-overlay">
        @if (!empty(session('parent_slug')))
            <div class="artily-table">
                <div class="artily-cell">

                    <form name="regFrom" class="artily-modal artily-form artily-reg_form" role="form" method="POST"
                          action="{{ url('/register') }}">
                        {{ csrf_field() }}
                        <a href="/" class="artily-close artily-close-reg"></a>
                        <ul class="">
                            <li>
                                <h3>Регистрация</h3>
                            </li>
                            <li class="artily-reg-input">
                                <label for="name">Имя</label>
                                <input type="text" id="name" name="name" pattern="[A-Za-zА-Яа-яЁё_.-]{2,50}" value="{{ old('name') }}" required autofocus >
                            </li>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            <li class="artily-reg-input">
                                <label for="email">E-mail</label>
                                <input type="email" id="email" name="email" value="{{ old('email') }}" required>
                            </li>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror

                            <li class="artily-reg-input">
                                <label for="password">Пароль</label>
                                <input type="password" id="password" name="password" required>
                            </li>


                            <li class="artily-reg-input">
                                <label for="password-confirm">Повтор пароля</label>
                                <input type="password" id="password-confirm" name="password_confirmation" required>
                            </li>

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </ul>
                        <div class="artily-submit-block">
                            <div class="artily-subw">
                                <input class="artily-submit" type="submit" id="reg-submit" value="отправить">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        @else
            <div class="artily-table">
                <div class="artily-cell">
                    <div class="artily-modal artily-notifications">
                        <a href="/" class="artily-close artily-close-prereg"></a>
                        <div class="artily-notifications-wrap">
                            <div class="artily-notifications-title">
                                <h4>Вы зашли на сайт по ссылке НЕ из приглашения.</h4>
                                <h5>Если Вас пригласил кто-нибудь, просим зайти по той ссылке, которую Вам
                                    прислали.</h5>
                                <h5>Если же у Вас нет ссылки-приглашения, мы сформируем Вам ссылку...</h5>
                            </div>
                            <form action="">
                                <div class="artily-reg-noslug">
                                    <div class="artily-yes_ref">
                                        <p>У меня есть ссылка-приглашение, <br>я могу ее использовать.</p>
                                        <div class="artily-ref-ok">
                                            <input type="text" id="invite-link"
                                                   placeholder="Вставьте в это поле ссылку-приглашение">
                                            <button id="invite-ok" class="btn btn-main-color" disabled>ОК</button>
                                        </div>
                                    </div>
                                    <div class="artily-no_ref">
                                        <p>У меня нет приглашения,<br> зарегистрируйте меня.</p>
                                        <button id="invite-nolink" class="btn btn-second-color">Регистрировать!</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <script>
        const inviteLink = document.querySelector('#invite-link');
        const inviteSubmit = document.querySelector('#invite-ok');
        const inviteNoLink = document.querySelector('#invite-nolink');
        let locationHref = '';
        let UserSlug = '';

        inviteLink.addEventListener('input', function () {
            let inviteLinkValue = inviteLink.value;
            UserSlug = extractSlug(inviteLinkValue);
            locationHref = "{{Request::url()}}" + UserSlug;
            inviteSubmit.removeAttribute('disabled');
        });

        inviteSubmit.addEventListener('click', function (e) {
            e.preventDefault();
            document.location.href = locationHref;
        });

        inviteNoLink.addEventListener('click', function (e) {
            e.preventDefault();
            window.location.href = "{{route('slug-referral-register', ['url_slug' => $root ?? 'just-first-registration'])}}";
        });

        function extractSlug(link) {
            let target = "/";
            let pos = -1;
            let found = 0;
            while ((pos = link.indexOf(target, pos + 1)) !== -1) {
                found = pos;
            }
            return link.slice(found);
        }
    </script>
    @endif
    </div>

@endsection
