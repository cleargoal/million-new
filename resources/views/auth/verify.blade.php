@extends('layouts.app')

@section('content')
    <div class="registered-wrap">
        <div class="million-panel">
            @if ($resent??"")
                <div class="million-panel-heading">
                    <h3>
                        {{ __('verification.sent') }}
                    </h3>
                </div>
            @else
                @if(session('expired'))
                    <div class="million-panel-heading">
                        <h3>{{ __('verification.expired') }}</h3>
                    </div>
                @else
                    <div class="million-panel-heading">
                        <h3>{{ __('verification.verify') }}</h3>
                    </div>
                    <div class="million-panel-body">
                        <p>{{ __('verification.check') }}</p>
                        <p>{{ __('verification.if not') }}</p>
                        @endif
                        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                            @csrf
                            @if($email ?? "")
                                <input type="hidden" name="email" value="{{$email}}">
                            @endif
                            <button type="submit" class="btn btn-info-color" style="margin-top: 1rem;">
                                {{ __('verification.resend') }}
                            </button>
                        </form>
                    </div>
                @endif
        </div>
    </div>
@endsection
