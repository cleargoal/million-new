@extends('layouts.app')

@section('content')

    <div class="artily-overlay">
        <div class="artily-table">
            <div class="artily-cell">
                <form class="artily-modal artily-form artily-reset-pass" role="form" method="POST"
                      action="{{ url('/password/email') }}">
                    {{ csrf_field() }}
                    <a href="/" class="artily-close"></a>
                    <ul class="">
                        <li><h3>Сброс пароля</h3></li>

                        @if (session('status'))
                            <h4>
                                {{ session('status') }}
                            </h4>
                            <a href="{{ url('/') }}" class="btn btn-info-inverse">Закрыть</a>
                        @else
                            <li class="artily-reset-passwd {{ $errors->has('email') ? ' email-has-error' : '' }}">
                                <input type="email" class="form-control" name="email" placeholder="E-mail"
                                       value="{{ old('email') }}" required autofocus id="email" maxlenght="40">
                                @if ($errors->has('email'))
                                    <div>
                                        <span class="" style="font-size: 1.7rem">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    </div>
                                @endif
                            </li>
                    </ul>
                    <div class="artily-submit-block">
                        <div class="artily-reset-submit">
                            <input class="artily-submit" type="submit" id="reg-submit" value="Сбросить пароль">
                        </div>
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
@endsection
