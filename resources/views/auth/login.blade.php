@extends('layouts.app')

@section('content')

<div class="artily-overlay">
		<div class="artily-table">
			<div class="artily-cell">

	<form class="artily-modal artily-form artily-login_form" role="form" method="POST" action="{{ url('/login') }}" >
			{{ csrf_field() }}
			<a href="/" class="artily-close"></a>
			<ul class="">
				<li><h3>Вход</h3></li>

				<li class="artily-login-mail">
					<label class="artily-login-label" for="email">E-mail <span>*</span></label>
					<input type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus id="email">

				</li>

				<li class="artily-login-pass">
					<label class="artily-login-label" for="login-password">Пароль <span>*</span></label>
					<input type="password" class="" name="password" required id="login-password">
				</li>

				<li class="artily-login-checkbox">
					<div class="artily-checkbox-block">
						<input class="artily-checkbox-input" type="checkbox" name="remember" id="login-remember-me">
						<label class="artily-checkbox-label" for="login-remember-me">Запомнить меня</label>

						<input class="artily-checkbox-input" type="checkbox" name="show-pass" id="login-show-pass">
						<label class="artily-checkbox-label" for="login-show-pass">Показать пароль</label>
					</div>

				</li>
			</ul>
			<div class="artily-submit-block">
				<div class="artily-subw">
					<input class="artily-submit" type="submit" id="reg-submit" value="Войти">
				</div>
				<div class="">
					<a class="pass-reset" href="{{ url('/password/reset') }}">
					Забыл(а) пароль?
					</a>
				</div>
			</div>

	</form>
			</div>
		</div>
	</div>

@endsection

@push('scripts')
<script>
  const showPass = document.getElementById('login-show-pass')
  let password = document.getElementById('login-password')

  showPass.addEventListener('change', function() {
    if (this.checked) {
      password.setAttribute('type', 'text')

    } else {
      password.setAttribute('type', 'password')
    }
  })
</script>
@endpush