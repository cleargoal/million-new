@extends('layouts.app')
@section('title', 'Просмотр визитки: ' . $viewCard->header)

@section('content')
    <div class="visit-post" style="background-color: {{$styleBack}}; color: {{$styleFont}};">
        <div id="view-zit">
            {{--            1st row start --}}
            <div class="view-zit">
                <div class="view-zit-wrapp"> {{-- left column start--}}
                    <div class="view-zit-avatar">
                        <div class="vis-avatar">
                            <img id="avatimg"
                                 src="@if($viewCard->avatar){{asset('storage/avatars/'.$viewCard->avatar)}}@else{{'/img/avatar.png'}}@endif"
                                 alt="avatar"/>
                        </div>
                    </div>
                    <div class="view-zit-inner view-zit-inner--sm ">
                        <div class="cont-links view-zit-cont">
                            @if(!empty($viewCard->phone) || !empty($viewCard->contacts))
                                <div class="view-zit-cont__title">
                                    <div>Контакты:</div>
                                </div>
                                @if(!empty($viewCard->email))
                                    <div class="view-zit-cont__text">Почта:
                                        <a href="mailto:{{ $viewCard->email}}" target="_blank" rel="noopener noreferrer"
                                           class="contacts-line"
                                           style="color: {{$styleFont}};">
                                            {{ $viewCard->email}}
                                        </a>
                                    </div>
                                @endif
                                @if(!empty($viewCard->phone))
                                    <div class="view-zit-cont__text">Номер:
                                        <a href="tel:" class="contacts-line"
                                           style="color: {{$styleFont}};">{{ $viewCard->phone}}</a>
                                    </div>
                                @endif
                            @endif
                            @if(!empty($viewCard->contacts))
                                <div class="view-zit-cont__text" style="word-wrap: break-word;
        word-break: break-all; ">
                                    <p>Дополнительные контакты:<br>{{ $viewCard->contacts}}</p>
                                </div>
                            @endif

                        </div>
                        <div class="view-zit-video">
                            @if (!empty($videoArray))
                                <ul class="view-zit-video-inner">
                                    @foreach ($videoArray as $i => $video)
                                        <li class="vis-vid view-zit-video__content"
                                            data-content="{{--https://www.youtube.com/embed/--}}{{$video}}">
                                            Video {{$i+1}}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                        @if($mode === 's')
                            <div class="artily-edit">
                                <div class="artily-edit-vizit">Если что-то выглядит не так, как Вы хотели - исправьте
                                </div>
                                <a href="{{ route('edit.visit', ['vicard' => $vicard]) }}" class="btn btn-info-color">Редактировать</a>
                            </div>
                        @endif
                    </div> {{-- left column end --}}
                </div>
                <div class="visbody view-body "> {{-- rihgt column start--}}
                    <div class="view-body-header">
                        <div class="view-body-header-wrapp">
                            <div class="viz-header view-body-header__title">
                                <h3>{{ $viewCard->header }}</h3>
                            </div>
                            <div class="view-body-header-price">
                                <h3 class="mob-price view-body-header__price">{{ $viewCard->price }}
                                    &nbsp;{{ $viewCard->currency }}</h3>
                            </div>
                        </div>
                        <div class="row borders visidescr view-body-header__text">
                            <div class=" fieldescr">{!! $viewCard->description !!}</div>
                        </div>
                    </div>
                    <div id="out-popup">
                        <div id="inpopup" class="">
                            <iframe id="video-frame" width="1016" height="515" src=""
                                    {{--frameborder="0"--}} allowfullscreen></iframe>
                            <div id="close-video" class="close-video hide" type="button" title="Закрыть">&times;</div>
                        </div>
                    </div>
                    <div id="close-pics" class="close-pics hide" type="button" title="Свернуть картинки">&times;</div>
                    <div id="popup" class="popup hide"></div>
                </div> {{-- rihgt column end --}}
            </div>{{--        1st row end    --}}
            {{--            2nd row start--}}
            <div class="pictures-block-wrapp">
                @if ($pictures->count() > 0)<p class="pictures-block__title">Клик на картинку - увеличение.</p>@endif
                <div class="pictures-block" id="picturesBlock">
                    @if (!empty($pictures))
                        @foreach ($pictures as $k => $picture)
                            <div class="picts fivepic-close">
                                <img class="vis-pic" alt="picture"
                                     data-content="{{str_replace('\\', '/', asset(config('image.view_pict') . str_replace('fold', 'thum', $picture)))}}"
                                     data-arind="{{$k}}"
                                     src="{{str_replace('\\', '/', asset(config('image.view_pict') . str_replace('fold', 'thum', $picture)))}}"/>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="view-zit-links">
                    <div class="view-zit-links__title">
                        <und>Ссылки:</und>
                    </div>
                    @if(!empty($links))
                        @foreach ($links as $y => $link)
                            <a class="view-zit-links__text">
                                {{ $y > 0 ? ', ' : '' }} <a href="{{ $link}}" target="_blank">
                                    Вебсайт {{ ($y + 1) }}</a>
                            </a>
                        @endforeach
                    @else
                        нет дополнительных ссылок
                    @endif
                </div>
                <!-- row -->
            </div>
            {{--            2nd - end--}}
        </div> <!-- id="view-zit" -->
        <div class="navigat view-zit-navigat">
            <div class="pageNavigation view-zit-navigat-wrapp">
                <div class="view-zit-navigat-arrow">
                    @if ($page_prev)
                        <a class="vizGliph" id="butprev"
                           href="{{  route('showvslug', ['vicard' => $page_prev]) }}">&#8678;</a>
                    @endif
                </div>
                @php
                    $type = session('listyp');
                    $uslug = session('user_slug');
                    if ($type == 'v') {
                        $rout = route('pageslist', ['page' => $visits_intended_page]);
                    } elseif ($type == 'q') {
                        $rout = route('listuslug', ['uslug' => $uslug,'page' => $visits_intended_page]);
                    }
                    else {
                        $rout = '';
                    }
                @endphp

                @if ($rout != '')
                    <div>
                        <a class="btn" href="{{ $rout }}">
                            <button id="vizListBut">К списку</button>
                        </a>
                    </div>
                @endif
                <div class="view-zit-navigat-arrow">
                    @if ($page_next)
                        <a class="vizGliph" id="butnext"
                           href="{{ route('showvslug', ['vicard' => $page_next]) }}">&#8680;</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script src="\js\visitcard.js"></script>
@endpush
