<footer class="main-footer">
    <div class="auto-container">
        <div class="b-bible__top">
            <div class="b-bible">
                <div class="b-bible__text">
                    <p id="bbq-text">{{$data[0]['text']}}</p>
                </div>
            </div>
            <div title="Еще цитата" class="next-quote" id="next-quote"
                 data-href="{{ route('next.bible.quote') }}"
                 data-csrf-token="{{ csrf_token() }}"><span class="next-quote__trigon"></span>
            </div>
            <div class="b-bible__svg mobile">
                <svg class="circle_arrow" aria-hidden="true" focusable="false" data-prefix="far"
                     data-icon="arrow-alt-circle-up" role="img" xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 512 512">
                    <path
                        d="M256 504c137 0 248-111 248-248S393 8 256 8 8 119 8 256s111 248 248 248zm0-448c110.5 0 200 89.5 200 200s-89.5 200-200 200S56 366.5 56 256 145.5 56 256 56zm20 328h-40c-6.6 0-12-5.4-12-12V256h-67c-10.7 0-16-12.9-8.5-20.5l99-99c4.7-4.7 12.3-4.7 17 0l99 99c7.6 7.6 2.2 20.5-8.5 20.5h-67v116c0 6.6-5.4 12-12 12z">
                    </path>
                </svg>
            </div>
        </div>
        <div class="b-bible__bottom">
            <div class="b-bible__link">
                <a id="bbq-link" href="{{$data[0]['link']}}" target="_blank">
                    <span class="b-bible__link-span footer-link" id="bbq-anchor">{!!$data[0]['anchor']!!}</span>
                </a>
            </div>
            <div class="b-policy">
                <p class="privacy-policy">
                    <a class="footer-link" href="{{route('privacy')}}">
                        Политика конфиденциальности
                    </a>
                </p>
            </div>
            <div class="b-copyright__milli">
                <p>Миллион просмотров объявления гарантировано&nbsp;©</p>
            </div>
        </div>
    </div>
</footer>
