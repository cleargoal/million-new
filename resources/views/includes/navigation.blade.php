<section class="b-navigation" style="width: 100%;">
    <div class="container">
        <div class="row">
            <div class="b-navigation-wrp">
                <div class="logo-wrp">
                    <a href="{{route('home')}}">
                        <img class="logo-wrp__img" src="/img/mi-logo.png" alt="logo">
                    </a>
                    <div class="main-menu__btn-wrp">
                        <button class="menu-btn">
                            <span></span>
                        </button>
                    </div>
                </div>
                <div class="main-menu">
                    <div class="main-menu__list-wrp">
                        <ul class="main-menu__list">
                            <li class="main-menu__item">
                                <a href="{{route('home')}}">
                                    Главная
                                </a>
                            </li>
                            <li class="main-menu__item"><a href="#">Работа с объявлением
                                <div class="shev-rot">&#711;</div>
                                </a>
                                <ul class="main-menu__nested-list">
                                    <li><a class="main-menu__nested-list-item" href="{{ route('rootads') }}">Посмотреть образцы</a></li>
                                    <li><a class="main-menu__nested-list-item" href="{{ route('pay-for-ad') }}">Оплатить объявление</a></li>
                                    <li><a class="main-menu__nested-list-item" href="{{ route('create-ad') }}">Опубликовать объявление</a></li>
                                </ul>
                            </li>

                            <li class="main-menu__item"><a href="#">Страница-визитка
                                <div class="shev-rot">&#711;</div>
                                </a>
                                <ul class="main-menu__nested-list">
                                    <li><a class="main-menu__nested-list-item" href="{{ route('pageslist') }}">Посмотреть образцы</a></li>
                                    <li><a class="main-menu__nested-list-item" href="{{route('pay-for-ad')}}">Оплатить визитку</a></li>
                                    <li><a class="main-menu__nested-list-item" href="{{ route('createvisit') }}">Создать свою визитку</a></li>
                                </ul>
                            </li>

                            <li class="main-menu__item"><a href="{{ route('support.faq') }}">
                                    Помощь
                                </a>
                            </li>
                            @if (Auth::guest())
                            <li class="main-menu__item"><a href="{{ url('/register') }}">
                                    Регистрация
                                </a>
                            </li>
                            <li class="main-menu__item">
                                <a class="button" href="{{ url('/login') }}">Войти</a>
                            </li>
                            @else
                            <li class="main-menu__item"><a href="{{ route('profile') }}">
                                    {{ Auth::user()->name }}</a>
                            </li>
                            <li class="main-menu__item">
                                <a class="button" href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    выход
                                </a>
                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
