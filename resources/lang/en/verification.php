<?php

return [

    'verify' => 'Verify Your Email Address',
    'check' => 'Before proceeding, please check your email box for an email containing verification link.',
    'if not' => 'If you did not receive the email, click button below to request another',
    'resend' => 'Send me email',
    'sent' => 'A fresh verification link has been sent to your email address.',
    'expired' => 'Your link has expired. Click on the button to get a new one'
];
