<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'verify_email_address' => 'Verify Email Address',
    'click_button_verify' => 'Please click the button below to verify your email address',
    'no_action_required' => 'If you did not create an account, no further action is required',
    'whoops' => 'Whoops!',
    'hello' => 'Hello!',
    'regards' => 'Regards',
    'if_trouble' => 'If you’re having trouble clicking the button ',
    'copy_paste' => 'copy and paste the URL below into your web browser: ',

];
