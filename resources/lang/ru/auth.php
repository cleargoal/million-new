<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Эти данные не соответствуют нашим записям.',
    'throttle' => 'Слишком много попыток войти. Пожалуйста попробуйте снова через : 5 минут.',

    'verify_email_address' => 'Подтвердите емэйл адрес',
    'click_button_verify' => 'Пожалуйста кликните кнопку ниже, чтобы подтвердить Ваш емэйл',
    'no_action_required' => 'Если Вы не создавали аккаунт, просто проигнорируйте это письмо',
    'whoops' => 'Не получилось....',
    'hello' => 'Приветствуем!',
    'regards' => 'С уважением',
    'if_trouble' => 'Если не произошел переход на сайт кликом по кнопке ',
    'copy_paste' => 'скопируйте и вставьте приведенный ниже URL в адресную строку веб-браузера: ',

];
