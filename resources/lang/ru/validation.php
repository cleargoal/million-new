<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Этот :attribute должен быть принят.',
    'active_url'           => 'Ссылка :attribute неправильная.',
    'after'                => ' :attribute должна быть дата после :date.',
    'alpha'                => 'Строка :attribute может содержать только буквы.',
    'alpha_dash'           => 'Строка :attribute может содержать только буквы, цыфры и дефис.',
    'alpha_num'            => 'Строка :attribute может содержать только буквы и цыфры.',
    'array'                => 'Здесь :attribute должен быть массив.',
    'before'               => ' :attribute должен быть дата до :date.',
    'between'              => [
        'numeric' => 'Аттрибут :attribute должен быть между :min и :max.',
        'file'    => 'Файл :attribute должен быть между :min и :max КБ.',
        'string'  => 'Строка :attribute должна быть между :min и :max символов.',
        'array'   => 'Массив :attribute должен содержать между :min и :max элементов.',
    ],
    'boolean'              => 'Логическое поле :attribute  должно быть true или false.',
    'confirmed'            => 'Это подтверждение :attribute не соответствует.',
    'date'                 => 'Это :attribute не правильная дата.',
    'date_format'          => 'Дата :attribute не соответствует формату :format.',
    'different'            => 'Значения :attribute и :other должны различаться.',
    'digits'               => 'Значение :attribute должно быть :digits цифрами.',
    'digits_between'       => 'Значение :attribute должно быть между :min и :max цифр.',
    'dimensions'           => 'Картинка :attribute имеет неправильный размер.',
    'distinct'             => 'Свойство :attribute поле имеет дублированное значение.',
    'email'                => 'В поле :attribute должен быть валидный email адрес.',
    'exists'               => 'Выбранный :attribute не верный.',
    'file'                 => 'Аттрибут :attribute должен быть файлом.',
    'filled'               => 'Аттрибут :attribute поле необходимо.',
    'image'                => 'Аттрибут :attribute должен быть картинкой.',
    'in'                   => 'Выбранный пункт :attribute не верен.',
    'in_array'             => 'Аттрибут :attribute поле не существует в :other.',
    'integer'              => 'Аттрибут :attribute должен быть целочисленным.',
    'ip'                   => 'Аттрибут :attribute должен быть действительным IP адресом.',
    'json'                 => 'Аттрибут :attribute должен быть действительной JSON строкой.',
    'max'                  => [
        'numeric' => 'Аттрибут :attribute не может быть больше чем :max.',
        'file'    => 'Аттрибут :attribute не может быть больше чем :max КБ.',
        'string'  => 'Аттрибут ":attribute" не может быть больше чем :max символов.',
        'array'   => 'Аттрибут :attribute не может быть больше чем :max позиций.',
    ],
    'mimes'                => 'Аттрибут :attribute должен быть поле типа: :values.',
    'mimetypes'            => 'Аттрибут :attribute должен быть поле типа: :values.',
    'min'                  => [
        'numeric' => 'Атрибут :attribute должен быть не менее :min.',
        'file'    => 'Атрибут :attribute должен быть не менее :min kilobytes.',
        'string'  => 'Атрибут :attribute должен быть не менее :min символов.',
        'array'   => 'Атрибут :attribute должен быть не менее :min штук.',
    ],
    'not_in'               => 'Выбранный :attribute недействителен.',
    'numeric'              => 'Атрибут :attribute должен быть численный.',
    'present'              => 'Это :attribute поле должно существовать.',
    'regex'                => 'Этот :attribute формат недействительный.',
    'required'             => 'Это :attribute поле - обязательное.',
    'required_if'          => 'Это :attribute поле обязательно когда :other равно :value.',
    'required_unless'      => 'Это :attribute поле обязательно если :other нет в списке :values.',
    'required_with'        => 'Это :attribute поле обязательно когда :values настоящее.',
    'required_with_all'    => 'Это :attribute поле обязательно когда :values настоящие.',
    'required_without'     => 'Это :attribute поле обязательно когда :values не настоящие.',
    'required_without_all' => 'Это :attribute поле обязательно когда :values не предоставлены.',
    'same'                 => 'Атрибут :attribute и :other должны соответствовать.',
    'size'                 => [
        'numeric' => 'Атрибут :attribute должен быть :size.',
        'file'    => 'Атрибут :attribute должен быть :size kilobytes.',
        'string'  => 'Атрибут :attribute должен быть :size символов.',
        'array'   => 'Атрибут :attribute должен содержать :size значений или строк.',
    ],
    'string'               => 'Атрибут :attribute должен быть строковым.',
    'timezone'             => 'Атрибут :attribute должен быть действительной зоной.',
    'unique'               => 'Данный :attribute уже занят.',
    'uploaded'             => 'Атрибут :attribute не удалось загрузить.',
    'url'                  => 'Этот :attribute имеет недействительный формат.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'name' => 'Имя',
        'email' => 'E-mail',
        'password' => 'пароля'
    ],

];
