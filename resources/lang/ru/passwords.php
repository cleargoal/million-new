<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен состоять из не менее 6 символов и быть сложным.',
    'reset' => 'Ваш пароль сброшен!',
    'sent' => 'Ссылка для смены пароля отослана на Ваш е-мэйл!',
    'token' => 'Неверный код сброса пароля.',
    'user' => "Пользователь с таким е-мэйл адресом НЕ ЗАРЕГИСТРИРОВАН.",

];
