(function() {
    "use strict";
    const imageOverVideo = document.querySelector(".vizit-video__img");
    const lookTable = document.querySelector("#lookTable");
    const showMatrix = document.querySelector("#showMatrix");
    const closeMatrix = document.querySelector("#closeMatrix");
    const hideImg = document.querySelector(".vizit-video");
    const videoShow = document.querySelector(".frame-wrapper");

    let localHref = location.href;

    // load video by click on picture
    function startVideo() {
        videoShow.innerHTML =
            '<iframe id="video-main" width="100%" height="100%" class="vid" controls="" loop="" ' +
            'src="https://www.youtube.com/embed/qBwhR8iz11Y?autoplay=0&rel=0&showinfo=0"' +
            ' frameborder="0"></iframe>';
        document.querySelector("#video-main").src =
            "https://www.youtube.com/embed/qBwhR8iz11Y?autoplay=1&rel=0&showinfo=0";
        videoShow.classList.add("frame-wrapper--show");
    }

    // show matrix table
    lookTable.addEventListener("click", function() {
        showMatrix.classList.remove("hide");
        showMatrix.classList.toggle("show-matrix--animation");
        showMatrix.classList.add("hide-matrix--animation");
        if (document.querySelector(".show-matrix--animation")) {
            showMatrix.classList.remove("hide-matrix--animation");
        }
    });

    // close matrix table
    closeMatrix.addEventListener("click", function() {
        showMatrix.classList.remove("hide");
        showMatrix.classList.add("hide-matrix--animation");
        showMatrix.classList.remove("show-matrix--animation");
    });

    // show frame-wrapper -"video main page" vizit-video
    hideImg.addEventListener("click", function() {
        imageOverVideo.classList.add("vizit-video__img--hidden");
        startVideo();
    });

})();
