(function () {
    // кнопка гамбургера меню при мобильном разрешении
    const btn = document.querySelector(".menu-btn");
    const mainMenu = document.querySelector(".main-menu__list-wrp");

    btn.addEventListener("click", function (e) {
        e.preventDefault;
        btn.classList.toggle("hamburger_active");
        mainMenu.classList.toggle("displayBlock");
    });
    // конец гамбургера меню

    // раскрывающийся футер
    const arrowBtn = document.querySelector(".b-bible__svg");
    const mainFooter = document.querySelector(".main-footer");

    arrowBtn.addEventListener("click", function (e) {
        e.preventDefault;
        mainFooter.classList.toggle("show-footer");
        arrowBtn.classList.toggle("rotate-svg");
    });
    // раскрывающийся футер

    // Quotes
    const nextQuote = document.querySelector("#next-quote");
    const nextQuoteHref = nextQuote.getAttribute("data-href");
    const nextQuoteToken = nextQuote.getAttribute("data-csrf-token");

    // Takes from DB and shows next quote of Bible \\
    function resolved(response) {
        const bbqText = document.querySelector("#bbq-text");
        const bbqLink = document.querySelector("#bbq-link");
        const bbqAnchor = document.querySelector("#bbq-anchor");
        const responseObject = response.data[0];

        bbqLink.setAttribute("href", responseObject.link);
        bbqAnchor.innerHTML = responseObject.anchor;
        bbqText.innerText = responseObject.text;
    }
    nextQuote.addEventListener("click", function () {
        axios({
            url: nextQuoteHref || window.location.pathname,
            method: "post",
            headers: {
                "X-CSRF-TOKEN": nextQuoteToken,
            },
        }).then(resolved);
    });

})();
