(function() {
    // Toggling profile-tabs
    const tabControllers = document.querySelectorAll(".profile-tabs>li");

    function DisableCurrentTab() {
        const currentTab = document.querySelector(".prof-tab-content.show");
        currentTab.classList.remove("show");
    }

    function EnableNewTab(selector) {
        const newTab = document.querySelector(selector);
        newTab.classList.add("show");
    }

    function DisableCurrentTabController() {
        const currentController = document.querySelector(
            ".profile-tabs>li.active"
        );
        currentController.classList.remove("active");
    }

    function EnableNewTabController(controller) {
        controller.classList.add("active");
    }

    function ToggleTabAndTabController(event) {
        const controllerInner = event.target;
        console.log(controllerInner);
        // const newController = controllerInner.parentElement;
        const newController = controllerInner;
        const newTabSelector = newController.getAttribute("data-id");

        if (newController.classList.contains("active")) {
            return;
        }

        DisableCurrentTabController();
        DisableCurrentTab();
        EnableNewTabController(newController);
        EnableNewTab(newTabSelector);
    }

    tabControllers.forEach(function(controller) {
        controller.addEventListener("click", ToggleTabAndTabController);
        console.log(controller);
    });
})();