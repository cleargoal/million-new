(function () {
    "use strict";
    const closePics = document.querySelector("#close-pics");
    // const popUp = document.querySelector("#popup");
    const visPost = document.querySelector(".visit-post");
    const pictures = document.querySelectorAll(".picts");
    const pictureOptsContainer = document.querySelector(".pictures-block");
    // const visPicture = document.querySelector(".vis-pic");
    const visPictures = document.querySelectorAll(".vis-pic");
    const navigation = document.querySelector(".navigat");

    const outPopup = document.querySelector("#out-popup");
    const videoFrame = document.querySelector("#video-frame");
    const closeVideo = document.querySelector("#close-video");
    let videoContent = "";
    const picturesBlock = document.querySelector("#picturesBlock");

    // Helpers
    function showElements(members) {
        console.log(members);
        members.forEach(function (element) {
            element.classList.remove("hide");
        });
    }

    function hideElements(members) {
        members.forEach(function (element) {
            element.classList.add("hide");
        });
    }

    // Visit show pictures listener \\
    function correctSrcAttributeOfPictures() {
        visPictures.forEach(function (element) {
            const currentPictureSrc = element.getAttribute("src");
            const newPictureSrc = currentPictureSrc.replace(/thum/g, "pics");
            element.setAttribute("src", newPictureSrc);
        });
    }

    function closePicturesBlock() {
        pictures.forEach(function (element) {
            element.classList.remove("fivepic-open");
            element.classList.add("fivepic-close");
        });
        pictureOptsContainer.classList.remove("pictures-block-open");
        pictureOptsContainer.classList.add("pictures-block");
        closePics.classList.add("hide");
    }

    function activateClosePop() {
        console.log("activateClosePop");
        closePics.classList.remove("hide");
        closePics.addEventListener("click", function () {
            closePicturesBlock();
        });
    }

    function OpenPics() {
        pictureOptsContainer.classList.remove("pictures-block");
        pictureOptsContainer.classList.add("pictures-block-open");
        pictures.forEach(function (element) {
            // console.log(element);
            element.classList.remove("fivepic-close");
            element.classList.add("fivepic-open");
        });

        correctSrcAttributeOfPictures();
        activateClosePop();
        console.log(picturesBlock);
        picturesBlock.scrollIntoView({ block: "start", behavior: "smooth" });
        navigation.classList.remove("nvgsticked");
    }

    if (visPictures) {
        visPictures.forEach(function (element) {
            element.addEventListener("click", OpenPics);
        });
    }

    // function hidePrevNextButtons() {
    //     hideElements(vaPrev, vaNext);
    // }

    function showPopupAndVideoFrame() {
        showElements([videoFrame, closeVideo, outPopup]);
    }

    function OpenVideo(video) {
        console.log(video);
        videoContent = video.getAttribute("data-content");
        showPopupAndVideoFrame();
        outPopup.classList.add("popup", "video-frame");
        videoFrame.setAttribute("src", videoContent);
        videoFrame.classList.add("video-frame");
    }

    closeVideo.addEventListener("click", function () {
        outPopup.classList.remove("popup");
        hideElements([videoFrame, closeVideo, outPopup]);
    });

    // Здесь я не могу заранее узнать то, будет ли существовать данный блок. онклик-атрибуты ставить не комильфо, а пыха есть пыха. Приходится довольствоваться такими проверочками.
    if (document.querySelector(".vis-vid")) {
        const videos = document.querySelectorAll(".vis-vid");
        videos.forEach(function (video) {
            video.addEventListener("click", function () {
                new OpenVideo(video);
            });
        });
    }

    document.querySelector(".view-zit-video").onclick = function () {
        outPopup.style.display = "block";
        console.log("123");
    };

})();
