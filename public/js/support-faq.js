(function () {

    let activeAnswer;
    const allAnswers = document.querySelectorAll(".art-list-item");

    // show/hide answer
    allAnswers.forEach(node => {
        node.addEventListener("click", (event) => {
            const hiddenBlock = node.children[1];
            hiddenBlock.classList.toggle("hide");
            if (hiddenBlock.classList.contains("hide") === false) {
                QuestionsClickHandler(node);
            }
        });
    });

    // listener for QA page to show answer \\
    function UpdateFAQDatabase(fieldname, UseHT, faqID) {
        const faqView = document.querySelector("#faqviewhref");
        const href = faqView.getAttribute("data-href");
        const token = faqView.getAttribute("data-csrf-token");

        axios({
            method: "post",
            url: href || window.location.pathname,
            data: {updfieldname: fieldname, faqincrease: UseHT, id: faqID},
            headers: {"X-CSRF-TOKEN": token}
        })
    }

    function QuestionsClickHandler(question) {
        const questionId = question.closest("li").id;
        const viewsAmount = document.querySelector("#view_" + questionId);

        if (activeAnswer) {
            const activeAnswerId = getIntFromStringId(activeAnswer.id);
            const questionIdInt = parseInt(questionId);
            if (activeAnswerId !== questionIdInt) {
                prepareDataSave(viewsAmount, questionId);
            }
        } else {
            prepareDataSave(viewsAmount, questionId);
        }
    }

    function getIntFromStringId(stringId) {
        return parseInt(stringId.match(/\d+/));
    }

    // вынесено в отдельную функцию для обеспечения независимости рсбатывания
    function prepareDataSave(viewsAmount, questionId) {
        viewsAmount.innerHTML = Number(viewsAmount.innerHTML) + 1;

        const faqID = Number(questionId);
        const UseHT = Number(viewsAmount.innerHTML);
        const fieldname = "views";

        UpdateFAQDatabase(fieldname, UseHT, faqID);
    }

    function IncreaseLikesAmount(likes) {
        const likesId = likes.closest("li").getAttribute("id");
        const likesAmount = document.querySelector("#like_" + likesId);

        likesAmount.innerHTML = Number(likesAmount.innerHTML) + 1;

        const faqID = Number(likesId);
        const UseHT = Number(likesAmount.innerHTML);
        const fieldname = "useful";

        UpdateFAQDatabase(fieldname, UseHT, faqID);
    }


    const questionsWrapper = document.querySelector(".bookms");
    if (questionsWrapper.children.length > 0) {
        const questions = document.querySelectorAll(".artily-list-row");
        const likeButtons = document.querySelectorAll(".like-vert");

        likeButtons.forEach(function (button) {
            button.addEventListener("click", function (ev) {
                IncreaseLikesAmount(button);
                ev.stopPropagation();
            }, {once: true})
        })
    }


    // Support FAQ - show form listener \\
    const popup = document.querySelector(".artily-popup");
    const hints = document.querySelectorAll(".hints");
    const showFormButton = document.querySelector(".show-form");

    function ClearTextareaHideHintsAndShowForm() {
        const question = document.querySelector("#question");
        question.value = "";

        hints.forEach(function (hint) {
            hint.classList.add("hide")
        });
        popup.classList.remove("hide");
        question.focus();
    }

    showFormButton.addEventListener("click", ClearTextareaHideHintsAndShowForm);

    // FAQ submiting question. \\
    function resolved() {
        const resolveRequestDisplay = document.querySelector("#resolve");
        // resolveRequestDisplay.classList.remove("hide");
        showResult(resolveRequestDisplay);
    }

    function rejected() {
        const rejectRequestDisplay = document.querySelector("#reject");
        // rejectRequestDisplay.classList.remove("hide")
        showResult(rejectRequestDisplay);
    }

    // показывает результат отсылки формы с вопросом
    function showResult(elem) {
        // create message div
        let questionSent = document.createElement("div");
        questionSent.classList.add("artily-notifications", 'question-result');
        // create message text in h4
        let messageText = document.createElement("h4");
        let textClass = elem.classList[0];
        messageText.classList.add(textClass);
        messageText.innerHTML = elem.innerHTML;
        // create button
        let messageBut = document.createElement("button");
        messageBut.id = "axiClose";
        messageBut.classList.add('btn', 'btn-info-color');
        messageBut.style.marginTop = '2rem';
        messageBut.innerText = 'Понятно';
        // add elements into DOM
        questionSent.append(messageText);
        questionSent.append(messageBut);
        const appCont = document.querySelector("#app");
        appCont.append(questionSent);
        // add EventListener to button
        const axiClose = document.querySelector("#axiClose");
        axiClose.addEventListener("click", function () {
            questionSent.style.opacity = 0;
            let t = setTimeout(function () {
                questionSent.style.display = "none";
                appCont.removeChild(questionSent);
            }, 1200);
        });
    }

    function hidePopup() {
        popup.classList.add("hide");
    }

    function submitQuestion() {
        const sqDataHref = document.querySelector("#sqhref").getAttribute("data-href");
        const token = document.querySelector("#faqviewhref").getAttribute("data-csrf-token");
        const fiData = new FormData(document.forms.quesform);

        axios({
            url: sqDataHref || window.location.pathname,
            method: "post",
            headers: {"X-CSRF-TOKEN": token},
            data: fiData
        })
            .then(resolved)
            .catch(rejected)
            .finally(hidePopup);

        return false; // Чтоб страница не перезагрузилась
    }

    const faqSubmitButton = document.querySelector("#faq_subm");
    faqSubmitButton.addEventListener("click", function (event) {
        event.preventDefault();
        submitQuestion();
        clearUploaded()
    });

    const faqResetButton = document.querySelector("#faq_esc");
    faqResetButton.addEventListener("click", function (event) {
        hidePopup();
        clearUploaded()
    });

    // check is entity exists
    function isset(val) {
        return typeof (val) != "undefined" && val !== null;
    }

    // Clear element that contains iploaded picture
    function clearUploaded() {
        if (isset(uploaded)) {
            uploaded.src = null;
        }
    }

    // показать выбранную для загрузки картинку на форме
    let uploaded = document.querySelector("#uploaded");
    const uploadFile = document.querySelector("#screenshot_q");
    const areaLabel = document.querySelector("#screenshot_l");
    const imgWrapper = document.querySelector("#imgWrapper");
    uploadFile.addEventListener("change", function (event) {
        let files = this.files;
        let file = files[0];
        let fileReader = new FileReader();
        fileReader.onload = function (event) {
            let result = event.target.result;
            areaLabel.classList.add("up-label");
            imgWrapper.classList.add("up-img");
            uploaded.src = result;
        };
        fileReader.readAsDataURL(file);
    });

})();
