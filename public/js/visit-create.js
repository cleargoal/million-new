// это файл также обслуживает страницу visit-edit
(function() {
    "use strict";
    const visPost = document.querySelector("#visit-post");
    const visitHeader = document.querySelector("#visitheader");
    const leftCharactersCount = document.querySelector("#symbols");
    const charactersLimit = 80; // данные для подсчета кол-ва символов при вводе - создание визитки
    let currentLength;

    // валидация номера телефона только цифры
    document
        .querySelector(".cont-contact__tel")
        .addEventListener("keyup", function () {
            this.value = this.value.replace(/[^\d]/g, "");
        });

    function countCharacters() {
        currentLength = visitHeader.value.length;
        leftCharactersCount.textContent = String(
            charactersLimit - currentLength
        );
        return currentLength;
    }

    if (visitHeader) {
        visitHeader.addEventListener("keyup", function () {
            countCharacters();
        });
    }

    // #visitstyles - обёртка над style-back & style-font
    if (document.querySelector("#visitstyles")) {
        const backgroundColor = document.querySelector("#style-back");
        const fontColor = document.querySelector("#style-font");
        backgroundColor.addEventListener("change", function () {
            visPost.style.backgroundColor = backgroundColor.value;
        });
        fontColor.addEventListener("change", function () {
            visPost.style.color = fontColor.value;
        });
    }
})();
