(function () {
    // Ask-form
    const form = document.querySelector('.popup');
    const showFormButtons = document.querySelectorAll('.open-form');

    function setQuestionText(target) {
        const parentLi = target.closest('li');
        const id = parentLi.getAttribute('data-suppid');

        const answeringMode = document.querySelector('#admin-answer');
        const questionId = document.querySelector('#question_id');
        const questionText = document.querySelector('#ques_' + id);

        questionId.value = id;
        answeringMode.textContent = questionText.textContent;
    }

    function openForm(event) {
        form.classList.remove('hide');
        setQuestionText(event.target);
    }

    showFormButtons.forEach(function (button) {
        button.addEventListener('click', openForm);
    });
})();
