(function () {
    const editStyles = document.querySelector("#edit-styles");

    // Throttle-функция
    function throttle(func, ms) {
        let isThrottled = false,
            savedArgs,
            savedThis;

        function wrapper() {
            if (isThrottled) {
                // (2)
                savedArgs = arguments;
                savedThis = this;
                return;
            }

            func.apply(this, arguments); // (1)

            isThrottled = true;

            setTimeout(function () {
                isThrottled = false; // (3)
                if (savedArgs) {
                    wrapper.apply(savedThis, savedArgs);
                    savedArgs = savedThis = null;
                }
            }, ms);
        }

        return wrapper;
    }

    // массив цветовых схем - [0] - color, [1] - background-color
    const colorSchemas = [
        ["#4d5859", "#a6fb1f"],
        ["#f2654b", "#102e0d"],
        ["#d2287f", "#ffffaf"],
        ["#f769a6", "#3d1902"],
        ["#45634b", "#e7fe32"],
        ["#ee5e07", "#220241"],
        ["#444", "#c05"],
        ["#28d343", "#1a3e52"],
        ["#fff", "#c05"],
        ["#96178e", "#eaf152"],
        ["#4fd5ca", "#040357"],
        ["#0a5daa", "#dcf4f1"],
        ["#de8b1e", "#4b361a"],
        ["#cb8bcf", "#ffffff"],
        ["#8bcf98", "#1f37b1"],
        ["#1e7dde", "#fcae05"],
        ["#753bd5", "#fec108"],
        ["#ef8e8e", "#034b95"],
        ["#3ab140", "#bafa97"],
        ["#ae8c64", "#efd98e"],
        ["#122d4f", "#f2f4bc"],
        ["#918bcf", "#5c35bf"],
        ["#82aac3", "#315f81"],
        ["#49a96b", "#2b613e"],
        ["#4084d1", "#ffc000"],
        ["#e8cc32", "#2e6f8a"],
        ["#ffc000", "#4a217d"],
        ["#e7804d", "#eed4ab"],
        ["#1d357b", "#e8cc32"],
        ["#fff798", "#5d8fd4"],
        ["#ea2a2a", "#56d5aa"],
        ["#b8ff98", "#9f1d1d"],
        ["#d3eb4f", "#403db1"],
        ["#972b72", "#f7e99e"],
        ["#9d3939", "#96e2fb"],
        ["#5a4ff2", "#e59c5c"]
    ];
    const colorsArray = [];
    const backgroundColorsArray = [];

    colorSchemas.forEach(function (schema) {
        colorsArray.push(schema[0]);
        backgroundColorsArray.push(schema[1]);
    });

    //
    const adSubmit = document.querySelector("#complement-submit");
    const adLink = document.querySelector("#complement-link");
    const noLink = document.querySelector("#no-link");
    let adLinkLength = 0;

    adLink.addEventListener("keyup", function () {
        adLinkLength = adLink.value.length;
        if (adLinkLength > 11) {
            adSubmit.removeAttribute("disabled");
            adSubmit.removeAttribute("title");
        }
        if (adLinkLength < 12) {
            adSubmit.setAttribute("disabled", true);
            adSubmit.setAttribute(
                "title",
                "Введите данные в поле Ссылка\nили отметьте галочку"
            );
        }
    });

    noLink.addEventListener("click", function () {
        adLinkLength = adLink.value.length;
        if (!noLink.checked && adLinkLength > 11) {
            adSubmit.removeAttribute("disabled");
            adSubmit.removeAttribute("title");
        }
        else if(noLink.checked)
        {
            adSubmit.removeAttribute("disabled");
            adSubmit.removeAttribute("title");
        }
    else
        {
            adSubmit.setAttribute("title", "Введите данные в поле Ссылка");
            adSubmit.setAttribute("disabled", true);
        }
    });

    // enabling Submit button in ad <editing> mode
    if (location.pathname.indexOf("edit-ad") !== -1 && noLink.checked) {
        adSubmit.removeAttribute("disabled");
        adSubmit.removeAttribute("title");
    }
    // end

    const selectStyles = document.querySelector("#select-styles");
    let selectStylesChildren = selectStyles.options;
    const fontSize = document.querySelector("#fsize");
    const enteredText = document.querySelector("#complement-text");

    // начальные значения стиля области просмотра
    const textViewArea = document.querySelector("#complement-view-area");
    textViewArea.style.color = colorsArray[0];
    textViewArea.style.backgroundColor = backgroundColorsArray[0];

    colorSchemas.forEach(function (schema, index) {
        const option = document.createElement("option");

        option.setAttribute("value", schema);
        option.setAttribute("id", "opt" + index + 1);
        option.setAttribute("class", "opts");

        option.textContent = "Цветовая схема " + index + 1;
        option.style.color = schema[0];
        option.style.backgroundColor = schema[1];

        selectStyles.append(option);
    });

    // ***  при создании объявления: задается стиль - цвет текста и фона  ***
    selectStyles.addEventListener("change", function () {
        const schema = selectStyles.value.split(",");
        textViewArea.style.color = schema[0];
        textViewArea.style.backgroundColor = schema[1];
    });

    // изменение размера шрифта области просмотра
    fontSize.addEventListener("change", function () {
        const size = fontSize.value;
        textViewArea.style.fontSize = size;
    });

    // данные для подсчета кол-ва символов при вводе - создание объявления
    const limit = 160;
    const typeChars = document.querySelector("#complement-type");
    const leftChars = document.querySelector("#complement-left");

    /** выводится в область просмотра текст, который вводит юзер, также подсчитывается и выводится кол-во введенных и оставшихся знаков -  */
    enteredText.addEventListener(
        "keyup",
        throttle(function () {
            textViewArea.textContent = enteredText.value;
            const objectValue = enteredText.value;
            const count = objectValue.length;
            const num = limit - count;

            typeChars.textContent = count;
            leftChars.textContent = num;
        }, 500)
    );

    if (editStyles) {
        const esStyles = editStyles.getAttribute("data-stls");
        let esStylesSplitted = esStyles.split(",");

        let esFontSize = editStyles.getAttribute("data-fnts");

        textViewArea.style.color = esStylesSplitted[0];
        textViewArea.style.backgroundColor = esStylesSplitted[1];
        textViewArea.style.fontSize = esFontSize;

        let gotStyleIndex;

        colorSchemas.forEach(function (value, index) {
            if (colorSchemas[index].join() === esStyles) {
                gotStyleIndex = index;
                return false;
            }
        });

        selectStylesChildren[gotStyleIndex].setAttribute("selected", true);
        selectStyles.style.color = esStylesSplitted[0];
        selectStyles.style.backgroundColor = esStylesSplitted[1];

        const fontSizeChildren = Array.from(fontSize.children);

        fontSizeChildren.forEach(function (option, index) {
            if (option.value === esFontSize) {
                fontSizeChildren[index].setAttribute("selected", true);
                return false;
            }
        });
    }

    /** closing 'popup' with message about view X examples of upline ads */
    if (document.querySelector("#message-ok")) {
        const okMessage = document.querySelector("#message-ok");
        const messages = document.querySelector(".messages");
        const adView = document.querySelector("#adview");

        okMessage.addEventListener("click", function () {
            messages.classList.add("hide");
            adView.classList.remove("hide");
        });
    }
})();
