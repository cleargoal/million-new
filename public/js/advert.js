const okMessage = document.querySelector("#message-ok");
const popup = document.querySelector(".messages");
const adView = document.querySelector("#adview");
const buttNext = document.querySelector("#butnext");
const buttPrev = document.querySelector("#butprev");
// const bookCheck = document.querySelector("#");
const bookmark = document.querySelector("#bookmrk");
const buttonPan = document.querySelector("#buttpan");
const token = buttonPan.getAttribute("data-csrf-token");
const timerWrapper = document.querySelector(".next-ad-timer-field-parent");
const adsCountElement = document.querySelector("#adsCount");
let adsCountCount = 0;
const bmkWordsElement = document.querySelector("#bmkWords");
const saveBook = document.querySelector("#saveBook");
const viewSavedAd = document.querySelector("#viewsavedad");
let adsCollection = "";
const blinkArea = document.querySelector("#blink-area");
let collIndex = 0;

(function () {
    "use strict";
    //            Просмотр объявления - START
    // adsCollection = viewSavedAd.getAttribute("data-ads");
    adsCollection = JSON.parse(viewSavedAd.getAttribute("data-ads"));
    console.log(
        "adsCollection",
        adsCollection,
        adsCollection[0].id
    );
    const adsCollectionLength = adsCollection.length;
    collIndex = 0;

    function saveAdView() {
        const viewHref = buttonPan.getAttribute("data-href");

        axios({
            url: viewHref || window.location.pathname,
            method: "post",
            headers: {"X-CSRF-TOKEN": token},
            data: {ads_id: adsCollection[collIndex].id}
        });
    }

    /** Bookmark blinks */
    function blinker() {
        let blink;
        setTimeout(function () {
            blink = setInterval(function () {
                blinkArea.classList.toggle("bookmrkhl");
            }, 500);
        }, 2000);
        setTimeout(function () {
            clearInterval(blink);
            blinkArea.classList.remove("bookmrkhl");
        }, 7000);
    }// end of blink func

    function setRemoveAttrDisabled(attr) {
        if (attr === 'set') {
            buttNext.setAttribute("disabled", "true");
            buttPrev.setAttribute("disabled", "true");
        } else if (attr === 'rem') {
            buttNext.removeAttribute("disabled");
            if (collIndex > 0) {
                buttPrev.removeAttribute("disabled");
            }
        }
    }

    /* ------- START_AdViewTimer_START ------- */
    function viewAdsTimer() {
        const constTimerValue = 10; //30;
        const constLengthOfCircle = 189;
        let circularAnimationLap = document.querySelector(".segmented_circle");
        let inCircleText = document.querySelector(".div_counter_text");
        let varCircleSegment = 0;
        let second = 9; // здесь должно быть на 1 сек меньше, чем общая длительность таймера: 29;
        setRemoveAttrDisabled('set');
        (function incrementCircleSegmentAndDecrementTimer() {
            let interval = setInterval(function () {
                circularAnimationLap.style.strokeDashoffset =
                    String((varCircleSegment + 1) * (constLengthOfCircle / constTimerValue));
                if (second === 0) {
                    clearInterval(interval);
                    setRemoveAttrDisabled('rem');
                }
                inCircleText.innerText = second;
                varCircleSegment += 1;
                second -= 1;
            }, 1000);
        })();
    }

    /* --------- END_AdViewTimer_END --------- */

    function BookmarksCollection() {
        const bookmarks = [];
        // let bookmarkCount = -1; // bookmarks counter
        adsCollection.forEach(function (item) {
            if (item.bookMarker) {
                // bookmarkCount++;
                // bookmarks[bookmarkCount] = item.id;
                bookmarks.push(item.id);
            }
        });
        return bookmarks;
    }

    function saveAndGoBookmark() {
        const bookmarksCollection = BookmarksCollection();
        console.log(bookmarksCollection);
        const nextPan = document.querySelector("#pan-next");
        const bookHref = nextPan.getAttribute("data-href");
        const listBookmarks = document.querySelector("#butnext");
        const listHref = listBookmarks.getAttribute("data-href");
        axios
            .post(bookHref, bookmarksCollection, {
                headers: {"X-CSRF-TOKEN": token}
            })
            .then(function () {
                window.location.href =
                    listHref + "?bookmark_ids=" + bookmarksCollection;
            });
    }

    function showAd(crease) {
        collIndex += crease;
        if (collIndex >= adsCollectionLength) {
            saveAndGoBookmark();
        } else {
            /** Bookmark checkbox turn on/off */
            // console.log(adsCollection[collIndex]);
            adsCollection[collIndex].bookMarker = adsCollection[collIndex].bookMarker !== undefined
                ? adsCollection[collIndex].bookMarker
                : false;

            bookmark.checked = adsCollection[collIndex].bookMarker;
            if (collIndex < 1) {
                buttPrev.setAttribute("disabled", true);
            } else {
                buttPrev.removeAttribute("disabled");
            }

            viewSavedAd.innerHTML = adsCollection[collIndex].adtext;

            let esStylesSplit = adsCollection[collIndex].adstyle.split(",");
            let esFontSize = adsCollection[collIndex].adfont;

            // Setting color, background-color and font-size for ad's area.
            viewSavedAd.style.color = esStylesSplit[0];
            viewSavedAd.style.backgroundColor = esStylesSplit[1];
            viewSavedAd.style.fontSize = esFontSize;

            if (collIndex > 0) {
                if (timerWrapper) {
                    viewAdsTimer();
                }
            }

            adView.classList.contains("hide") === false
                ? saveAdView()
                : "";
        }
    }

    // add 'bookMarker' field set it to true
    if (bookmark) {
        bookmark.addEventListener("click", function () {
            // let getChecked = bookCheck.checked;
            let getChecked = bookmark.checked;
            console.log('click bookmark', 'getChecked', getChecked);
            if (adsCountElement) {
                showAdsCounter(getChecked);
            }
            adsCollection[collIndex].bookMarker = getChecked;

            // const glyphiconCheck = document.querySelector(".artily-bookmark-glyph-check");
            const goToBookmark = document.querySelector(".artily-gotobmk");

            if (adsCountElement) {
                if (getChecked || adsCountCount > 0) {
                    goToBookmark.classList.remove("hide");
                } else {
                    goToBookmark.classList.add("hide");
                }
            }
        });
    }

    buttPrev.addEventListener("click", function () {
        showAd(-1);
    });

    buttNext.addEventListener("click", function () {
        showAd(1);
        // blinker();
    });

    function showAdsCounter(getChecked) {
        adsCountCount = getChecked === true ? ++adsCountCount : --adsCountCount;
        adsCountElement.textContent = '' + adsCountCount;
        bmkWordsElement.textContent = setBmkWord(adsCountCount);
    }

    if (okMessage) {
        okMessage.addEventListener("click", function () {
            popup.classList.add("hide");
            adView.classList.remove("hide");
            showAd(0);
            viewAdsTimer();
        });
    } else {
        showAd(0);
    }

    if (saveBook) {
        saveBook.addEventListener("click", saveAndGoBookmark);
    }

    function setBmkWord(adc) {
        let retVal = "закладок";
        let bmkWordsAr = ["закладку", "закладки", "закладок"];
        switch (true) {
        case adc === 0:
            retVal = bmkWordsAr[2];
            break;
        case adc === 1:
            retVal = bmkWordsAr[0];
            break;
        case adc > 1 && adc < 5:
            retVal = bmkWordsAr[1];
            break;
        case adc > 4:
            retVal = bmkWordsAr[2];
        }
        return retVal;
    }

    // blinker();
})();
