<?php

namespace Tests\Feature;

use App\Mail\MailSender;
use App\User;
use App\Userad;
use App\UserStat;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class ViewLimitControlTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp():void
    {
        parent::setUp();

        $this->author = factory(User::class)->create();
        $this->userAd = factory('App\Userad')->create(['user_id' => $this->author->id]);
        factory(UserStat::class)->create(['user_id' => $this->author->id, 'record_id' => $this->userAd->id, 'views' => 131068]);
    }

    /** @test */
    public function it_hits_endpoint()
    {
        $responce = $this->post('/adview', ['ads_id' => $this->userAd->id]);
        $responce->assertStatus(200);

    }

    /** @test */
    public function it_increased_amount_of_view()
    {
        $responce = $this->post('/adview', ['ads_id' => $this->userAd->id]);
        $this->assertDatabaseHas('user_stats', ['views' => 131069]);

    }

    /** @test */
    public function it_close_user_ad_for_viewing()
    {
        $this->post('/adview', ['ads_id' => $this->userAd->id]);
        $this->post('/adview', ['ads_id' => $this->userAd->id]);

        $this->assertDatabaseHas('userads', ['id' => $this->userAd->id, 'actual' => false]);

    }

    /** @test */
    public function it_send_notification_email()
    {
        Mail::fake();

        Mail::assertNothingSent();

        $this->post('/adview', ['ads_id' => $this->userAd->id]);
        $this->post('/adview', ['ads_id' => $this->userAd->id]);

        Mail::assertSent(MailSender::class);

    }
}
