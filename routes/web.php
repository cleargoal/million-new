<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Illuminate\Http\Request;
use \App\Http\Controllers;

Auth::routes(['verify' => true]);

/* ALL routes in group - for registered Users */
Route::group(
    ['middleware' => ['web', 'auth', 'verified']],
    function () {

        // Show profile page
        Route::get('/profile', 'ProfileController@index')->name('profile');
        // Show profile page
        Route::get('/profile/ads', 'ProfileController@ads')->name('profile-ads');
        Route::get('/profile/vis', 'ProfileController@vis')->name('profile-vis');
        Route::get('/profile/pay', 'ProfileController@pay')->name('profile-pay');
        Route::get('/profile/bookmarks', 'ProfileController@bookmarks')->name('profile-bookmarks');
        Route::get('/profile/viewvis', 'ProfileController@viewvis')->name('profile-viewvis');
        Route::get('/profile/invites', 'ProfileController@invites')->name('profile-invites');

        // Show Ad constructor page
        Route::get('/create-ad', 'UseradController@createAd')->name('create-ad'); // creation form

        // Save new Ad
        Route::post('/save-ad', 'UseradController@doCreateAd')->name('do-create-ad'); // save ad

        // Show saved Ad page
        Route::get('/adview/{usersad}', 'AdviewController@savedAd')->name('adview');

        // edit Ad
        Route::get('/edit-ad/{adid}', 'UseradController@editAd')->name('edit-ad');
        Route::post('/update-ad', 'UseradController@updateAd')->name('update-ad');

        // Show 20 upline Ads
        Route::get('/viewads/{adstoview}', 'AdviewController@viewAds')->name('viewAds');

        // call Controller and Show page - choose payment method
        //	Route::get('/choose-payment-method', 'PayServController@getAll')->name('payment-method');

        // call Controller and Show page - make payment
        //	Route::get('/make-payment/{payslug}', 'PayServController@makePayment')->name('route-payment');

        Route::get(
            '/registered',
            function () {
                return view('registered');
            }
        )->name('registered');

        // Visit page in create mode
        Route::get('/createvisit', 'VisitController@createPage')->name('createvisit');
        // Visit page in edit mode
        Route::get('/editvisit/{vicard}', 'VisitController@editPage')->name('edit.visit');
        // Controller save new page
        Route::post('/visitingpage', 'VisitController@saveVisitPage')->name('savevisit');
        /** Visit page in update mode  */
        Route::post('/updatevisitingpage', 'VisitController@updateVisitPage')->name('update-visit');
        // Visit page with mode = 's'
        Route::get('/view_saved_page/{vicard}', 'VisitController@viewSavedPage')->name('view.saved.page');

        // payment: pay_for_ad - page with payment buttons
        Route::get('/payment/pay_for_ad', 'PaymentController@makePayment')->name('pay-for-ad');

        // Route::get(
        //     '/payment/need_payment',
        //     function () {
        //         return view('/payment/need_payment/{product}');
        //     }
        // )->name('payment.need_payment');

    }
);


/** *****  END of Group with Auth ***********/

/*** save Logging of viewed Ad ***/
Route::post('/adview/', 'AdviewController@saveViewedAd')->name('adviewed');

/**** show bookmarks list page ***/
Route::get('/bookmarks-list/', 'BookmarksListController@show')->name('list-bookmarks');

// save bookmarks list
Route::post('/bookmarks/', 'BookmarksListController@saveBookmark')->name('save-bookmark');

// save log bookmark klicked
Route::post('/saveclick/', 'BookmarksListController@logLink')->name('loglinkbmk');

// Show Visiting page by visit's SLUG
Route::get('/visiting-page/{vicard}', 'VisitController@viewSavedPage')->name('showvslug'); // vicard - is visit slug
/** Show Visiting pages list by user's SLUG */
Route::get('/pageslist/{uslug}', 'VisitController@usVisList')->name('listuslug');

Route::get('/pageslist/', 'VisitController@viewVisits')->name('pageslist');

/** Show Ads page */
Route::get('/rootads', 'AdviewController@rootAds')->name('rootads');

/** Show collect BTC page */
Route::get('/collect-btc', 'BtcController@collectbtc')->name('collectbtc');

/** Show Support FAQ page */
Route::get('/support-faq', 'SupportFaqController@supportfaq')->name('support.faq');

/** Save Support FAQ item viewed */
Route::post('/support-faq', 'SupportFaqController@faqViewSave')->name('save.faq.view');

/** Save Support FAQ useful mark or quantity */
Route::post('/save_faq_ques', 'SupportFaqController@saveFaques')->name('save-support-question');

// go through Bible Quotes
Route::post('/getBbq', 'BibleQuotesController@getBbq')->name('next.bible.quote');

/** send mail */
Route::get('/render-mail/', 'MailSendController@trySendMail')->name('render-mail'); // GET method
Route::post('/render-mail/', 'MailSendController@trySendMail')->name('render-mail'); // or POST method

/** subscribe member to news letter */
Route::post('/subscribe', 'SubscribeController@newSubscriber')->name('subscribe');

/** subscribe member to news letter */
Route::get('/privacy', function () {
    return view('/includes/privacy');
})->name('privacy');

Route::get('/verify-email', function () {
    return view('/notify/verify_email');
})->name('notify.verify-email');


/** CHECKERS */
/** check email sending */
Route::get('mail-check', function () {
    return view('check_send_mail');
})->name('check-email-form');
/** check email sending */
Route::post('mail-check', 'CheckSendMailController@checkSend')->name('check-email-send');
/** check email sending */
Route::get('paycheck', 'ReportsController@getNotPaidMembersList')->name('getNotPaidMembersList');
/** END */

// get slug from request and show register page
Route::get('/register/{url_slug}', 'HomeController@slugReferralRegister')->name('slug-referral-register');

// ******* Shows main page ******/
// ** Get SLUG from request - into session */
Route::get('/{url_slug}', 'HomeController@processSlugReferralLink')->name('slug-referral-link');

// Laravel Log Viewer
//Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('/', function () {
    return view('frontpage');
})->name('home');
