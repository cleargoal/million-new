<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/** service URL for WayForPay response */
Route::post('/payment/payed_success', 'PaymentController@checkPayment')->name('checkpayment');
Route::get('/payment/payed_success', 'PaymentController@checkPayment')->name('checkpaym');

/** payment: check WFP result */
Route::post('/payment/approved/{product_slug}', 'PaymentController@paidSuccess')->name('paid-success');
Route::post('/payment/declined/{product_slug}', 'PaymentController@paidUnsuccess')->name('paid-unsuccess');

/** reports */
Route::post('/report_not_paid', 'ReportsController@getNotPaidMembersList')->name('report-not-paid');
Route::get('/report_not_paid', 'ReportsController@getNotPaidMembersList')->name('report-not-paid');

Route::get('/check_axios', 'HomeController@checkAxios')->name('check-axios');
