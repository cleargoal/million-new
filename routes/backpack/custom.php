<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('user', 'UserCrudController');
    Route::crud('viewstatistics', 'ViewStatisticsCrudController');
    Route::get('user/{id}/children', 'UserCrudController@children');
    Route::get('user/{id}/parent', 'UserCrudController@parent');
    Route::crud('supp_faq', 'Supp_faqCrudController');
    Route::get('supp_faq/{id}/answer', 'Supp_faqCrudController@answer');
    Route::post('supp_faq/{faq_id}/send-answer', 'Supp_faqCrudController@sendAnswer');
    Route::crud('biblequote', 'BiblequoteCrudController');
    Route::crud('logtype', 'LogTypeCrudController');
    Route::crud('payment', 'PaymentCrudController');
    Route::crud('userad', 'UseradCrudController');
    Route::crud('vicard', 'VicardCrudController');
}); // this should be the absolute last line of this file