<?php

return [

    /*
    |--------------------------------------------------------------------------
    | УРЛ на сайт Библии
    |--------------------------------------------------------------------------
    | на этот УРЛ я пересылаю посетителя после клика на ссылку место Писания
    |
    */

    'bible_url' => 'https://allbible.info/bible/modern/',

    /*
    |--------------------------------------------------------------------------
    | Типы ресурсов на сайте
    |--------------------------------------------------------------------------
    | эти типы ресурсов сайта применяются в нескольких местах;
    | [0] - имя для вывода на страницу подтверждения оплаты
    | [1] - имя таблицы соответствующего ресурса
    | ATTENTION - currently NOT used
    |
    */
    'test' => ['тест', ''],
    'adv' => ['Объявление', 'userads'],
    'vis' => ['Визитка', 'vicards'],

    /*
    |--------------------------------------------------------------------------
    | Compiled View Path
    |--------------------------------------------------------------------------
    |
    | This option determines where all the compiled Blade templates will be
    | stored for your application. Typically, this is within the storage
    | directory. However, as usual, you are free to change this value.
    |
    */

    'tables' => [
        ['biblequotes', 'Цитаты из Библии'],
        ['logtypes', 'Типы сущностей для Лога'],
        ['payments', 'Платежи'],
        ['userads', 'Объявления'],
        ['vicards', 'Страницы визитки'],
    ],

    'og_url' => 'toolkit-shelley-nautical-outlay',
    /* different settings **********
    cookiesLong - cookies Longest
     * */
    'cookiesLong' => 60*24*7,
    'adViewLimit' => 131070

];
