<?php
return [
    /** amount of ads that user needs to view before create own ad */
		'adsAmount' => 20,
    /** site OG description */
    'ogdescrip' => "Миллион просмотров объявления гарантировано | 1 > 1 000 000. ",
    
    /** ad view modes */
    's' => 's', // view _saved_ ad
    'r' => 'r', // view ads of _root_
    'b' => 'b', // _base_ mode - view of uplines ads
    'm' => 'm', // _more_ - if user wants to see more than base mode ads

    /** create or edit modes */
    'e' => 'e', // _edit_
    
    /** visit cards modes */
    'v' => 'v', //
    
];