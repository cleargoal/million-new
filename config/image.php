<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver'        => 'gd',
    'avatars'       => 'app' . DIRECTORY_SEPARATOR .'public' . DIRECTORY_SEPARATOR . 'avatars' . DIRECTORY_SEPARATOR,
    'avatars_folder'  => 'public' . DIRECTORY_SEPARATOR . 'avatars' . DIRECTORY_SEPARATOR,
    'pictures'      => 'public' . DIRECTORY_SEPARATOR . 'pictures' . DIRECTORY_SEPARATOR, // for controller - treat pictures
    'view_pict'     => 'storage' . DIRECTORY_SEPARATOR . 'pictures' . DIRECTORY_SEPARATOR, // for view - show pictures
    'dir_prefix'    => '.' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR,
    'fold'          => DIRECTORY_SEPARATOR . 'fold' . DIRECTORY_SEPARATOR, // 'fold' interceptor
    'fif'           => '500', // amount picture files in numbered folder
    'pics_folder'   => ['pics', 'thum'], // structure of numbered folder with pictures
    'param_name'    => 'folder_number', // in DB the NAME of parameter that contains actual 'pictures' folder
];
