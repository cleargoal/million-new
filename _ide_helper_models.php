<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Biblequote
 *
 * @property int $id
 * @property string $biblequote
 * @property string $biblebook
 * @property string $biblebookln
 * @property int $bblbooktop
 * @property string $bblbooktopln
 * @property int $bibleverse
 * @property string $bibleverseln
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Biblequote newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Biblequote newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Biblequote query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Biblequote whereBblbooktop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Biblequote whereBblbooktopln($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Biblequote whereBiblebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Biblequote whereBiblebookln($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Biblequote whereBiblequote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Biblequote whereBibleverse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Biblequote whereBibleverseln($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Biblequote whereId($value)
 */
	class Biblequote extends \Eloquent {}
}

namespace App{
/**
 * App\Logging
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $src_id
 * @property string $log_slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User|null $user
 * @property-read \App\Userad $userad
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Logging newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Logging newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Logging query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Logging whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Logging whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Logging whereLogSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Logging whereSrcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Logging whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Logging whereUserId($value)
 */
	class Log extends \Eloquent {}
}

namespace App{
/**
 * App\Logtype
 *
 * @property int $id
 * @property string $type_slug
 * @property string $type_descr
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Logtype newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Logtype newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Logtype query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Logtype whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Logtype whereTypeDescr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Logtype whereTypeSlug($value)
 */
	class Logtype extends \Eloquent {}
}

namespace App{
/**
 * App\Myconfig
 *
 * @property int $id
 * @property string $parameter
 * @property string|null $param_descriptor
 * @property string|null $str_value
 * @property int|null $int_value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Myconfig newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Myconfig newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Myconfig query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Myconfig whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Myconfig whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Myconfig whereIntValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Myconfig whereParamDescriptor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Myconfig whereParameter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Myconfig whereStrValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Myconfig whereUpdatedAt($value)
 */
	class Myconfig extends \Eloquent {}
}

namespace App{
/**
 * App\Payment
 *
 * @property int $id
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property float|null $amount
 * @property string|null $currency
 * @property string|null $product
 * @property int|null $product_id
 * @property string|null $email
 * @property string|null $pay_processor
 * @property string|null $orderReference
 * @property string|null $authCode
 * @property string|null $phone
 * @property string|null $cardPan
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereAuthCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereCardPan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereOrderReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment wherePayProcessor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereUserId($value)
 */
	class Payment extends \Eloquent {}
}

namespace App{
/**
 * App\Payservice
 *
 * @property int $id
 * @property string $payslug
 * @property string $name
 * @property string $descript
 * @property float $cost
 * @property string $purse
 * @property string $active
 * @property string|null $extdata
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payservice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payservice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payservice query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payservice whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payservice whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payservice whereDescript($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payservice whereExtdata($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payservice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payservice whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payservice wherePayslug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payservice wherePurse($value)
 */
	class Payservice extends \Eloquent {}
}

namespace App{
/**
 * App\Supp_faq
 *
 * @property int $id
 * @property string $question
 * @property string|null $screenshot_q
 * @property string|null $answer
 * @property string|null $screenshot_a
 * @property string|null $faqdepkey
 * @property string|null $faqdepname
 * @property int $useful
 * @property int $views
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Supp_faq newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Supp_faq newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Supp_faq query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Supp_faq whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Supp_faq whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Supp_faq whereFaqdepkey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Supp_faq whereFaqdepname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Supp_faq whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Supp_faq whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Supp_faq whereScreenshotA($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Supp_faq whereScreenshotQ($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Supp_faq whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Supp_faq whereUseful($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Supp_faq whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Supp_faq whereViews($value)
 */
	class Supp_faq extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $closure_table_parent_id
 * @property int $closure_table_position
 * @property int $closure_table_real_depth
 * @property string|null $deleted_at
 * @property int $invitor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Userad[] $ads
 * @property-read int|null $ads_count
 * @property int $parent_id
 * @property int $position
 * @property-read int $real_depth
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereClosureTableParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereClosureTablePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereClosureTableRealDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereInvitor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\Userad
 *
 * @property int $id
 * @property int $user_id
 * @property string $user_slug
 * @property string $adtext
 * @property string|null $adlink
 * @property string $adstyle
 * @property string $adfont
 * @property int $actual
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Userad newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Userad newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Userad query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Userad whereActual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Userad whereAdfont($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Userad whereAdlink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Userad whereAdstyle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Userad whereAdtext($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Userad whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Userad whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Userad whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Userad whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Userad whereUserSlug($value)
 */
	class Userad extends \Eloquent {}
}

namespace App{
/**
 * App\UserClosure
 *
 * @property int $closure_id
 * @property int $ancestor
 * @property int $descendant
 * @property int $depth
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserClosure newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserClosure newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserClosure query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserClosure whereAncestor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserClosure whereClosureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserClosure whereDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserClosure whereDescendant($value)
 */
	class UserClosure extends \Eloquent {}
}

namespace App{
/**
 * App\UserStat
 *
 * @property int $id
 * @property int $user_id
 * @property int $ads_count
 * @property int $ads_count_day
 * @property \Illuminate\Support\Carbon $ads_count_day_update
 * @property int $vis_count
 * @property int $vis_count_day
 * @property \Illuminate\Support\Carbon $vis_count_day_update
 * @property int $bookmarks_count
 * @property int $links_count
 * @property int $links_count_day
 * @property string $links_count_day_update
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat whereAdsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat whereAdsCountDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat whereAdsCountDayUpdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat whereBookmarksCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat whereLinksCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat whereLinksCountDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat whereLinksCountDayUpdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat whereVisCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat whereVisCountDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserStat whereVisCountDayUpdate($value)
 */
	class UserStat extends \Eloquent {}
}

namespace App{
/**
 * App\Vicard
 *
 * @property int $id
 * @property string $visi_slug
 * @property int $user_id
 * @property string $user_slug
 * @property string|null $avatar
 * @property string $header
 * @property int $price
 * @property string $description
 * @property string|null $pictures
 * @property string|null $videos
 * @property string|null $contacts
 * @property string|null $links
 * @property string $styles
 * @property int $actual
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $currency
 * @property string $email
 * @property string|null $phone
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereActual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereContacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereLinks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard wherePictures($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereStyles($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereUserSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereVideos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vicard whereVisiSlug($value)
 */
	class Vicard extends \Eloquent {}
}

