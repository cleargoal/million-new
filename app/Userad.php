<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userad extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;

    protected $fillable = [
        'user_id', 'user_slug', 'adtext', 'adlink', 'adstyle', 'adfont', 'actual',
    ];

    public $timestamps = true;

    private function users()
    {
        return $this->belongsToMany('App\User', 'logs', 'src_id', 'user_id');
    }

    public function bookmarkers()
    {
        return $this->users()->wherePivot('log_slug', 'bmk');
    }

    public function views()
    {
        return $this->hasMany(UserStat::class, 'record_id');
    }
}
