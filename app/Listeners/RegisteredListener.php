<?php

namespace App\Listeners;

use App\Helpers\UserHelper;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Events\Verified;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;


/**
 * Class Registered Listener - to listen registrations
 *
 * @category Listener
 * @package  Listeners
 * @author   Yef <yef@mail.net>
 * @license  MIT http://license.org
 * @link     http://license.org
 */
class RegisteredListener
{
    /**
     *  Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->user = $user;
    }

    /**
     *  Handle the event and create User's slug
     *
     * @param Verified $event event
     *
     * @return void
     */
    public function createUserSlug(Verified $event)
    {
        $userHelp = new UserHelper;
        $userHelp->createUserSlug($event->user->id);
        $userHelp->userVerified($event->user);
    }

    /**
     *  Handle the event and send verify email
     *
     * @param Registered $event event
     *
     * @return void
     */
    public function sendVerifyEmail(Registered $event)
    {
        if ($event->user instanceof MustVerifyEmailContract && ! $event->user->hasVerifiedEmail()) {
            $event->user->sendEmailVerificationNotification();
        }
    }

    /**
     * Subscribe this Listener to events
     *
     * @param $events registered
     *
     * @return void
     */
    public function subscribe($events)
    {
        $events->listen(
            Verified::class,
            'App\Listeners\RegisteredListener@createUserSlug'
        );

        $events->listen(
            Registered::class,
            'App\Listeners\RegisteredListener@sendVerifyEmail'
        );

    }

}
