<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStat extends Model
{
    protected $fillable = [
        'user_id',
        'record_id',
        'record_type',
        'views',
        'views_today'
    ];

    protected $dates = [
        'daily_update'
    ];
}
