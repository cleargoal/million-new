<?php

namespace App;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Franzose\ClosureTable\Contracts\EntityInterface;
use Franzose\ClosureTable\Models\Entity;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Mail\MustVerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Illuminate\Support\Facades\DB;

class User extends Entity implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    MustVerifyEmailContract,
    HasLocalePreference
{

    use CrudTrait;
    use Notifiable;
    use Authenticatable, Authorizable, CanResetPassword;
    use MustVerifyEmail;

    /**
     * The table associated with the model.     *
     *
     * @var string
     */
    protected $table = 'users';

    protected $locale = 'ru';

    /**
     * ClosureTable model instance.     *
     *
     * @var UserClosure
     * */
    protected $closure = 'App\UserClosure';

    /**     * The attributes that are mass assignable.     *     * @var array */
    protected $fillable = [
        'name', 'email', 'password', 'invitor',
    ];

    /**      * The attributes that should be hidden for arrays.     *     * @var array */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $timestamps = true;


    public function scopeViewStatistics($query)
    {
        return $query->select('users.email', DB::raw("count(userads.user_id) AS count_users_ads"), 'user_details.user_data_val')
            ->join('userads', 'users.id', 'userads.user_id')
            ->join('logs', 'userads.id', 'logs.src_id')
            ->join('user_details', 'users.id', 'user_details.user_id')
            ->groupBy('users.id', 'users.email', 'user_details.user_data_val')
            ->orderBy('count_users_ads', 'desc');
    }

    /**      * This is necessary for Closure Table stuff  */
    public function getParentIdColumn()
    {
        return 'closure_table_parent_id';
    }

    public function getPositionColumn()
    {
        return 'closure_table_position';
    }

    public function getRealDepthColumn()
    {
        return 'closure_table_real_depth';
    }
    /* end of Closure Table stuff */


    /* methods for serve viewing ads */
    public function ads()
    {
        return $this->belongsToMany('App\Userad', 'logs', 'user_id', 'src_id')
            ->withPivot('log_slug')->withTimestamps();
    }

    public function bookmarks()
    {
        return $this->ads()->wherePivot('log_slug', 'bmk');
    }

    public function addToBookmarks($bookmark_ids)
    {
        $this->bookmarks()->attach($bookmark_ids, ['log_slug' => 'bmk']);
    }

    /* end of serve viewing ads */

    /**
     * Get the user's preferred locale.
     *
     * @return string
     */
    public function preferredLocale()
    {
        return $this->locale;
    }

    public function getDefaultReferralSlug()
    {
        $parentId = $this->where('closure_table_parent_id', null)->value('id');
        if ($parentId !== null) {
            $referalUser = $this->where('closure_table_parent_id', $parentId)->value('id');
            $slug = DB::table('user_details')->where('user_id', $referalUser)->value('user_data_val');

            return $slug;
        }
    }

    public function details()
    {
        return $this->hasOne(UserDetails::class, 'user_id', 'id');
    }
}
