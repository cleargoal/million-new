<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vicard extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    protected $fillable = [
        'visi_slug', 'user_id', 'user_slug', 'avatar', 'header', 'price', 'description', 'pictures', 'videos', 'contacts', 'links', 'styles', 'actual', 'currency', 'email', 'phone',
    ];

    public $timestamps = true;

    public function medias()
    {
        return $this->hasMany('App\Media');
    }
}
