<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Payment extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    protected $fillable = [
        'user_id', 'amount', 'currency', 'product', 'product_id', 'email', 'pay_processor', 'orderReference', 'authCode', 'phone', 'cardPan',
    ];

    public $timestamps = true;

    /**
     * Checks Is new payment of current User exists
     * and this User allowed to create resource
     *
     * @param string $product_slug product_slug
     *
     * @return model object
     */
    public function checkIsNewPaid($product_slug)
    {
        return $this::where('user_id', Auth::user()->id)
            ->where('product', $product_slug)
            ->whereNull('product_id')
            ->first();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
