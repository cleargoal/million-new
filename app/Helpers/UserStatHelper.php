<?php

namespace App\Helpers;

use App\UserStat;
use Carbon\Carbon;

class UserStatHelper
{

    /**
     * Увеличиваем счетчик: получаем тип ресурса -
     * (объява, визитка, букмарк, линк),
     * находим владельца ресурса, счетчик увеличиваем на единицу
     *
     * @param $resType
     * @param $srcId
     */
    public function increment($resType, $srcId)
    {
        // передавать ИД ресурса и находить user_id
        $getUserId = $this->getUserId($resType, $srcId);
        $stat = UserStat::updateOrCreate(
            [
                'user_id' => $getUserId,
                'record_id' => $srcId,
                'record_type' => $resType
            ]
        );

        if (isset($stat->daily_update)) {
            $date = Carbon::now()->diffInHours($stat->daily_update);
            if ($date >= 24) {
                $stat->views_today = 0;
                $stat->daily_update = Carbon::now();
            }
        }
        $stat->views += 1;
        $stat->views_today += 1;
        $stat->save();
    }

    /**
     * @param $resType
     * @param $srcId
     * @return mixed
     */
    protected function getUserId($resType, $srcId)
    {
        $getModel = $this->getModel($resType);
        $model = "\App\\" . $getModel;
        return $model::select('user_id')
            ->where('id', $srcId)->first()->user_id;
    }

    /**
     * @param $resType
     * @return string
     */
    protected function getModel($resType)
    {
        $models = [
            'ad' => 'Userad',
            'vis' => 'Vicard',
            'bookmark' => 'Userad',
            'link' => 'Userad',
        ];
        return $models[$resType];
    }
}
