<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

/**
 * Class LogHelper - helps logging
 *
 * @category Helper
 * @package  Helpers
 * @author   Yef <yef@mail.net>
 * @license  MIT http://license.org
 * @link     http://license.org
 */
class LogHelper
{
    /**
     * Call this method in PHP code
     *
     * @param $user_id
     * @param $src_id
     */

    public function save_ad_views($user_id, $src_id)
    {
        DB::table('logs')->insert(['user_id' => $user_id, 'src_id' => $src_id, 'log_slug' => 'vad']);
    }
}
