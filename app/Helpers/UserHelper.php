<?php

namespace App\Helpers;

use App\Mail\MailSender;
use App\User;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

/**
 * Class UserHelper - creates slugs
 *
 * @category Helper
 * @package  Helper
 * @author   Yef <yef@mail.net>
 * @license  MIT http://license.org
 * @link     http://license.org
 */
class UserHelper
{
    private $words = null;

    /**
     * SLUG creation START
     */
    private function getAllWords()
    {
        if ($this->words === null) {
            $this->words = config('slug_words');
        }
        return $this->words;
    }

    /**
     * Function generates random slug
     *
     * @param string $tableName name of table
     * @param int $readMax = 5 with      default
     * @param int $readMin = 1 word default
     * @param string $field field name
     *
     * @return string
     */
    protected function generateNewSlug(string $tableName, $readMax = 5, $readMin = 1, $field = '')
    {
//        $words = $this->getAllWords();
        $words = config('slug_words');
        $n = $readMax == $readMin ? $readMax : rand($readMin, $readMax); // amount selected words

        do {
            // array_flip используется потому что array_rand
            // выбирает КЛЮЧИ массива в качестве своего результата, а не его значения
            $slug = implode('-', array_rand(array_flip($words), $n));
            if ($tableName == 'user_details') {
                $chs = $this->checkUserSlug($slug);
            } else {
                $chs = $this->checkAnySlug($tableName, $field, $slug);
            }
        } while ($chs);
        return $slug; // this slug is unique and valid
    }

    /**
     * Check if generated slug exist in DB
     *
     * @param string $slug - user's slug
     *
     * @return boolean
     */
    protected function checkUserSlug(string $slug)
    {
        $query = DB::table('user_details')->where([
            ['user_data_type', 'slug'],
            ['user_data_val', $slug],
        ]);
        return $query->exists();
    }

    /**
     * check slug in any table with 1 field
     *
     * @param $table
     * @param $field
     * @param $slug
     * @return bool
     */
    protected function checkAnySlug($table, $field, $slug)
    {
        return DB::table($table)->where($field, $slug)->exists();
    }

    /**
     * Create User Slug
     *
     * @param $user_id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createUserSlug($user_id)
    {
        // dd('create user slag method');
        $user_slug = $this->generateNewSlug('user_details', 5, 4);
        DB::table('user_details')
            ->insert([
                    'user_id' => $user_id,
                    'user_data_type' => 'slug',
                    'user_data_val' => $user_slug
                ]
            );

        return redirect(route('registered'));
    }

    /**
     * Get User's slug
     *
     * @param $user_id
     * @return string
     */
    public function getUserSlug($user_id)
    {
        return DB::table('user_details')
            ->where('user_id', $user_id)
            ->where('user_data_type', 'slug')
            ->value('user_data_val');
    }

    /** create Visit card Slug */
    public function createViSlug()
    {
        return $this->generateNewSlug('vicards', 3, 3, 'visi_slug');
    }

    /**
     * Called after user verified his email
     *
     * @param User $child user who just verified his email
     *
     * @return mixed
     */
    public function userVerified($child)
    {
        $invitor = User::findOrFail($child->invitor);
        if ($invitor) {
            $invitor->addChild($child);
        }
        return redirect(route('registered'));
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * when email send through Object creation
     *
     * @param $recipient
     * @param $mailParams
     * @return string
     */
    public function send($mailParams)
    {
        // TODO change for: get return messages from dictionary
        try {
            $sent = Mail::to($mailParams['recipient'])->send(new MailSender($mailParams));
            $result = "Письмо успешно отослано.";
        } catch (Exception $e) {
            $result = "Письмо не отослано. Ошибка: " . $e->getMessage();
        }
        return $result;
    }

}
