<?php


namespace App\Services;

use App\Helpers\UserHelper;
use App\Helpers\UserStatHelper;
use App\Logging;
use App\Media;
use App\Myconfig;
use App\Payment;
use App\Vicard;
use FilesystemIterator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use function Composer\Autoload\includeFile;

class VisitService
{
    protected $pay_model; // Payment Model
    protected $avatarsFolder; // full path to current folder for saving pictures
    protected $picsFolder; // full path to current folder for saving pictures
    protected $thumbFolder; // full path to current folder for saving picture thumbnails
    protected $avatarsFolderNumber; // current number of numbered folder of avatars
    protected $picturesFolderNumber; // current number of numbered folder of pictures
    protected $basePicturesFolder; // base part of path to 'pictures' folder - above numbered folders
    protected $baseAvatarsFolder; // base part of path to 'avatars' folder - above numbered folders

    /**
     * Save pictures
     * @param Vicard $vicard visit card object
     * @param array $attachedPictures attached pictures filenames array
     * @return bool
     */
    public function savePicturesToDb(Vicard $vicard, array $attachedPictures): bool
    {
        $picturesCollection = collect($attachedPictures)->map(
            function ($picture) {
                return new Media(['slug' => 'pict', 'value' => $picture]);
            }
        );
        $vicard->medias()->saveMany($picturesCollection);

        return true;
    }

    /**
     * Return all media records for single visit
     *
     * @param Vicard $visCard
     * @return mixed
     */
    public function getMediaFromDb(Vicard $visCard)
    {
        return $visCard->medias;
    }

    /**
     * Get Visit To Array
     *
     * @param $visCard
     * @return array
     */
    public function getVisitToArray($visCard)
    {
        $viewCard = Vicard::where('visi_slug', $visCard)->first();
        if (isset($viewCard->videos)) {
            $videoArray = $this->arrExplode($viewCard->videos, 3);
        } else {
            $videoArray = "";
        } // чтобы не менять строку compact('

        $visCardMedia = $viewCard->medias;
        $pictures = $visCardMedia->where('slug', 'pict')->take(5)->pluck('value');

        if (isset($viewCard->links)) {
            $links = $this->arrExplode($viewCard->links);
        } else {
            $links = "";
        } // чтобы не менять строку compact('

        $explodeStyle = explode(',', $viewCard->styles);
        $styleBack = $explodeStyle[0];
        $styleFont = $explodeStyle[1];

        return compact('videoArray', 'pictures', 'links', 'styleBack', 'styleFont', 'viewCard');
    }

    /**
     * Explode string to array of videos, pictures, etc. and slice it for set amount
     *
     * @param $to_slice
     * @param $stringToExplode
     * @return mixed
     */
    protected function arrExplode($stringToExplode, $to_slice = null)
    {
        return !empty($stringToExplode) ? collect(
            array_diff(array_slice(explode(',', $stringToExplode), 0, $to_slice), ['', null, false])
        ) : "";
    }

    /**
     * Gets and returns all visit page data
     *
     * @param $visCard
     * @param $slugs
     * @param $mode
     * @return array
     */
    public function getVisitPageView($visCard, $slugs, $mode)
    {
        $viewCard = $this->getVisitToArray($visCard);
        $visitCard = $viewCard['viewCard'];

        $page_prev = $page_next = $page_curr = '';
        if ($slugs != '') {
            $page_curr = $slugs->search($visCard); // current page collection key
            $page_prev = $page_curr > 0 ? $slugs->get($page_curr - 1) : false;
            $page_next = $page_curr < $slugs->count() - 1 ? $slugs->get($page_curr + 1) : false;
        }

        // open graph data setter
        if (count($viewCard['pictures']) !== 0) {
            $preparedPic = str_replace('\\', '/', 'pictures/' . str_replace('fold', 'thum', $viewCard['pictures'][0]));
            $ogImage = $preparedPic;
        } else {
            $ogImage = (!empty($visitCard->avatar) ? 'avatars/' . $visitCard->avatar : 'og-header.jpg');
        }
        $ogData = $this->ogSetter($visitCard->header, $visCard, $ogImage, $visitCard->description);

        /** save visit 'view' to DB */
        $this->saveLog($visitCard->id);

        // Инкрементим просмотр
        $stat = new UserStatHelper;
        $stat->increment('vis', $visitCard->id);

        $viewCard['mode'] = $mode;
        $viewCard['visits_intended_page'] = session('visits_last_page', 1);
        $viewCard['page_prev'] = $page_prev;
        $viewCard['page_next'] = $page_next;
        $viewCard['ogData'] = $ogData;
        $viewCard['vicard'] = $visCard;

        return $viewCard;
    }

    /** save 'view' to DB */
    private function saveLog($vicard)
    {
        $newLog = new Logging();
        $newLog->user_id = Auth::user() ? Auth::user()->id : null;
        $newLog->src_id = $vicard;
        $newLog->log_slug = Logging::LOGGING_VIEW_VISIT_PAGE;
        $newLog->save();
    }

    /**
     * fills open graph variables for Vicard page
     *
     * @param $ogTitle
     * @param $ogUrl
     * @param $ogImage
     * @param $ogDescription
     * @return array
     */
    private function ogSetter($ogTitle, $ogUrl, $ogImage, $ogDescription)
    {
        $strWidth = mb_strwidth($ogDescription) < 250 ? mb_strwidth($ogDescription) : 250;
        $og_description = mb_strimwidth($ogDescription, 0, $strWidth, '...');
        return [
            'og_title' => $ogTitle,
            'og_url' => 'visiting-page/' . $ogUrl,
            'og_image' => asset('storage/' . $ogImage),
            'og_description' => $og_description,
        ];
    }

    public function saveVisitCardToDb($visitFormData)
    {
        $visitSaveData = $visitFormData; // get unchangeable fields like contacts
        if (isset($visitSaveData['avatar']) && $visitSaveData['avatar'] !== null) {
            $visitSaveData['avatar'] = $this->saveAvatar($visitFormData['avatar']);
        }

        if (isset($visitFormData['pictures'])) {
            $picsArray = $this->savePictures($visitFormData['pictures']);
            unset($visitSaveData['pictures']);
//            $visitFormData['pictures'] = implode(',', $picsArray);
        }

        $visitSaveData['videos'] = $this->replaceVideoLinks($visitFormData['videos']);

        $uh = new UserHelper;
        $visitSaveData['visi_slug'] = $uh->createViSlug();

        $visitSaveData['user_slug'] = DB::table('user_details')->where('user_id', $visitFormData['user_id'])->where(
            'user_data_type',
            'slug'
        )->value('user_data_val');
        $visitSaveData['styles'] = $visitFormData['style-back'] . ',' . $visitFormData['style-font'];
        $visitSaveData['actual'] = 1;

        $vicard = Vicard::create($visitSaveData);
        if (isset($picsArray)) {
            (new \App\Services\VisitService())->savePicturesToDb($vicard, $picsArray);
        }

        $this->pay_model = new Payment;
        $this->pay_model = $this->pay_model->checkIsNewPaid('vis');
        $this->pay_model->product_id = $vicard->id;
        $this->pay_model->save();
        return $vicard->visi_slug;
    }

    /**
     * Replace part of video links on 'embed/' instead of'watch?v=', and also remove Play list if  exist
     *
     * @param $videoLinks
     * @return string
     */
    protected function replaceVideoLinks($videoLinks)
    {
        return collect($videoLinks)->map(
            function ($item) {
                $from = ['watch?v=', '&feature=youtu.be'];
                $to = ['embed/', ''];
                $readyLink = str_replace($from, $to, $item);
                if (Str::contains($readyLink, 'list')) {
                    $shortLine = substr($readyLink, 0, stripos($readyLink, '&list'));
                    $readyLink = $shortLine;
                }
                return $readyLink;
            }
        )->implode(',');
    }

    /**
     * Save file of avatar into file system and return saved name
     *
     * @param $avatar
     * @return string
     */
    protected function saveAvatar($avatar)
    {
        if ($this->checkAvatarsFolder() === false) {
            $this->createAvatarsFolder();
        }

        $avaHash = $avatar->hashName(); // it's need because Image not converts file name
        $avaImg = Image::make($avatar)->fit(400);
        $avaPath = storage_path(config('image.avatars') . $this->avatarsFolderNumber . DIRECTORY_SEPARATOR . $avaImg);

        return $this->avatarsFolderNumber . DIRECTORY_SEPARATOR . $avaImg->save($avaPath . $avaHash)->basename;
    }

    /**
     * Saving pictures to folder
     *
     * @param
     * @return array
     */
    protected function savePictures($pictures): array
    {
        if ($this->checkPicturesFolder() === false) {
            $this->createPicturesFolder();
        }

        $picHashes = [];

        foreach ($pictures as $ky => $pic) {
            $pic->store($this->picsFolder);
            $picHashes[$ky] = $pic->hashName();
            $thumImg = $this->resizePicture($pic);
            $thumImg->save(config('image.dir_prefix') . $this->thumbFolder . DIRECTORY_SEPARATOR . $picHashes[$ky]);
            $picHashes[$ky] = $this->picturesFolderNumber . config('image.fold') . $picHashes[$ky]; // numbered path for saving to DB
        }
        return $picHashes;
    }

    /**
     * Resize picture to make thumbnail
     *
     * @param
     * @return \Intervention\Image\Image
     */
    protected function resizePicture($picture): \Intervention\Image\Image
    {
        $pic = Image::make($picture);
        $width = $pic->width();
        $height = $pic->height();
        $prop = 1.5;
        if ($width <= $height || $width / $height > $prop) {
            $pic->heighten(200);
        } else {
            $pic->widen(300);
        }
        $pic->resizeCanvas(300, 200, 'center', false, 'aaa')->trim();
        return $pic;
    }

    // checks how much number files in folder? Is current pictures folder OK for saving data
    protected function checkPicturesFolder()
    {
        $this->getPicturesCurrentFolder();

        if (File::exists(config('image.dir_prefix') . $this->basePicturesFolder) === false) {
            $this->createPicturesFolder();
        }
        // current path is ../million-ru/public/
        $fi = new FilesystemIterator(config('image.dir_prefix') . $this->picsFolder, FilesystemIterator::SKIP_DOTS);
        $count_files = iterator_count($fi);
        return $count_files < config('image.fif');
    }

    // checks how much number files in folder? Is current pictures folder OK for saving data
    protected function checkAvatarsFolder()
    {
        $this->getAvatarsCurrentFolder();

        if (File::exists(config('image.dir_prefix') . $this->baseAvatarsFolder) === false) {
            $this->createAvatarsFolder();
        }

        // current path is ../million-ru/public/
        $fi = new FilesystemIterator(config('image.dir_prefix') . $this->avatarsFolder, FilesystemIterator::SKIP_DOTS);
        $count_files = iterator_count($fi);
        return $count_files < config('image.fif');
    }

    protected function getPicturesCurrentFolder()
    {
        $this->picturesFolderNumber = (new Myconfig())->getMaxNumberValue(Myconfig::PICTURES_FOLDER_NUMBER);
        $this->basePicturesFolder = config('image.pictures');
        $this->picsFolder = $this->basePicturesFolder . $this->picturesFolderNumber . DIRECTORY_SEPARATOR . 'pics';
        $this->thumbFolder = $this->basePicturesFolder . $this->picturesFolderNumber . DIRECTORY_SEPARATOR . 'thum';
    }

    protected function getAvatarsCurrentFolder()
    {
        $this->avatarsFolderNumber = (new Myconfig())->getMaxNumberValue(Myconfig::AVATARS_FOLDER_NUMBER);
        $this->baseAvatarsFolder = config('image.avatars_folder');
        $this->avatarsFolder = $this->baseAvatarsFolder . $this->avatarsFolderNumber;
    }

    protected function createPicturesFolder()
    {
        (new Myconfig())->setIncrementNumberValue(Myconfig::PICTURES_FOLDER_NUMBER);
        $this->getPicturesCurrentFolder();
        if (!is_dir($this->basePicturesFolder . $this->picturesFolderNumber)) {
            Storage::makeDirectory($this->basePicturesFolder . $this->picturesFolderNumber);
        }
        Storage::makeDirectory($this->picsFolder); // make pictures folder
        Storage::makeDirectory($this->thumbFolder); // make thumbnails folder
    }

    protected function createAvatarsFolder()
    {
        (new Myconfig())->setIncrementNumberValue(Myconfig::AVATARS_FOLDER_NUMBER);
        $this->getAvatarsCurrentFolder();
        if (is_dir($this->baseAvatarsFolder . $this->avatarsFolderNumber) === false) {
            Storage::makeDirectory($this->baseAvatarsFolder . $this->avatarsFolderNumber);
        }
    }


    public function viewVisits($querystr)
    {
        $mode = 'v';
        $type = '';
        $paginate = 10;
        if (!$querystr) {
            $type = 'v';
            $results = Vicard::paginate($paginate);
        } else {
            $type = 'q';
            $results = $querystr->paginate($paginate);
        }
        session(['listyp' => $type]);
        $currPage = $results->currentPage();
        session(['visits_last_page' => $currPage]);
        $page_slugs = $results->pluck('visi_slug');
        session(['pagescolle' => $page_slugs]);

        return ['pageslist' => $results, 'mode' => $mode, 'current' => $currPage];
    }

    public function updateVisitPage($request)
    {
        $upData = $request->all();
        $vicard = Vicard::where('visi_slug', $upData['visi_slug'])->first();
        $upData['user_id'] = Auth::user()->id;
        $upData['videos'] = $this->replaceVideoLinks($upData['videos']);
        $upData['styles'] = $upData['style-back'] . ',' . $upData['style-font'];
        $upData['styles'] = $upData['style-back'] . ',' . $upData['style-font'];
        unset($upData['style-back']);
        unset($upData['style-font']);
        unset($upData['_token']);

        if ($request->hasFile('avatar')) {
            $upData['avatar'] = $this->saveAvatar($upData['avatar']);
        }

        if (!empty($upData['pictures'])) {
            $media = Media::where('vicard_id', $vicard->id)->first();
            $picturesPath = $this->savePictures($upData['pictures']);

            if (!empty($media)) {
                (new \App\Services\VisitService())->updatePucturesInDB($vicard, $picturesPath, $upData['returned_pics'], $upData['pictures']);
                unset($upData['pictures']);
            } else {
                (new \App\Services\VisitService())->savePicturesToDb($vicard, $picturesPath);
                unset($upData['pictures']);
            }
        }

        $vicard->update($upData);

        return ['vicard' => $upData['visi_slug']];
    }

    public function updatePucturesInDB(Vicard $vicard, $pics, $returned_pics, $oldPictures)
    {
        $this->checkPicturesFolder();

        $toDelete = array_intersect_key($returned_pics, $oldPictures);

        foreach ($toDelete as $key => $value) {
            if ($returned_pics[$key] !== null) {
                $picsPath = str_replace('thum', 'pics', $returned_pics[$key]);

                preg_match_all('/\/pictures\/(\d+\/\w+\/\w+.[a-z]*)/', $returned_pics[$key], $matches);
                $deleteFromDB = str_replace('thum', 'fold', $matches[1][0]);

                preg_match_all('/\/pictures\/(\d+\/\w+\/\w+.[a-z]*)/', $picsPath, $matches);
                Storage::disk('public')->delete($matches[0][0]);

                preg_match_all('/\/pictures\/(\d+\/\w+\/\w+.[a-z]*)/', $value, $matches);
                Storage::disk('public')->delete($matches[0][0]);

                $vicard->medias()->where('value', $deleteFromDB)->forceDelete();
            }
        }

        $this->savePicturesToDb($vicard, $pics);
    }
}
