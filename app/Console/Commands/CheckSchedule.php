<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CheckSchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command checks - how to run schedule';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'command "check:schedule" handle' . PHP_EOL;
        file_put_contents('check_schedule.txt', date('d-M-Y H:i:s') . PHP_EOL, FILE_APPEND);
    }
}
