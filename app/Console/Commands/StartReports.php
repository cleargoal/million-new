<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ReportsController;

class StartReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'start:reports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command starts all daily reports';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        file_put_contents('check_schedule.txt', date('d-M-Y H:i:s') . ' // handle() - before reports' . PHP_EOL, FILE_APPEND);
        $reportNotPaid = new ReportsController();
        $reportNotPaid->getNotPaidMembersList();
        file_put_contents('check_schedule.txt', date('d-M-Y H:i:s') . ' // handle() - after reports' . PHP_EOL, FILE_APPEND);
        return;
    }
}
