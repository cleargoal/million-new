<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biblequote extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
	protected $fillable = [
			'biblequote', 'biblebook', 'biblebookln', 'bblbooktop', 'bblbooktopln', 'bibleverse', 'bibleverseln',
	];
	public $timestamps = false;
}
