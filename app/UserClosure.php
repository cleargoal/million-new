<?php
namespace App;

use Franzose\ClosureTable\Contracts\ClosureTableInterface;
use Franzose\ClosureTable\Models\ClosureTable;

class UserClosure extends ClosureTable {
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_closure';
}
