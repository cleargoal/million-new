<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Supp_faq extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;

	protected $fillable = [
			'user_id', 'qu_header', 'question', 'screenshot_q', 'answer', 'screenshot_a', 'useful', 'views',
	];

	public $timestamps = true;

    /**
     * Получить пользователя данного вопроса
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

	public function getScreenshotQAttribute($value)
	{
	  	return empty($value) ? $value : Storage::url($value);
	}

	public function getScreenshotAAttribute($value)
	{
	  	return empty($value) ? $value : Storage::url($value);
	}

}
