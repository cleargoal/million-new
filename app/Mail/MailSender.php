<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailSender extends Mailable
{
    use Queueable, SerializesModels;

    /** @var $mailTemplate - the key in config.mailtemplate;
     * @var $mailData - data from config.mailtemplate array
     * @var $parameters - parameters
     */
    protected $mailTemplate;
    protected $mailData;
    protected $parameters;

    /** @return void
     * @var $parameters - parameters
     */
    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    /** * Build the message.
     * @return $this
     */
    public function build(): MailSender
    {
        $this->setMailData();
        $subject = $this->mailData['title'];
        return $this->view('emails.mail_template')->subject($subject)->with('maildata', $this->mailData);
    }

    protected function setMailData()
    {
        $this->mailTemplate = $this->parameters['mailtemplate'] ?: '';
        $this->mailData = config('mailtemplate.' . $this->mailTemplate);
        // filling e-Mail body empty settings
        if ($this->mailTemplate === 'user_registered') {
            $this->mailData['text2'] .= $this->parameters['password'];
        } elseif ($this->mailTemplate === 'report') {
            $today_regs = isset($this->parameters['today_regs']) ? $this->parameters['today_regs'] : 0;
            $total_regs = isset($this->parameters['total_regs']) ?: 0;
            $this->mailData['text2'] = $today_regs . ' людей';
            $this->mailData['text4'] = $total_regs;
        } elseif ($this->mailTemplate === 'support') {
            $this->mailData['text1'] .= $this->parameters['question'];
            $this->mailData['text2'] .= $this->parameters['answer'];
        }
    }
}
