<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logtype extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;

    protected $fillable = [
			'type_descr', 'type_slug',
	];
}
