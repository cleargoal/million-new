<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Myconfig extends Model
{
    protected $fillable = ['parameter', 'str_value', 'int_value'];
    public $timestamps = true;
    const PICTURES_FOLDER_NUMBER = 'pictures_folder_number';
    const AVATARS_FOLDER_NUMBER = 'avatars_folder_number';

    public function getMaxNumberValue($parameter)
    {
        return self::select('int_value')->where('parameter', $parameter)->max('int_value');
    }

    public function setIncrementNumberValue($parameter, $descriptor = null)
    {
        \DB::table('myconfigs')
            ->where('parameter', $parameter)
            ->update(['int_value' => self::getMaxNumberValue($parameter) +1]);
    }

}
