<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payservice extends Model
{
  /** * The attributes that are mass assignable.
   * @var array */
  protected $fillable = [
    'payslug', 'name', 'descript', 'cost', 'purse',
  ];

}
