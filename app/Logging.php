<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Logging extends Model
{
    const LOGGING_VIEW_VISIT_PAGE = 'vvp';

    protected $table = 'logs';
    protected $fillable = [
        'user_id', 'log_slug', 'src_id',
    ];

	public $timestamps = true;

	public function user(): BelongsTo
    {
		return $this->belongsTo('App\User');
	}

	public function userad(): BelongsTo
    {
		return $this->belongsTo('App\Userad', 'src_id');
	}

    /** count number of current user's ad views
     * @param $srcId
     * @return mixed
     */
	public function countViewsNumber($srcId) {
	    return $this->select('src_id')->where('log_slug', 'vad')->where('src_id', $srcId)->count();
    }

}
