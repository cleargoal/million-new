<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    public $timestamps = true;
    protected $table = 'medias';

    // slug variants: 'cont' - contact, 'link' - URL, 'vide' - video link, 'pict' - picture name/path
    protected $fillable = ['vicard_id', 'slug', 'value'];

    public function vicard()
    {
        return $this->belongsTo('App\Vicard');
    }

}
