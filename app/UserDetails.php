<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
}
