<?php

namespace App\Http\Controllers;

use App\Helpers\UserHelper;
use Illuminate\Http\Request;

class CheckSendMailController extends Controller
{
    protected $sendata;

    /**
     * Method for just check email sender
     *
     * @param \Illuminate\Http\Request $request
     * @return string
     */
    public function checkSend(Request $request)
    {
        $parameters['recipient'] = $request->email;
        $parameters['subject'] = $request->subject;
        $parameters['message'] = $request->message;
        $parameters['name'] = $request->name;
        $parameters['mailtemplate'] = 'report';

        return (new UserHelper())->send($parameters);
    }

}
