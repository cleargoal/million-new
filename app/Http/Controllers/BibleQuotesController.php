<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Biblequote;

class BibleQuotesController extends Controller
{
	public function getBbq()
	{
		//$bq_id = $this->getNewBbqId();
		//$quote = Biblequote::find($bq_id);
		$quote = $this->testGetBbqId();
		$link = config('settings.bible_url') . $quote['biblebookln'] . '/' . $quote['bblbooktopln'] . '/' . $quote['bibleverseln'];
		$from_replace = array(' ', '-');
		$to_replace = array('&nbsp;', '&#8209;');
		$anchor = str_replace($from_replace, $to_replace, $quote['biblebook'] . ' ' . $quote['bblbooktop'] . ':' . $quote['bibleverse']); // &#8209;
		$txtsubstlen = 144;
		$text = mb_strimwidth($quote['biblequote'], 0, $txtsubstlen, "...");
		$data = array(['link' => $link, 'anchor' => $anchor, 'text' => $text]);
		return $data;
	}

	/**
	 * Выборка цитаты из базы по ID, хранящемуся в куках
	 *
	 * @return Biblequote
	 */
	protected function testGetBbqId(): Biblequote
    {
		// Если записан ID в Cookie
		if (request()->cookie('bbqid')) {
			$lastBbqId = request()->cookie('bbqid');
			$nextQuote = Biblequote::where('id', '>=', $lastBbqId)
				->orderBy('id')
				->take(2)->get()->last();
		} else {
            $lastBbqId = 1;
            $nextQuote = Biblequote::orderBy('id')->first();
		}

		// when reached full quotes amount **!!!! attention: if() обязательно должно быть не строгое, т.к. из БД извлекаем число, а из кука приходит строка
		if ($nextQuote->id == $lastBbqId) {
			$nextQuote = Biblequote::orderBy('id')->first();
		}
		cookie()->queue(cookie('bbqid', $nextQuote->id, '5270400'));
		return $nextQuote;
	}
}
