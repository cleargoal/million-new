<?php

namespace App\Http\Controllers;

use App\Helpers\UserHelper;
use App\Helpers\UserStatHelper;
use App\Mail\MailSender;
use App\User;
use App\Userad;
use App\UserStat;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Logging as LogModel;
use Illuminate\Support\Facades\Mail;

class AdviewController extends Controller
{
    public $viewads = '';

    public function getAds()
    {
        return view('adview');
    }

    /**
     *  See one ad - just saved - own of user
     *
     * @param users-ad $usersad ID of saved ad
     *
     * @return View
     */
    public function savedAd($usersad)
    {
        $ad = Userad::find($usersad);
        $ads = collect([$ad]);
        $mode = 's'; // saved
        return view('adview', compact('ads', 'mode', 'usersad'));
    }

    // see root (owner's) Ads - view when click "смотреть объявления"
    public function rootAds()
    {
        $take = config('logic.adsAmount');
        if (Auth::guest()) {
            $ads = Userad::take($take)->where('actual', true)->orderBy('user_id')->get();
            $guest = true;
        } else {
            $user_bookmarks = Auth::user()->bookmarks->pluck('id');
            $ads = Userad::whereNotIn('id', $user_bookmarks)->where('actual', true)->orderBy('user_id')->take($take)->get();
            $guest = false;
        }
        $mode = 'r'; // root
        return view('rootads', compact('ads', 'mode'))->with('guest', $guest);
    }

    /**
     * See upline's Users 20 Ads, BASE feature,
     * receives array of IDs of upline's ads to view
     *
     * @param Request $request Request
     *
     * @return View|Application|Factory|\Illuminate\View\View
     */
    public function viewAds(Request $request)
    {
        $adsToView = collect(explode(',', $request->adstoview));
        $ads = Userad::whereIn('id', $adsToView)->where('actual', true)->get();
        $viewAmount = $adsToView->count();
        $mode = 'b'; // base
        return view('adview', compact('ads', 'mode', 'viewAmount'));
    }

    /**
     * See Users 20 Ads when click 'просмотреть еще...'
     *
     */
    public function moreAds()
    {
        $mode = 'm'; // more
        return view('adview', ['mode' => $mode]);
    }

    /** saves to Logging marks about ads viewed by this user or anonimous */
    public function saveViewedAd(Request $request)
    {
        $datas = $request->all();
        $data['user_id'] = Auth::user() ? Auth::user()->id : null;
        $data['src_id'] = $datas['ads_id'];
        $data['log_slug'] = 'vad';
        LogModel::create($data);
        $stat = new UserStatHelper;
        $stat->increment('ad', $data['src_id']);
        $this->checkViewsNumber($data['src_id']);
    }

    // count number of current user's ad views & and deactivate his ad when amount is full
    protected function checkViewsNumber($dataSrcId)
    {
        $adViewCount = UserStat::where('record_id', $dataSrcId)->value('views');

        if ($adViewCount >= config('settings.adViewLimit')) {
            Userad::where('id', $dataSrcId)->update(['actual' => false]); // turn off show ad
            $this->sendFinishMessageToOwner($dataSrcId);
        }
    }

    /** prepare message about Stop show ads - for send to ad owner */
    protected function sendFinishMessageToOwner($dataSrcId)
    {
        $adOwnerId = Userad::where('id', $dataSrcId)->value('user_id'); // get User for send him email about his ad show finish
        $user = User::where('id', $adOwnerId)->get();

        $parameters['recipient'] = $user;
        $parameters['mailtemplate'] = 'adLimit';
        Mail::to($parameters['recipient'])->send(new MailSender($parameters));
    }
}
