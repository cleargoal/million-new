<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Payment;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    protected $usersNotPaid, $usersToSend;
    
    public function getNotPaidMembersList()
    {
        $this->usersNotPaid = User::whereNotIn('id', DB::table('payments')->select('user_id')
                                    ->where('product', 'adv')
                                    ->groupBy('user_id')
                                    ->pluck('user_id')
                                )->get();
        $this->getDescendentsForEveryUser();
        $this->processQueryResult();
    }
    
    protected function getDescendentsForEveryUser()
    {
        $this->usersToSend = $this->usersNotPaid->filter(function ($item, $key) {
            $structure = $item->getDescendants();
            $structureCount = $structure->count(); // total subscribers
            if($structureCount > 0){
                $today = (new Carbon())->today();
                $subToday = $structure->where('created_at', '>', $today)->count(); // today subscribers
                if ($subToday > 0) { // if User has subscribers Today
                    $item->total_regs = $structureCount;
                    $item->today_regs = $subToday;
                    return $item;
                }
            }
        });
    }
    
    protected function processQueryResult()
    {
        $this->usersToSend->each(function ($item) {
            $parameters['recipient'] = $item->email;
            $parameters['today_regs'] = $item->today_regs;
            $parameters['total_regs'] = $item->total_regs;
            if($item->total_regs > 0) {
                $this->prepareSendEmail($parameters);
            }
        });
    }
    
    protected function prepareSendEmail($parameters)
    {
        $parameters['mailtemplate'] = 'report';
        $sendReport = new MailSendController($parameters);
        $sendReport->send();
    }

}
