<?php

namespace App\Http\Controllers;

use App\Vicard;
use App\Helpers\UserHelper;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;
use App\Payment;
use App\Myconfig;
use FilesystemIterator;
use App\Services\VisitService;

class VisitController extends Controller
{
    protected $pay_model; // Payment Model
    const VIEW_SAVED_PAGE = 'view.saved.page';

    /**
     * Method checks is the Payment for visit card done by user and shows creation page
     */
    public function createPage()
    {
        $this->pay_model = new Payment();
        if ($this->pay_model->checkIsNewPaid('vis') === null) {
            return view('/payment/need_payment', ['product' => 'страницу визитку']);
        }
        return view('visit_create', ['mode' => 'c']); // create mode - form
    }

    public function editPage($visCard)
    {
        $viewCard = (new \App\Services\VisitService())->getVisitToArray($visCard);

        $viewCard['mode'] = 'e';
        $viewCard['vicard'] = $visCard;

        foreach ($viewCard['pictures'] as $key => $value) {
            $viewCard['pictures'][$key] = str_replace('\\', '/', asset(config('image.view_pict') . str_replace('fold', 'thum', $viewCard['pictures'][$key])));
        }

        return view('visit_edit', $viewCard);
    }

    /**
     * saves Visit Page
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function saveVisitPage(Request $request)
    {
        $validation = \Validator::make($request->all(), ['pictures.*' => 'mimetypes:image/*']);
        if ($validation->fails()) {
            return Redirect::back()->withErrors($validation);
        } else {
            $visitFormData = $request->all();
            $visitFormData['user_id'] = Auth::user()->id;

            $visCardSlug = (new VisitService())->saveVisitCardToDb($visitFormData);
            return redirect()->route(self::VIEW_SAVED_PAGE, ['vicard' => $visCardSlug]);
        }
    }

    /** Shows single page
     *
     * @param Request $request
     * @param Vicard->slug $visCard it's visiting slug
     * @return Factory|View
     */
    public function viewSavedPage(Request $request, $visCard)
    {
        $slugs = session('pagescolle');
        $mode = '';
        if ($request->route()->named(self::VIEW_SAVED_PAGE)) {
            $mode = 's';
        }
        $viewCard = (new VisitService())->getVisitPageView($visCard, $slugs, $mode);
        return view('visiting-page', $viewCard);
    }

    /** shows List of pages BY paginate */
    public function viewVisits($querystr = null)
    {
        $viewVisitsData = (new VisitService())->viewVisits($querystr);
        return view('pageslist')->with($viewVisitsData);
    }

    /**  */
    public function usVisList($uslug)
    {
        session(['user_slug' => $uslug]);
        $querystr = DB::table('user_details')
            ->join('vicards', 'user_details.user_data_val', '=', 'vicards.user_slug')
            ->where('user_details.user_data_type', 'slug')->where('user_details.user_data_val', $uslug);
        return $this->viewVisits($querystr);
    }

    public function updateVisitPage(Request $request)
    {
        $updateVisitsData = (new VisitService())->updateVisitPage($request);
        return redirect()->route(self::VIEW_SAVED_PAGE, $updateVisitsData);
    }
}
