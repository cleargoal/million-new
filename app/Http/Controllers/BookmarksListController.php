<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Logging;
use App\Userad;
use Illuminate\Support\Facades\Auth;
use App\Helpers\UserStatHelper;
use Str;

class BookmarksListController extends Controller
{
    // shows user's bookmarks made in 'adview' view
    public function show()
    {
        $ids = request()->bookmark_ids ?? 0;
        $bookmark_ids = explode(',', $ids);
        $listbookmarks = Userad::whereIn('id', $bookmark_ids)->get();
        $showBtnCreate = Str::contains(parse_url(url()->previous())['path'], 'viewads');
        return view('bookmarkslist', compact('listbookmarks'))->with('showBtnCreate', $showBtnCreate);
    }

    /**
     * Saves marked user's bookmarks
     *
     * @param request $request Request
     *
     * @return void
     */
    public function saveBookmark(Request $request)
    {
        $bookmarkIds = $request->all();
        if (!empty($bookmarkIds)) {
            if (Auth::guest()) {
                $now = Carbon::now()->toDateTimeString();
                $dataArray = collect(
                    $bookmarkIds
                )->map(
                    function ($id) use ($now) {
                        return [
                            'log_slug' => 'bmk',
                            'src_id' => $id,
                            'created_at' => $now,
                        ];
                    }
                )->toArray();
                Logging::insert($dataArray); // сохраняет массив данных без цикла
            } else {
                Auth::user()->addToBookmarks($bookmarkIds);
            }
            $this->statBooks($bookmarkIds);
        }
    }

    /**
     * Logging the views of user's links
     *
     * @param Request $request Request
     *
     * @return void
     */
    public function logLink(Request $request)
    {
        \dump($request);
        $ad_id = $request->adv_id;
        if (Auth::guest()) {
            $data = ['log_slug' => 'lnk', 'src_id' => $ad_id,];
        } else {
            $data = ['log_slug' => 'lnk', 'src_id' => $ad_id,
                'user_id' => Auth::user()->id];
        }
        Logging::create($data);

        // Инкремент на статистику
        $stat = new UserStatHelper;
        // передать имя поля, в которое писать и массив ИД ресурсов
        $stat->Increment('link', $ad_id);
    }

    /**
     * Logging the statistic of bookmarking of user's ads
     *
     * @param array $bookmarkIds
     */
    protected function statBooks(array $bookmarkIds)
    {
        foreach ($bookmarkIds as $id) {
            // Инкремент на статистику
            $stat = new UserStatHelper;
            // передать имя поля, в которое писать и массив ИД ресурсов
            $stat->Increment('bookmark', $id);
        }
    }
}
