<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PaymentRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PaymentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PaymentCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Payment::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/payment');
        CRUD::setEntityNameStrings('payment', 'payments');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumns([
            'id',]);

        $this->crud->addColumn([
            'label' => 'User',
            'type' => 'relationship',
            'name' => 'user_id',
        ]);

        $this->crud->addColumns([

            'email',
            'amount',
            'currency',
        ]);

        $this->crud->addColumn([
            'label' => 'Product Tag',
            'type' => 'text',
            'name' => 'product',
            'model' => 'App\Payment',
        ]);

        $this->crud->addColumn([
            'label' => 'Product ID',
            'type' => 'number',
            'name' => 'product_id',
        ]);

        $this->crud->addColumns([
            'pay_processor',
            'created_at'
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PaymentRequest::class);

        $this->crud->addField([
            'label' => 'User',
            'type' => 'relationship',
            'name' => 'user_id',
            'entity' => 'user'
        ]);

        $this->crud->addField([
            'label' => 'Currency',
            'type' => 'radio',
            'options' => [
                'UAH' => 'UAH',
                'RUB' => 'RUB'
            ],
            'name' => 'currency',
            'model' => 'App\Payment',
        ])->afterField('CardPan');

        $this->crud->addField([
            'label' => 'Product Tag',
            'type' => 'radio',
            'options' => [
                'adv' => 'Advert',
                'vis' => 'Vicard'
            ],
            'name' => 'product',
            'model' => 'App\Payment',
        ])->afterField('Currency');

        $this->crud->addField([
            'label' => 'Product ID',
            'type' => 'number',
            'name' => 'product_id',
        ]);

        $this->crud->addField([
            'label' => 'Pay processor',
            'type' => 'text',
            'name' => 'pay_processor',
            'default' => 'wfp'
        ]);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
