<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\MailSendController;
use App\Http\Requests\Supp_faqRequest;
use App\Mail\MailSender;
use App\Supp_faq;
use App\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

/**
 * Class Supp_faqCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class Supp_faqCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Supp_faq::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/supp_faq');
        CRUD::setEntityNameStrings('supp_faq', 'supp_faqs');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        $this->crud->addColumn([
            'label' => 'Question',
            'type' => 'text',
            'name' => 'question',
            'limit' => 50
        ]);

        $this->crud->addColumn([
            'label' => 'Answer',
            'type' => 'text',
            'name' => 'answer',
            'limit' => 50
        ]);

        $this->crud->addColumn([
            'label' => 'Screenshot Q',
            'type' => 'image',
            'name' => 'screenshot_q',
            'height' => '50px',
            'width' => '50px',
        ]);

        $this->crud->addColumn([
            'label' => 'Screenshot A',
            'type' => 'image',
            'name' => 'screenshot_a',
            'height' => '50px',
            'width' => '50px',
        ]);

        CRUD::setFromDb(); // columns

        $this->crud->removeButton('show');


        $this->crud->addButtonFromView('line', 'Ответить', 'answer', 'beginning');


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(Supp_faqRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function answer($id)
    {
        $faq = Supp_faq::find($id);
        return view('vendor.backpack.base.inc.answer', compact('faq'));
    }

    public function sendAnswer(Request $request)
    {
        $this->sendata = Supp_faq::findOrFail($request->faq_id);
        $this->sendata->answer = $request->answer;
        if ($request->hasFile('screenshot_a')) {
            $image = $request->file('screenshot_a');
            $name = Str::random(16) . '.' . $image->getClientOriginalExtension();
            $path = $image->storeAs('public/questions', $name);
            if ($path) {
                $this->sendata->screenshot_a = $path;
            }
        }
        $this->sendata->save();
        //отправка письма

        $user = User::where('id', $this->sendata->user_id)->get();


        $parameters['question'] = $this->sendata->question;
        $parameters['answer'] = $this->sendata->answer;
        $parameters['mailtemplate'] = 'support';
        $parameters['recipient'] = $user;
        Mail::to($parameters['recipient'])->send(new MailSender($parameters));

        return redirect(backpack_url('supp_faq'));

    }
}
