<?php

namespace App\Http\Controllers;

use App\Mail\MailSender;
use Illuminate\Http\Request;
use App\Payment;
use App\User;
use Illuminate\Support\Facades\Mail;

class PaymentController extends Controller
{
    protected $payment;
    /** variables: $mailtemp: 'adpaid', 'visitpaid'; $product - array: 'type' => 'adv' or 'vis', 'price' => 'amount', 'curr' => 'UAH' or 'RUB' */
    protected $user, $sendata, $mailtemp, $product, $method, $uri, $request_input, $returnJson, $json_parsed, $request_json, $php_input, $output;

    public function checkPayment (Request $request) {
        $this->output = 'Ответ сервера JSON in '.date('Y-M-d H:i:s').PHP_EOL;
        $this->php_input = file_get_contents('php://input'); // Json from server

        $this->json_parsed = $this->jsonValidate($this->php_input);
        $this->output .=  'Тип данных this->json_parsed: '.gettype($this->json_parsed).PHP_EOL;
        $this->output .=  'Value of this->json_parsed: '.PHP_EOL.print_r($this->json_parsed, true).PHP_EOL;

        /** saving to DB - start */
        if ($this->json_parsed->transactionStatus == 'Approved') {
            try{
                $this->paymentSaveToDB();
                $this->output .= 'DB insertion Success'.PHP_EOL;
            }catch(\Exception $e){
                $this->output .= 'DB insert ERROR'.PHP_EOL;
            }
        }
        /** DB saving end */

        $this->jsonCreate();
        $this->output .= PHP_EOL.$this->returnJson.PHP_EOL;
        file_put_contents('../storage/temp/wfp_purchase_response.txt', $this->output.PHP_EOL);
        echo $this->returnJson;
    }

    protected function jsonCreate(){
        $hash_time = time();
        $hash_ref = !empty($this->json_parsed->orderReference) ? $this->json_parsed->orderReference : 'N/a';
        $hash_string = $hash_ref .';accept;'.$hash_time;
        $json_array = [ "orderReference" => $hash_ref,
            "status" => "accept",
            "time" => $hash_time,
            "signature" => hash_hmac("md5", $hash_string, config('wayforpay.merchantSecretKey')),
        ];
        $this->returnJson = json_encode($json_array);
    }

    /** save to 'payment' table */
    protected function paymentSaveToDB () {
        $insert_data = [
            'user_id' => $this->getUserIdByEmail(),
            'amount' => floatval($this->json_parsed->amount),
            'currency' => $this->json_parsed->currency,
            'product' => '',
            'email' => $this->json_parsed->email,
            'pay_processor' => 'wfp',
            'orderReference' => $this->json_parsed->orderReference,
            'authCode' => $this->json_parsed->authCode,
            'phone' => $this->json_parsed->phone,
            'cardPan' => $this->json_parsed->cardPan,
        ];
        $this->output .= 'insert_data: '.PHP_EOL.print_r($insert_data, true).PHP_EOL;

        $paym = Payment::updateOrCreate(
            ['orderReference' => $this->json_parsed->orderReference],
            $insert_data
        );
    }

    protected function sendEmail($request) {
        $parameters['recipient'] = $request->email;
        switch ($request->product_slug) {
            case 'adv':
                $template = 'adpaid';
                break;
            case 'vis':
                $template = 'visitpaid';
                break;
        }
        $parameters['mailtemplate'] = $template;
        Mail::to($parameters['recipient'])->send(new MailSender($parameters));
    }

    protected function jsonValidate( $string) {
        // decode the JSON data
        $result = json_decode($string);

        // switch and check possible JSON errors
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = ''; // JSON is valid // No error has occurred
                break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded.';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
                break;
            // PHP >= 5.3.3
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded.';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given.';
                break;
            default:
                $error = 'Unknown JSON error occurred.';
                break;
        }

        if ($error !== '') {
            // throw the Exception or exit // or whatever :)
            return $error;
        }
        else {
            // everything is OK
            return $result;
        }
    }

    /** data for payment widget;   */
    /** ATTENTION! For start method makePayment not needed  */
    public function makePayment() {
        /*        $order_reference = Str::random(4) . '-' . Str::random(4) . '-' . Str::random(4) . '-' . Str::random(4);
                $send_pay_data = [
                    'merchantAccount' => config('wayforpay.merchantAccount'), //  *
                    'merchantDomainName' => "million-ru.segment.work", //   *
                    'merchantSignature' => "b95932786cbe243a76b014846b63fe92",
                    'orderReference' => $order_reference,
                    'orderDate' => time(),
                    'clientEmail' => Auth::user()->email,
                ];*/
//        return view('/payment/pay_for_ad', ['pay_data' => $send_pay_data]);
        return view('/payment/pay_for_ad');
    }

    protected function getUserIdByEmail() {
        return User::where('email', $this->json_parsed->email)->first()->id;
    }

    /** method:
     * receives redirect from WFP to 'returnUrl'
     * updates 'product' field
     */
    public function paidSuccess(Request $request, $product_slug) {
        $this->sendEmail($request);
        $product_name = $this->setProductName($product_slug);
        $paym = Payment::updateOrCreate(
            ['orderReference' => $request->orderReference],
            ['product' => $product_slug]
        );
//        $route = $product_slug == 'adv' ? 'create-ad' : $product_slug == 'vis' ? 'createvisit' : 'home';
        switch ($product_slug) {
            case 'adv':
                $route = 'create-ad';
                break;
            case 'vis':
                $route = 'createvisit';
                break;
            case '':
                $route = 'home';
                break;
            default:
                $route = 'home';
        }
        return view('/payment/paid_successfully', ['product' => $product_name, 'route' => $route] );
    }

    public function paidUnsuccess(Request $request)
    {
        $product_name = $this->setProductName($request->product_slug);

        return view('/payment/payment_declined', ['product' => $product_name]);
    }

    protected function setProductName($key) {
        $p_names = ['test' => 'тест', 'adv' => 'Объявление', 'vis' => 'Визитка'];
        return $p_names[$key];
    }
}
