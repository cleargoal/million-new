<?php

namespace App\Http\Controllers;

use App\Userad;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Helpers\UserHelper;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Logging;
use App\Payment;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class UseradController extends Controller
{

    protected $payModel, $logicAdsAmount, $rootAds;

    /**
     * Method checks conditions of availability to create ad
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View;
     */
    public function createAd()
    {
        $this->logicAdsAmount = config('logic.adsAmount');
        if (Auth::guest()) {
            return redirect()->route('login');
        }
        // check - is ad paid? IF 'no', user redirected to payment from this method
        if ($this->checkProductPayment() === null) {
            $product = 'объявление';
            return view('/payment/need_payment', ['product' => $product]);
        }

        // check - are uplines ads viewed & get viewed IDs list
        $uplinesAdsViewed = $this->checkUplineViewed();

        $viewedAncestorAdsIds = $uplinesAdsViewed[0]; // collection
        $viewedAncestorAdsIdsCount = $viewedAncestorAdsIds->count();

        $ancestorAdsIds = $uplinesAdsViewed[1]; // collection
//        $ancestorAdsIdsCount = $ancestorAdsIds->count();

        $this->rootAds = $this->rootAds(); // collection
        $rootAdsViewedIds = $this->getRootAdsViewedIds(); // collection
        // Calculated:
        // difference of - all ancestor Ads Ids with viewed
        // & all root Ads IDs with viewed
        // total limited to 20 minus ancestor Ads viewed
        $adsTotalToViewCollection
            = $ancestorAdsIds->diff($viewedAncestorAdsIds)
            ->concat($this->rootAds->diff($rootAdsViewedIds))
            ->slice(0, 20 - $viewedAncestorAdsIdsCount);

        //if uplinesAdsViewed[0]->count() + tootAds->count() - has amount enought: GO
        if ($adsTotalToViewCollection->count() === 0) {
            return view('createad', ['mode' => 'c']);
        } else {
            $adsTotalToView = $adsTotalToViewCollection->implode(",");
            return redirect()->route('viewAds', ['adstoview' => $adsTotalToView]);
        }
    }

    /**
     * Saves created ad
     *
     * @param request $request Request object
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function doCreateAd(Request $request)
    {
        $data = $request;
        $data['user_id'] = Auth::user()->id;
        $uh = new UserHelper;
        $data['user_slug'] = $uh->getUserSlug($data['user_id']); //
        $data['actual'] = 1;
        $usersad = Userad::create($data->all());

        // $this->payModel = new Payment;
        $this->payModel = (new Payment)->checkIsNewPaid('adv');
        if ($this->payModel !== null) {
            $this->payModel->product_id = $usersad->id;
            $this->payModel->save();
            return redirect()->route('adview', ['usersad' => $usersad->id]);
        } else {
            $product = 'объявление';
            return view('/payment/need_payment', ['product' => $product]);
        }
    }

    /**
     * Saves edited Ad data
     *
     * @param request $request Request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateAd(Request $request): RedirectResponse
    {
        $userad = Userad::find($request->id);
        $userad->fill($request->input())->save();
        return redirect()->route('adview', ['usersad' => $userad->id]);
    }

    /**
     *  Func checks - are Upline ads viewed by this User
     *
     * @return array
     */
    protected function checkUplineViewed(): array
    {
        $ancesIds = $this->getAncestorsIds();
        // all Ancestors Ads IDs
        $ancestAdsIds = Userad::select('id')
            ->whereIn('user_id', $ancesIds)
            ->where('actual', 1)
            ->pluck('id');
        // ancestor ads IDs viewed by User
        $ancestorAdsViewed = Logging::select('src_id')
            ->where('log_slug', 'vad')
            ->where('user_id', Auth::user()->id)
            ->whereIn('src_id', $ancestAdsIds)
            ->groupBy('src_id')
            ->orderBy('src_id', 'desc')
            ->pluck('src_id');
        return array($ancestorAdsViewed, $ancestAdsIds);
    }

    /**
     * Gets Root Ads Viewed Ids
     *
     * @return object
     */
    protected function getRootAdsViewedIds(): object
    {
        $rootAdsViewed = Logging::select('src_id')
            ->where('log_slug', 'vad')
            ->where('user_id', Auth::user()->id)
            ->whereIn('src_id', $this->rootAds)
            ->groupBy('src_id')
            ->orderBy('src_id', 'desc')
            ->pluck('src_id');
        return $rootAdsViewed;
    }

    /**
     * All Root ads
     *
     * @return Collection
     */
    protected function rootAds(): Collection
    {
        return Userad::select('id')->where('user_id', 2)->pluck('id');
    }

    /**
     * Returns Ancestors IDs list/array;
     *
     * @return Collection
     */
    protected function getAncestorsIds(): Collection
    {
        $theUser = Auth::user();
        return $theUser->getAncestors()
            ->pluck('id')
            ->take($this->logicAdsAmount);
    }

    /**
     * Starts form for editing ad by ID
     *
     * @param Userad $adId ID of ad that have to be changed
     *
     * @return \Illuminate\View\View
     */
    public function editAd($adId): View
    {
        $getAd = Userad::where('id', $adId)
            ->where('user_id', Auth::user()->id)->first();
        return view('createad')->with('editad', $getAd)->with('mode', 'e');
    }

    /**
     * Check is Product Payment made
     *
     * @return Model
     */
    protected function checkProductPayment(): ?Model
    {
        $payment = new Payment();
        return $payment->checkIsNewPaid('adv');
    }
}
