<?php

namespace App\Http\Controllers;

use App\Mail\MailSender;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Collection;

class SubscribeController extends Controller
{
    protected $resultString;
    protected $requestEmail;
    protected $apiKey;
    protected $uri;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function newSubscriber(Request $request)
    {
        $parameters['recipient'] = $request->email;
        $parameters['mailtemplate'] = 'subscribe';
        Mail::to($parameters['recipient'])->send(new MailSender($parameters));

        return response()->json('Вы подписались');
    }

    /** method for testing & debugging */
    protected function walkThrowResult($result)
    {
        if (is_array($result)) {
            foreach ($result as $key => $item) {
                if (is_array($item) || $item instanceof Collection) {
                    $this->walkThrowResult($item);
                } else {
                    $this->resultString .= $key . ' ' . $item . PHP_EOL;
                }
            }
        } elseif ($result instanceof Collection) {
            $result->each(
                function ($item, $key) {
                    if (is_array($item) || $item instanceof Collection) {
                        $this->walkThrowResult($item);
                    } else {
                        $this->resultString .= $key . ' ' . $item . PHP_EOL;
                    }
                }
            );
        } else {
            $result_ser = serialize($result);
            $this->resultString .= $result_ser . PHP_EOL;
            $result_code = $result->http_response_code;
            $this->resultString .= 'response_code: ' . $result_code . PHP_EOL;
        }
    }
}
