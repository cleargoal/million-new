<?php

namespace App\Http\Controllers;

use App\Logging;
use App\User;
use App\Vicard;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Userad;
use Illuminate\Support\Facades\DB;
use App\Payment;
use App\UserStat;
use Illuminate\View\View;

/**
 * Controller of user profile
 *
 * @category Profile
 */
class ProfileController extends Controller
{
    protected $user;
    protected $vicard;
    protected $userAd;
    protected $payment;
    protected $logging;

    public function boot(): parent
    {

    }

    /**
     * Главная страница со статистикой
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $this->user = $this->getUser();
        $this->vicard = Vicard::where('user_id', $this->user->id)->get();
        $this->userAd = Userad::where('user_id', $this->user->id)->get();
        $this->payment = Payment::where('user_id', $this->user->id)->get();
        $this->logging = Logging::where('user_id', '<>', $this->user->id)->orWhere('user_id', null)->get();
        //dd($this->user->stat);

        $ads = $this->ads();
        $visits = $this->vis();
        $payment = $this->pay();
        $bookmarks = $this->bookmarks();
        $viewed = $this->viewVis();
        $members = $this->invites();
        //эта строка должна быть последней - использует данные из предыдущих запросов
        $overview = $this->overView();
        return view('profile.index')->with(
            [
                'user' => $this->user,
                'ovewview' => $overview,
                'ads' => $ads,
                'visits' => $visits,
                'paym' => $payment,
                'lbls' => $bookmarks,
                'lbvs' => $viewed,
                'members' => $members,
            ]
        );
    }

    /**
     * Get User, his detailes and statistics and place it into class member
     *
     * @return User|Authenticatable|null
     */
    protected function getUser()
    {
        $user = Auth::user();
        $user->slug = DB::table('user_details')
            ->where('user_id', $user->id)->where('user_data_type', 'slug')
            ->value('user_data_val');
        $allStat = UserStat::where('user_id', $user->id)->get();

        $stat['ads_count'] = $allStat->where('record_type', 'ad')->sum('views');
        $stat['ads_count_day'] = $allStat->where('record_type', 'ad')->sum('views_today');
        $stat['vis_count'] = $allStat->where('record_type', 'vis')->sum('views');
        $stat['vis_count_day'] = $allStat->where('record_type', 'vis')->sum('views_today');
        $stat['bookmarks_count'] = $allStat->where('record_type', 'bookmark')->sum('views');
        $stat['bookmarks_count_day'] = $allStat->where('record_type', 'bookmark')->sum('views_today');
        $stat['links_count'] = $allStat->where('record_type', 'link')->sum('views');
        $stat['links_count_day'] = $allStat->where('record_type', 'link')->sum('views_today');
        $user->stat = $stat;

        return $user;
    }

    /**
     * @return \Illuminate\Support\Collection|\Tightenco\Collect\Support\Collection
     */
    protected function overView()
    {
        $ads_count = $this->userAd->count('id'); // объявления
        $vcs_count = $this->vicard->count('id'); // визитки
        // сумма платежей юзера
        $payments = $this->payment;
        $currency = $payments->first()->currency ?? '';
        $amount = $payments->sum('amount') ?? 0;
        $overview = collect(
            [
                'ads_count' => $ads_count, 'vcs_count' => $vcs_count,
                'payments' => $payments, 'currency' => $currency, 'amount' => $amount
            ]
        );
        return $overview;
    }

    /**
     * Просмотр объявлений
     *
     * @return \Illuminate\Support\Collection|\Tightenco\Collect\Support\Collection
     */
    protected function ads()
    {
        $ads = Userad::where('user_id', $this->user->id)->with('views:record_id,record_type,views')->get();

        foreach ($ads as $ad) {
            $ad->bookmarks = $ad->views->where('record_type', 'bookmark')->first();
            $ad->links = $ad->views->where('record_type', 'link')->first();
            $ad->adViews = $ad->views->where('record_type', 'ad')->first();
            unset($ad->views);
        }
        return $ads;
    }

    /**
     * Просмотр визиток
     *
     * @return \Illuminate\Support\Collection
     */
    public function vis()
    {
        $vcs = $this->vicard; // визитки
        // dd($vcs);
        $vids = $vcs->pluck('id'); // массив ай-ди визиток
        // Лог записи - просмотры визиток
        $lgv = $this->logging->where('log_slug', 'vvp')
            ->whereIn('src_id', $vids);
        $collection = collect(['vcs' => $vcs, 'lgv' => $lgv]);
        return $collection;
    }

    /**
     * Выводим платежи
     *
     * @return Collection
     */
    public function pay()
    {
        $paym = $this->payment;
        if ($paym->isEmpty()) {
            $newPayment = new Payment();
            $paym = new \Illuminate\Support\Collection();
            $paym->add($newPayment);
        }
        return $paym;
    }

    /**
     * Просмотр закладок - переходы юзером по ссылкам закладок
     *
     * @return \Illuminate\Support\Collection
     */
    public function bookmarks()
    {
        $viewedBooks = DB::table('logs')->select(DB::raw('count(*) as grp_ads, src_id, adtext, adlink'))
            ->where('logs.user_id', $this->user->id)
            ->where('logs.log_slug', 'lnk')
            ->join('userads', 'userads.id', 'logs.src_id')
            ->groupBy('logs.src_id', 'userads.adtext', 'userads.adlink')->get();
        return $viewedBooks;
    }

    /**
     * Просмотренные визитки - просмотры юзером других страниц
     *
     * @return \Illuminate\Support\Collection
     */
    public function viewVis()
    {
        $lbvs = DB::table('logs')->select(DB::raw('count(*) as grp_vs, src_id, header, price, visi_slug'))
            ->where('logs.user_id', $this->user->id)
            ->where('logs.log_slug', 'vvp')
            ->join('vicards', 'vicards.id', 'logs.src_id')
            ->groupBy('logs.src_id', 'vicards.header', 'vicards.price', 'vicards.visi_slug')->get();
        return $lbvs;
    }

    /**
     * Просмотр приглашенных - structure
     *
     * @return array
     */
    public function invites()
    {
        $members = [];
        $structure = $this->user->getDescendants();
        $members[] = $structure->count();
        $members[] = Payment::whereIn('user_id', $structure->pluck('descendant'))->count();
        $members[] = DB::table('user_closure')->select('ancestor')
            ->whereIn('ancestor', $structure->pluck('id'))
            ->where('depth', '<>', '0')->distinct()->get()->count();

        return $members;
    }
}
