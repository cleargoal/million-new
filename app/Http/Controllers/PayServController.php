<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Payservice;


class PayServController extends Controller
{
  protected $allpays;

  // show page with ALL payment methods
  public function getAll()
  {
	  $allpays = Payservice::all()->where('active', 'y');
	  return view('choose-payment-method')->with(['allpays'=>$allpays]);
  }
	
	// select chosen payment service and show form
	public function makePayment($payslug) {
		$paymentserv = Payservice::where('payslug', $payslug)->get();
		return view('make-payment', compact('paymentserv'));
	}
	
}
