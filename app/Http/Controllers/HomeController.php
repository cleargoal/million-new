<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class HomeController extends Controller
{
    protected $request;

    /**
     * Create a new controller instance. **
     *
     * @param request $request Request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     ** @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        return view('home');
    }

    /**
     *  Gets 'url_slug', call method 'findParent', shows frontpage
     *
     * @param string $url_slug Slug
     *
     * @return Factory|View
     */
    public function processSlugReferralLink(string $url_slug)
    {
        $this->findParent($url_slug);

        return view('frontpage');
    }

    /**
     *  Gets 'url_slug', call method 'findParent', shows register page
     *
     * @param string $url_slug Slug
     *
     * @return Factory|View
     */
    public function slugReferralRegister(string $url_slug)
    {
        $this->findParent($url_slug);
        return view('auth.register');
    }

    /**
     *  Gets 'url_slug', find user in DB by slug, takes ID by slug, set Parent
     *
     * @param string $url_slug Slug
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object
     */
    protected function findParent(string $url_slug)
    {
        $parentFound = DB::table('user_details')
            ->select('user_id')
            ->where('user_data_type', 'slug')
            ->where('user_data_val', $url_slug)
            ->first();

        if ($parentFound != null) {
            $this->storingSlug($url_slug, $parentFound->user_id);
        } else {
            if (User::count() < 1) {
                $this->storingSlug('just-first-registration', '0');
            }
        }

        return $parentFound;
    }

    protected function storingSlug($urlSlug, $parentIdFound)
    {
        $parentSlug = $this->request->cookie('parent_slug');
        if (empty($parentSlug)) {
            session(['parent_id' => $parentIdFound]);
            session(['parent_slug' => $urlSlug]);
            $cookiesLong = (string)config('settings.cookiesLong'); // 1 week
            cookie()->queue(cookie('parent_slug', $urlSlug, $cookiesLong));
            cookie()->queue(cookie('parent_id', $parentIdFound, $cookiesLong));
        }
        if (empty(session('parent_id'))) {
            session(['parent_id' => $parentIdFound]);
            session(['parent_slug' => $urlSlug]);
        }
    }

    public function checkAxios(Request $request)
    {
        if ($request->token === 'jhdfuisoh97y34fkJHLGiue89t') {
            return response()->json(
                [
                    'status' => 'good',
                ]
            );
        }
        return response('bad');
    }
}
