<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supp_faq;
use Intervention\Image\Facades\Image;
use \Illuminate\Support\Str;

class SupportFaqController extends Controller
{
    /** show all FAQ items with answers */
    public function supportfaq()
    { // frecinlist = first record in list
        $results = Supp_faq::whereNotNull('answer')->paginate(10);
        return view('support-faq')->with('pageslist', $results);
    }

    /** saves FAQ view and/or likes amount */
    public function faqViewSave(Request $request)
    {
        // dump('id: '.$request->id.', field: '.$request->updfieldname.', incr: '.$request->faqincrease);
        $updfieldname = $request->updfieldname; // DB field that updated
        $faqincrease = $request->faqincrease; // the new field value
        $id = $request->id;
        Supp_faq::where('id', $id)->update(
            [
                $updfieldname => $faqincrease,
            ]
        );
    }

    /** saves new question */
    public function saveFaques(Request $request)
    {
        $this->validate($request, [
            'question' => 'required',
            'screenshot_q' => 'mimes:jpeg,pjpeg,png',
        ]);
        $data = $request->all();
        $path = null;
        if ($request->hasFile('screenshot_q')) {
            $image = $request->file('screenshot_q');
            $name = Str::random(16) . '.' . $image->getClientOriginalExtension();
            $path = $image->storeAs('public/questions', $name);
        }
        $data['screenshot_q'] = $path;
        $data['user_id'] = \Auth::id();
        $supp_faq = Supp_faq::create($data);
        return response()->json([]);
    }
}
