<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailSender;
use Exception;

class MailSendController extends Controller
{

    // $mailTemplate - the key in 'config.mailTemplate' - for email type
    // $recipient - email of recipient
    protected $recipient;
    protected $mailTemplate;
    protected $mailParams;

    /**
     * Constructor
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        if (isset($request->parameters)) {
            $this->mailTemplate = $request->parameters['mailTemplate'];
            $this->recipient = $request->parameters['recipient'];
            $this->mailParams = $request->parameters;
        }
    }

    //  Try send email through browser by get/post methods - !!! may produce errors !!!
    public function trySendMail(Request $request)
    {
        $this->mailTemplate = $request->mailTemplate;
        $this->recipient = $request->recipient;
        $this->send();
    }

    // when email send through Object creation
    public function send(): string
    {
        /** change for: get return messages from dictionary */
        try {
            Mail::to($this->recipient)->send(new MailSender($this->mailParams));
            return "Письмо успешно отослано.";
        } catch (Exception $e) {
            return "Письмо не отослано. Ошибка: \n" .
                $e->getCode() . ', ' . $e->getMessage();
        }
    }
}
