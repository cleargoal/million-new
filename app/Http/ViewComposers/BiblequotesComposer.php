<?php

namespace app\Http\ViewComposers;

use App\Biblequote;
use Illuminate\View\View;
use App\Http\Controllers\BibleQuotesController;
use Illuminate\Support\Facades\Route;
use Request;

class BiblequotesComposer
{
    /** @var Biblequote  +++ also executes Open graph meta tags  */
    protected $biblequotes;

    /*** Bind data to the view.
     * @param  View  $view
     * @return View
     */
    public function compose(View $view)
    {
        $bcc = new BibleQuotesController;
        $data = $bcc->getBbq();

        $route_name = Route::currentRouteName();

        if ($route_name != 'showvslug') {
            $ogData = $this->ogSetter();
            return $view->with(compact('data', 'ogData'));
        } else {
            $fl = true;
            return $view->with(compact('data', 'fl'));
        }
    }

    /** multi_array_keys
     * @param $ar
     * @param $val
     * @return mixed
     */
    protected function multi_array_keys($ar, $val)
    {
        foreach ($ar as $k => $v) {
            if (array_search($val, $v) !== false)
                $keys[] = $k;
        }
        return isset($keys) ? $keys : false;
    }

    /** fills open graph variables for all pages exclude Vicard */
    private function ogSetter()
    {
        $og_title = config('app.name');
        $og_url = '';
        if (Request::path() === '/') {
            $og_url =  '/' . config('settings.og_url');
        }
        $og_image = asset('img/og-header.jpg');
        $og_description = config('logic.ogdescrip');
        return ['og_title' => $og_title, 'og_url' => $og_url, 'og_image' => $og_image, 'og_description' => $og_description];
    }
}
