<?php
namespace App;

use Franzose\ClosureTable\Contracts\ClosureTableInterface;

interface UserClosureInterface extends ClosureTableInterface
{
}
